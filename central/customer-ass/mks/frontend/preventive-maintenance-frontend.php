<?php
include("database/database.php");
$query = "SELECT equipamento.serie,equipamento_familia.fabricante,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade where maintenance_preventive.id_status like '1'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($serie,$fabricante,$instituicao,$area,$setor,$programada,$id,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);


  ?>
  <style>
  * {
    box-sizing: border-box;
  }

  #myInput {

    background-position: 10px 10px;
    background-repeat: no-repeat;
    width: 100%;
    font-size: 16px;
    padding: 12px 20px 12px 40px;
    border: 1px solid #ddd;
    margin-bottom: 12px;
  }

  #myTable {
    border-collapse: collapse;
    width: 100%;
    border: 1px solid #ddd;
    font-size: 18px;
  }

  #myTable th, #myTable td {
    text-align: left;
    padding: 12px;
  }

  #myTable tr {
    border-bottom: 1px solid #ddd;
  }

  #myTable tr.header, #myTable tr:hover {
    background-color: #f1f1f1;
  }
</style>



<h2>Preventiva</h2>

<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Busca por Rotina.." title="Type in a name">

<table id="myTable">
  <tr class="header">
    <th style="width:20%;">Rotina</th>
    
    <th style="width:20%;">Programada</th>
    <th style="width:30%;">Equipamento</th>
    <th style="width:30%;">Ação</th>
  </tr>
  <?php   while ($stmt->fetch()) {   ?>
    <tr>
      <td><?printf($rotina); ?></td>
     
      <td><?php printf($programada); ?></td>
      <td><?php printf($codigo); ?> - <?php printf($nome); ?> - <?php printf($modelo); ?> - <?php printf($fabricante); ?> </td>
   <td> <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('preventive-maintenance-open.php?id=<?printf($id); ?>');">
                                <i class="bi bi-credit-card size-32"></i><span>Abrir</span>
                            </button></td>
    </tr>

  </tr>
        <?php   } }  ?>
</table>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
