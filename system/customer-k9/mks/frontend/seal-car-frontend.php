<?php
include("database/database.php");
$query = "SELECT  device.macadress ,car.id,car.codigo,car.patrimonio, unidade_setor.nome AS 'setor' FROM car LEFT JOIN unidade_area ON  unidade_area.id = car.id_unidade_area LEFT JOIN  unidade_setor ON unidade_setor.id = unidade_area.id_unidade_setor LEFT JOIN unidade ON unidade.id = unidade_setor.id_unidade LEFT JOIN device ON car.id_device = device.id LEFT JOIN alarm_status ON alarm_status.id = car.id_status where car.trash = 1 and car.id_status != 3 order by car.id DESC ";

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($macadress,$car_id,$codigo,$patrimonio,$area);
 

  ?>
  <style>
  * {
    box-sizing: border-box;
  }

  #myInput {

    background-position: 10px 10px;
    background-repeat: no-repeat;
    width: 100%;
    font-size: 16px;
    padding: 12px 20px 12px 40px;
    border: 1px solid #ddd;
    margin-bottom: 12px;
  }

  #myTable {
    border-collapse: collapse;
    width: 100%;
    border: 1px solid #ddd;
    font-size: 18px;
  }

  #myTable th, #myTable td {
    text-align: left;
    padding: 12px;
  }

  #myTable tr {
    border-bottom: 1px solid #ddd;
  }

  #myTable tr.header, #myTable tr:hover {
    background-color: #f1f1f1;
  }
</style>



<h2>Carrinho</h2>

<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Busca por Carrinho.." title="Type in a name">

<table id="myTable">
  <tr class="header">
    <th style="width:20%;">Codigo</th>
    <th style="width:20%;">Patrimonio</th>
    
    <th style="width:30%;">Area</th>
    <th style="width:30%;">Ação</th>
  </tr>
  <?php   while ($stmt->fetch()) {   ?>
    <tr>
      <td><?printf($codigo); ?></td>
      <td><?php printf($patrimonio); ?></td>
     
      <td><?php printf($area); ?>  </td>
      <td> <button type="button" class="btn btn-black btn-lg"
        onclick="window.location.replace('seal-car-look.php?id=<?printf($macadress); ?>');">
        <i class="bi bi-credit-card text-white size-32"></i><span class="text-white">Abrir</span>
      </button>
</td>
    </tr>

  </tr>
        <?php   } }  ?>
</table>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
