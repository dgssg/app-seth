<?php
include("database/database.php");
//include("../../codigo.php");
//$codigoget = (int)($_GET["codigoget"]);
$car_id = ($_GET["id"]);

	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
    setcookie('file', null, -1);

	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$email=	$_SESSION['email'] ;
    $file = "seal-car-look";
    setcookie("file","$file");
    $query = "SELECT car.id,alarm_status.alarm,car.seal_now,car.seal_before,car.serie,car.ativo,car.patrimonio,device.nome AS 'device',car.nome, car.codigo AS 'codigo',unidade_area.nome AS'area',unidade_area.codigo AS 'codigo_area',unidade_setor.nome AS 'setor',unidade_setor.codigo AS 'codigo_setor',unidade.id AS 'id_unidade',  unidade.nome AS 'unidade', unidade.codigo AS 'codigo_unidade', unidade.endereco, unidade.cep, unidade.bairro, unidade.cidade, unidade.estado, unidade.ibge, unidade.logo, unidade.upgrade, unidade.reg_date, unidade.trash, unidade.cnpj FROM car LEFT JOIN unidade_area ON  unidade_area.id = car.id_unidade_area LEFT JOIN  unidade_setor ON unidade_setor.id = unidade_area.id_unidade_setor LEFT JOIN unidade ON unidade.id = unidade_setor.id_unidade LEFT JOIN device ON car.id_device = device.id LEFT JOIN alarm_status ON alarm_status.id = car.id_status where car.trash = 1 and  device.macadress like '$car_id' ";
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($device_id,$alarm,$seal_now,$seal_before,$serie,$ativo,$patrimonio,$device,$nome,$codigo,$area,$codigo_area,$setor,$codigo_setor,$id_unidade,$unidade,$codigo_unidade,$endereco,$cep,$bairro,$cidade,$estado,$ibge,$logo,$upgrade,$reg_date,$trash,$cnpj);
    
    
    
      while ($stmt->fetch()) {
    
      }
    }
  function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
    $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
    $con = 1;
    $var = '';
    while($con < $QuantidadeDeCaracteresDaChave ){
      $n = rand(0, 57); 
      if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
      }else{
        $var = $var.$n.$res[$n];
        $con++;
      }
    }
    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
  }
  //chamando a função.
  // echo chaveAlfaNumerica(5);
  //   echo nl2br("\r\n");
  /* A uniqid, like: 4b3403665fea6 
  printf("uniqid(): %s\r\n", uniqid());
  
  /* We can also prefix the uniqid, this the same as 
  * doing:
  *
  * $uniqid = $prefix . uniqid();
  * $uniqid = uniqid($prefix);
  
  printf("uniqid('php_'): %s\r\n", uniqid('php_'));
  
  /* We can also activate the more_entropy parameter, which is 
  * required on some systems, like Cygwin. This makes uniqid()
  * produce a value like: 4b340550242239.64159797
  
  printf("uniqid('', true): %s\r\n", uniqid('', true));
  */
  class UUID {
    public static function v3($namespace, $name) {
      if(!self::is_valid($namespace)) return false;
      
      // Get hexadecimal components of namespace
      $nhex = str_replace(array('-','{','}'), '', $namespace);
      
      // Binary Value
      $nstr = '';
      
      // Convert Namespace UUID to bits
      for($i = 0; $i < strlen($nhex); $i+=2) {
        $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
      }
      
      // Calculate hash value
      $hash = md5($nstr . $name);
      
      return sprintf('%08s-%04s-%04x-%04x-%12s',
        
        // 32 bits for "time_low"
        substr($hash, 0, 8),
        
        // 16 bits for "time_mid"
        substr($hash, 8, 4),
        
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 3
        (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,
        
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
        
        // 48 bits for "node"
        substr($hash, 20, 12)
      );
    }
    
    public static function v4() {
      return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        
        // 32 bits for "time_low"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),
        
        // 16 bits for "time_mid"
        mt_rand(0, 0xffff),
        
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,
        
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,
        
        // 48 bits for "node"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
      );
    }
    
    public static function v5($namespace, $name) {
      if(!self::is_valid($namespace)) return false;
      
      // Get hexadecimal components of namespace
      $nhex = str_replace(array('-','{','}'), '', $namespace);
      
      // Binary Value
      $nstr = '';
      
      // Convert Namespace UUID to bits
      for($i = 0; $i < strlen($nhex); $i+=2) {
        $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
      }
      
      // Calculate hash value
      $hash = sha1($nstr . $name);
      
      return sprintf('%08s-%04s-%04x-%04x-%12s',
        
        // 32 bits for "time_low"
        substr($hash, 0, 8),
        
        // 16 bits for "time_mid"
        substr($hash, 8, 4),
        
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 5
        (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,
        
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
        
        // 48 bits for "node"
        substr($hash, 20, 12)
      );
    }
    
    public static function is_valid($uuid) {
      return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
        '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
    }
  }
  
  // Usage
  // Named-based UUID.
  
  $v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
  $v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
  
  // Pseudo-random UUID
  
  $v4uuid = UUID::v4();
  
?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
    <title>MK Sistema Biomedicos </title>


    <!-- manifest meta -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="manifest" href="../../manifest.json" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="../../assets/img/favicon180.png" sizes="180x180">
    <link rel="icon" href="../../assets/img/favicon32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="../../assets/img/favicon16.png" sizes="16x16" type="image/png">

    <!-- Google fonts-->

    <link rel="preconnect" href="../../https://fonts.googleapis.com/">
    <link href="../../https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&amp;display=swap" rel="stylesheet">
    <link href="../../https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&amp;display=swap" rel="stylesheet">

    <!-- bootstrap icons -->
    <link rel="stylesheet" href="../../../../../../cdn.jsdelivr.net/npm/bootstrap-icons%401.5.0/font/bootstrap-icons.css">

    <!-- swiper carousel css -->
    <link rel="stylesheet" href="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.css">

    <!-- style css for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet" id="style">
  <link href="dropzone.css" type="text/css" rel="stylesheet" />
  
  <!-- 2 -->
  <script src="dropzone.min.js"></script>
</head>

<body class="body-scroll" data-page="index">

    <!-- loader section -->
    <div class="container-fluid loader-wrap">
        <div class="row h-100">
            <div class="col-10 col-md-6 col-lg-5 col-xl-3 mx-auto text-center align-self-center">
                <div class="loader-cube-wrap loader-cube-animate mx-auto">
                    <img src="../../../framework/img/64x64.png" alt="Logo">
                </div>
                <p class="mt-4">Carregando<br><strong>Aguarde...</strong></p>
            </div>
        </div>
    </div>
    <!-- loader section ends -->

    <!-- Sidebar main menu -->
    <div class="sidebar-wrap  sidebar-pushcontent">
        <!-- Add overlay or fullmenu instead overlay -->
        <div class="closemenu text-muted">Fechar Menu</div>
        <div class="sidebar dark-bg">
            <!-- user information -->
            <div class="row my-3">
                <div class="col-12 ">
                    <div class="card shadow-sm bg-opac text-white border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <figure class="avatar avatar-44 rounded-15">
                                        <img src="logo/clientelogo.png" alt="">
                                    </figure>
                                </div>
                                <div class="col px-0 align-self-center">
                                    <p class="mb-1"> <?php printf($usuariologado); ?></p>
                                    <p class="text-muted size-12"><?php printf($setorlogado); ?></p>
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-44 btn-light">
                                        <i class="bi bi-box-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-opac text-white border-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <h1 class="display-4"></h1>
                                    </div>
                                    <div class="col-auto">
                                        <p class="text-muted"></p>
                                    </div>
                                    <div class="col text-end">
                                        <p class="text-muted"><a href="#" ></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

       <!-- user emnu navigation -->
       <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="dashboard">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Dashboard</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                       <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="historic">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Historico</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="seal-car">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Lacrar Carrinho</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li><!--
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-technician">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Tecnico </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-user">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Usuario </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="service">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Serviço </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li> 

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button"
                                aria-expanded="false">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-person"></i></div>
                                <div class="col">Conta</div>
                                <div class="arrow"><i class="bi bi-plus plus"></i> <i class="bi bi-dash minus"></i>
                                </div>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item nav-link" href="#">
                                        <div class="avatar avatar-40 rounded icon"><i class="bi bi-calendar2"></i></div>
                                        <div class="col">Perfl</div>
                                        <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                                    </a></li>
                                <li><a class="dropdown-item nav-link" href="#">
                                        <div class="avatar avatar-40 rounded icon"><i class="bi bi-calendar-check"></i>
                                        </div>
                                        <div class="col">Configurações</div>
                                        <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                                    </a></li>
                            </ul>
                        </li> -->









                        <li class="nav-item">
                            <a class="nav-link" href="../../signin.html" tabindex="-1">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-box-arrow-right"></i></div>
                                <div class="col">Sair</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Sidebar main menu ends -->

    <!-- Begin page -->
    <main class="h-100">

        <!-- Header -->
        <header class="header position-fixed">
            <div class="row">
                <div class="col-auto">
                    <a href="../../javascript:void(0)" target="_self" class="btn btn-light btn-44 menu-btn">
                        <i class="bi bi-list"></i>
                    </a>
                </div>
                <div class="col align-self-center text-center">
                    <div class="logo-small">
                        <img src="../../../framework/img/64x64.png" alt="Logo">
                        <h5>Sistema MKS</h5>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="#" target="_self" class="btn btn-light btn-44">
                        <i class="bi bi-bell"></i>
                        <span class="count-indicator"></span>
                    </a>
                </div>
            </div>
        </header>
        <!-- Header ends -->
        <div class="row mb-4">
                <div class="col-12 text-center">
                    <img src="../../assets/img/infographic-scan.png" alt="">
                </div>
            </div>
 
        <!-- main page content -->
        <div class="main-container container">
            <!-- wallet balance -->
            <div class="card shadow-sm mb-4">
                <div class="card-body">
                    <div class="row">
                         
                        
                    </div>
                </div>
                <div class="card theme-bg text-white border-0 text-center">
                    <div class="card-body">
                        <h1 class="display-1 my-2"><?php printf($codigo); ?></h1>
                        <p class="text-muted mb-2">Carrinho de Emergencia</p>
                    </div>
                </div>
            </div>

            <!-- summary -->
            <div class="row mb-3">
                <div class="col-6 col-md-4">
                    <div class="card shadow-sm mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto px-0">
                                    <div class="avatar avatar-40 bg-warning text-white shadow-sm rounded-10-end">
                                        <i class="bi bi-star"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-muted size-12 mb-0">Lacre Atual </p>
                                    <p><?php printf($seal_now); ?> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="card shadow-sm mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto px-0">
                                    <div class="avatar avatar-40 bg-success text-white shadow-sm rounded-10-end">
                                        <i class="bi bi-cash-stack"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-muted size-12 mb-0">Setor</p>
                                    <p><?php printf($setor); ?> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="card shadow-sm mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto px-0">
                                    <div class="avatar avatar-40 bg-warning text-white shadow-sm rounded-10-end">
                                        <i class="bi bi-star"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-muted size-12 mb-0">Carrinho </p>
                                    <p><?php printf($codigo); ?> - <?php printf($patrimonio); ?>  - <?php printf($nome); ?> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
              <div class="col-12 col-md-6 col-lg-4">
                <div class="form-group form-floating">
              <!--    <input type="file" class="form-control" id="fileupload" name="fileupload"> -->
                  <form  class="dropzone" action="backend/seal-dropzone-upload-backend.php" method="post">
                    </form >
                  <label for="fileupload">Anexo</label>
                </div>
              </div>
                    <form action="backend/seal-backend.php" method="post">
                      <input id="id_car" class="form-control" type="hidden" name="id_car" value="<?php printf($device_id); ?>"  >
                      <input id="seal_before" class="form-control" type="hidden" name="seal_before"  value="<?php printf($seal_now); ?>"  >
                      <input type="hidden" class="form-control"  value=" <?php  printf($v4uuid);?>" id="v4uuid" name="v4uuid" >
                      <input type="hidden" class="form-control"  value=" <?php  printf($chave);?>" id="chave" name="chave" >

              <div class="form-floating is-valid mb-3">
                <input type="text" class="form-control"   placeholder="Lacre Novo"
                  id="seal_now" name="seal_now">
                <label for="username">Lacre Novo</label>
              </div>
              <div class="col ps-0">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Observação" name="obs" id="obs">
                                </div>
              </div>

            
            
                <p class="text-muted mb-4">Digite o Codigo Abaixo</p>
              <h4>Assinatura digital</h4>
              <p>Para Assinar digite o texto abaixo.</p>
              <?php $chave=chaveAlfaNumerica(5); echo "$chave" ?>
              <p> 												<label class="col-form-label col-md-3 col-sm-3 label-align" for="assinature"> <span ></span>
              </label>
                <div class="form-floating is-valid mb-3">
                  <input type="text" class="form-control"   placeholder="Enter OTP" id="otp">
                  <label for="otp">Chave</label>
                </div>
                <button type="submit" class="btn btn-lg btn-default w-100 mb-4 shadow">
                  Salvar
                </button>
                
                </form>

                        <div class="col-12 text-center mt-auto">
                <div class="row justify-content-center footer-info">
                  <div class="col-auto text-center">
                    <span class="progressstimer">
                      <img src="../../assets/img/progress.png" alt="">
                      <span class="timer" id="timer">5:00</span>
                    </span>
                    <br/>
                                     </div>
                </div>
              </div>
            </div>
            

            <!-- tabs structure 
            <ul class="nav nav-pills nav-justified tabs mb-3" id="assetstabs" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#cards"
                        type="button" role="tab" aria-controls="cards" aria-selected="true">Posicionamento</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="currency-tab" data-bs-toggle="tab" data-bs-target="#currency"
                        type="button" role="tab" aria-controls="currency" aria-selected="false">Fechar</button>
                </li>
            </ul>
         <!--    <div class="tab-content" id="assetstabsContent">
                <div class="tab-pane fade show active" id="cards" role="tabpanel" >
                    <!-- swiper credit cards 
                    <div class="row mb-3">
                        <div class="col-12 px-0">
                            <div class="swiper-container cardswiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="card dark-bg">
                                            <div class="card-body">
                                                <div class="row mb-3">
                                                    <div class="col-auto align-self-center">
                                                        <img src="assets/img/masterocard.png" alt="">
                                                    </div>
                                                    <div class="col align-self-center text-end">
                                                        <p class="small">
                                                            <span class="text-uppercase size-10">Validity</span><br>
                                                            <span class="text-muted">06/25</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h4 class="fw-normal mb-2">
                                                            56040.00
                                                            <span class="small text-muted">USD</span>
                                                        </h4>
                                                        <p class="mb-0 text-muted size-12">10141 0021 0001 0154</p>
                                                        <p class="text-muted size-12">Debit Card</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="card theme-radial-gradient border-0">
                                            <div class="card-body">
                                                <div class="row mb-3">
                                                    <div class="col-auto align-self-center">
                                                        <i class="bi bi-wallet2"></i> Wallet
                                                    </div>
                                                    <div class="col align-self-center text-end">
                                                        <p class="small">
                                                            <span class="text-uppercase size-10">Validity</span><br>
                                                            <span class="text-muted">Unlimited</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h4 class="fw-normal mb-2">
                                                            100.00
                                                            <span class="small text-muted">USD</span>
                                                        </h4>
                                                        <p class="mb-0 text-muted size-12">10141 0021 0001 0154</p>
                                                        <p class="text-muted size-12">Debit Card</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row mb-3">
                                                    <div class="col-auto align-self-center">
                                                        <img src="assets/img/masterocard.png" alt="">
                                                    </div>
                                                    <div class="col align-self-center text-end">
                                                        <p class="small">
                                                            <span class="text-uppercase size-10">Validity</span><br>
                                                            <span class="text-muted">09/24</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h4 class="fw-normal mb-2">
                                                            150540.00
                                                            <span class="small text-muted">USD</span>
                                                        </h4>
                                                        <p class="mb-0 text-muted size-12">10141 0021 0001 0154</p>
                                                        <p class="text-muted size-12">Debit Card</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Transactions 
                    <div class="row mb-3">
                        <div class="col">
                            <h6 class="title">Transactions</h6>
                        </div>                        
                        <div class="col-auto">
                            <a href="#" class="small">Add Card</a>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-12 px-0">
                            <ul class="list-group list-group-flush bg-none">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10 ">
                                                <img src="assets/img/company4.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Zomato</p>
                                            <p class="text-muted size-12">Food</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0">-25.00</p>
                                            <p class="text-muted size-12">Debit Card 4545</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10">
                                                <img src="assets/img/company5.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Uber</p>
                                            <p class="text-muted size-12">Travel</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0">-26.00</p>
                                            <p class="text-muted size-12">Debit Card 4545</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10">
                                                <img src="assets/img/company1.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Starbucks</p>
                                            <p class="text-muted size-12">Food</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0">-18.00</p>
                                            <p class="text-muted size-12">Cash</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10">
                                                <img src="assets/img/company3.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Walmart</p>
                                            <p class="text-muted size-12">Clothing</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0">-105.00</p>
                                            <p class="text-muted size-12">Wallet</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div> -->

                </div>
                <div class="tab-pane fade" id="currency" role="tabpanel" aria-labelledby="currency-tab">
                 <!--   <div class="row">
                        <div class="col-12 col-md-6 align-self-center">
                            <div class="chartdoughnut mb-4">
                                <div class="datadisplay text-center shadow">
                                    <h1 class="fw-normal">15.56k</h1>
                                    <p class="text-muted">Expense</p>
                                </div>
                                <canvas id="doughnutchart"></canvas>
                            </div>
                        </div>
                    </div>
                        
                    <!-- Transactions 
                    <div class="row mb-3">
                        <div class="col">
                            <h6 class="title">Currencies</h6>
                        </div>                        
                        <div class="col-auto">
                            <a href="#" class="small">Add Currency</a>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-12 px-0">
                            <ul class="list-group list-group-flush bg-none">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10 bg-warning text-white">
                                                <span class="vm d-inline-block">$</span>
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">US Dollar</p>
                                            <p class="text-muted size-12">USD</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0 fw-bold">25485.00</p>
                                            <p class="text-muted size-12 text-success">2.4 EUR (0.15%)</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10 bg-success text-white">
                                                <span class="vm d-inline-block">O</span>
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Other</p>
                                            <p class="text-muted size-12">DGC, DRC, FIT</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0 fw-bold">1001.36</p>
                                            <p class="text-muted size-12 text-success">1.65 USD (0.10%)</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10">
                                                <img src="assets/img/company6.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">BitCoin</p>
                                            <p class="text-muted size-12">BTC</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0 fw-bold">150.00</p>
                                            <p class="text-muted size-12 text-danger">1.65 USD (0.08%)</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10">
                                                <img src="assets/img/company7.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Etherium</p>
                                            <p class="text-muted size-12">ETH</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0 fw-bold">146.00</p>
                                            <p class="text-muted size-12 text-success">2.65 USD (0.16%)</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div> 
                </div>
            </div>-->

        </div>
        <!-- main page content ends -->



        <!-- main page content -->

<!-- <iframe src="https://drive.google.com/embeddedfolderview?id=16rAbXMay8M-iXygeGnpOFSgEtzE0VPX9#list" style="width:100%; height:500px; border:0;"></iframe> -->
        <!-- main page content ends -->


    </main>
    <!-- Page ends-->

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <ul class="nav nav-pills nav-justified">
                <li class="nav-item">
                    <a class="nav-link active" href="dashboard">
                        <span>
                            <i class="nav-icon bi bi-house"></i>
                            <span class="nav-text">Dashboard</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="historic">
                        <span>
                            <i class="nav-icon bi bi-laptop"></i>
                            <span class="nav-text">Historico</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item centerbutton">
                    <div class="nav-link">
                        <span class="theme-radial-gradient">
                            <i class="close bi bi-x"></i>

                            <img src="../../assets/img/" class="nav-icon" alt="" />
                        </span>
                        <div class="nav-menu-popover justify-content-between">
                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('seal-car.php');">
                                <i class="bi bi-credit-card size-32"></i><span>Lacrar</span>
                            </button>

                         

                          

                         <!--   <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('receivemoney.html');">
                                <i class="bi bi-arrow-down-left-circle size-32"></i><span>Receive</span>
                            </button> -->
                        </div>
                    </div>
                </li>
               
            </ul>
        </div>
    </footer>
    <!-- Footer ends-->

    <!-- PWA app install toast message -->
  <!--  <div class="position-fixed bottom-0 start-50 translate-middle-x  z-index-10">
        <div class="toast mb-3" role="alert" aria-live="assertive" aria-atomic="true" id="toastinstall"
            data-bs-animation="true">
            <div class="toast-header">
                <img src="../../assets/img/favicon32.png" class="rounded me-2" alt="...">
                <strong class="me-auto">Install PWA App</strong>
                <small>now</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                <div class="row">
                    <div class="col">
                        Click "Install" to install PWA app & experience indepedent.
                    </div>
                    <div class="col-auto align-self-center ps-0">
                        <button class="btn-default btn btn-sm" id="addtohome">Install</button>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- Camera Modal -->
    <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xsm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="cammodalLabel">Autorizar</h6>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                    <img src="assets/img/infographic-camera.png" alt="" class="mb-4">
                    <h3 class="text-color-theme mb-2">Camera Acesso</h3>
                    <p class="text-muted">QRCOD.
                    </p>
                </div>
                <button type="button" class="btn btn-lg btn-primary rounded-15" data-bs-dismiss="modal">Sempre</button>
            </div>
        </div>
    </div>
                <script>
                  // Defina a data de término da contagem regressiva (3 minutos)
                  const endTime = new Date().getTime() + 5 * 60 * 1000;
                  
                  // Atualize a contagem regressiva a cada segundo
                  const timerElement = document.getElementById('timer');
                  
                  function updateTimer() {
                    const currentTime = new Date().getTime();
                    const timeRemaining = endTime - currentTime;
                    
                    if (timeRemaining <= 0) {
                      // Contagem regressiva encerrada
                      timerElement.textContent = '0:00';
                      clearInterval(timerInterval);
                      // Execute ação quando a contagem regressiva terminar
                      //alert('Tempo encerrada!');
                      window.location = "https://app.mksistemasbiomedicos.com.br/system";

                    } else {
                      const minutes = Math.floor((timeRemaining % (1000 * 60 * 60)) / (1000 * 60));
                      const seconds = Math.floor((timeRemaining % (1000 * 60)) / 1000);
                      const formattedTime = `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
                      timerElement.textContent = formattedTime;
                    }
                  }
                  
                  // Inicialize a contagem regressiva
                  updateTimer();
                  const timerInterval = setInterval(updateTimer, 1000);

                </script>
               
    <!-- Camera Modal ends-->
    <!-- Required jquery and libraries -->
    <script src="../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/popper.min.js"></script>
   <!-- <script src="../../assets/vendor/bootstrap-5/js/bootstrap.bundle.min.js"></script> -->

    <!-- cookie js -->
    <script src="../../assets/js/jquery.cookie.js"></script>

    <!-- Customized jquery file  -->
    <script src="../../assets/js/main.js"></script>
    <script src="../../assets/js/color-scheme.js"></script>

    <!-- PWA app service registration and works -->
    <script src="../../assets/js/pwa-services.js"></script>

    <!-- Chart js script -->
    <script src="../../assets/vendor/chart-js-3.3.1/chart.min.js"></script>

    <!-- Progress circle js script -->
    <script src="../../assets/vendor/progressbar-js/progressbar.min.js"></script>

    <!-- swiper js script -->
    <script src="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.js"></script>

   <!-- page level custom script 
     <script src="../../assets/js/app.js"></script> 
  <!-- Required jquery and libraries -->
 
  <!-- page level custom script -->

</body>

</html>
