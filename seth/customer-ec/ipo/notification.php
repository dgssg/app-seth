<?php
include("database/database.php");
//include("../../codigo.php");
$codigoget = (int)($_GET["codigoget"]);

	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
    setcookie('file', null, -1);

	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$email=	$_SESSION['email'] ;
    $file = "notification";
    setcookie("file","$file")


?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
      <title>MK Sistema Biomedicos </title>
  <!-- Link para o Font Awesome (se ainda não estiver carregado no projeto) -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <!-- manifest meta -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="manifest" href="../../manifest.json" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="../../assets/img/favicon180.png" sizes="180x180">
    <link rel="icon" href="../../assets/img/favicon32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="../../assets/img/favicon16.png" sizes="16x16" type="image/png">

    <!-- Google fonts-->

    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&amp;display=swap" rel="stylesheet">

    <!-- bootstrap icons -->
    <link rel="stylesheet" href="../../../../../../cdn.jsdelivr.net/npm/bootstrap-icons%401.5.0/font/bootstrap-icons.css">

    <!-- swiper carousel css -->
    <link rel="stylesheet" href="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.css">

    <!-- style css for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet" id="style">
</head>

<body class="body-scroll" data-page="index">

    <!-- loader section -->
    <div class="container-fluid loader-wrap">
        <div class="row h-100">
            <div class="col-10 col-md-6 col-lg-5 col-xl-3 mx-auto text-center align-self-center">
                <div class="loader-cube-wrap loader-cube-animate mx-auto">
                    <img src="../../../framework/img/64x64.png" alt="Logo">
                </div>
                <p class="mt-4">Carregando<br><strong>Aguarde...</strong></p>
            </div>
        </div>
    </div>
    <!-- loader section ends -->

    <!-- Sidebar main menu -->
    <div class="sidebar-wrap  sidebar-pushcontent">
        <!-- Add overlay or fullmenu instead overlay -->
        <div class="closemenu text-muted">Fechar Menu</div>
        <div class="sidebar dark-bg">
            <!-- user information -->
            <div class="row my-3">
                <div class="col-12 ">
                    <div class="card shadow-sm bg-opac text-white border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <figure class="avatar avatar-44 rounded-15">
                                        <img src="https://seth.mksistemasbiomedicos.com.br/customer-ec/<?php printf($instituicaologado); ?>/logo/clientelogo.png" alt="">
                                    </figure>
                                </div>
                                <div class="col px-0 align-self-center">
                                    <p class="mb-1"> <?php printf($usuariologado); ?></p>
                                    <p class="text-muted size-12"><?php printf($setorlogado); ?></p>
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-44 btn-light">
                                        <i class="bi bi-box-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-opac text-white border-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <h1 class="display-4"></h1>
                                    </div>
                                    <div class="col-auto">
                                        <p class="text-muted"></p>
                                    </div>
                                    <div class="col text-end">
                                        <p class="text-muted"><a href="#" ></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- user emnu navigation -->
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="dashboard">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Dashboard</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="corrective-maintenance">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manutenção Corretiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="corrective-maintenance-new">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Nova Manutenção Corretiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="preventive-maintenance">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manutenção Preventiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-technician">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Tecnico </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-user">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Usuario </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="service">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Serviço </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>

                      <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="config">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Configurações </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>









                        <li class="nav-item">
                            <a class="nav-link" href="../../signin.html" tabindex="-1">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-box-arrow-right"></i></div>
                                <div class="col">Sair</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Sidebar main menu ends -->

    <!-- Begin page -->
    <main class="h-100">

        <!-- Header -->
        <header class="header position-fixed">
            <div class="row">
                <div class="col-auto">
                    <a href="../../javascript:void(0)" target="_self" class="btn btn-light btn-44 menu-btn" >
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="col align-self-center text-center">
                    <div class="logo-small">
                        <img src="../../../framework/img/64x64.png" alt="Logo">
                        <h5>Sistema SETH</h5>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="#" target="_self" class="btn btn-light btn-44">
                        <i class="fa fa-bell"></i>
                        <span class="count-indicator"></span>
                    </a>
                </div>
            </div>
        </header>
        <!-- Header ends -->
        <div class="row mb-4">
                <div class="col-12 text-center">
                    <img src="../../assets/img/infographic-scan.png" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center mb-4">
                    <h1 class="mb-3 text-color-theme">Notificações</h1>
                    <p class="text-muted mb-3">Consulta de Notificações e  Eventos.</p>
                    
                </div>
            </div>
    
      <style>
        body {
          font-family: Arial, sans-serif;
          margin: 0;
          padding: 0;
          background-color: #f0f0f0;
          text-align: center;
        }
        
        #box {
          width: 200px;
          height: 200px;
          background-color: #ddd;
          margin: 100px auto;
          line-height: 200px;
        }
      
       
      </style>
      <div class="error-container">
        <div class="well">
          <h1 class="grey lighter smaller">
            <span class="blue bigger-125">
              <i class="ace-icon fa fa-random"></i>
              Desenvolvimento
            </span>
            <br>
            Recurso ainda indisponível nessa versão!
          </h1>
          
          <hr />
          <h3 class="lighter smaller">
            Mas estamos trabalhando
            <i class="ace-icon fa fa-wrench icon-animated-wrench bigger-125"></i>
            sobre ele!
          </h3>
          
          <div class="space"></div>
          
          <div>
            <h4 class="lighter smaller">Enquanto isso, tente um dos seguintes:</h4>
            
            <ul class="list-unstyled spaced inline bigger-110 margin-15">
              <li>
                <i class="ace-icon fa fa-hand-o-right blue"></i>
                Leia o documento
              </li>
              
              <li>
                <i class="ace-icon fa fa-hand-o-right blue"></i>
                Veja as ferramentas!
              </li>
              
              <li>
                <i class="ace-icon fa fa-hand-o-right blue"></i>
                Veja os avisos de atualização!
              </li>
            </ul>
          </div>
          
          <hr />
          <div class="space"></div>
          
          <div class="center">
            
            
            <a href="dashboard" class="btn btn-primary">
              <i class="ace-icon fa fa-tachometer"></i>
              Dashboard
            </a>
          </div>
        </div>
      </div>
      

    <!--
      <script src="https://unpkg.com/html5-qrcode"></script>
      <div id="qr-reader" style="width:500px"></div>
      <div id="qr-reader-results"></div>
      <div id="box">Deslize Aqui</div>
       -->
      <script>
        
        var resultContainer = document.getElementById('qr-reader-results');
        var lastResult, countResults = 0;
        
        function onScanSuccess(decodedText, decodedResult) {
          if (decodedText !== lastResult) {
            ++countResults;
            lastResult = decodedText;
            // Handle on success condition with the decoded message.
            console.log(`Scan result ${decodedText}`, decodedResult);
            alert(`Scan result ${decodedText}`, decodedResult);

          }
        }
        
        var html5QrcodeScanner = new Html5QrcodeScanner(
          "qr-reader", { fps: 10, qrbox: 250 });
        html5QrcodeScanner.render(onScanSuccess);
      </script>
      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js"></script>
      <script>
        // Selecionando o elemento para aplicar o swipe
        var box = document.getElementById('box');
        
        // Criando um objeto Hammer para o elemento
        var hammer = new Hammer(box);
        // Adicionando um ouvinte de evento para detectar o gesto de tap (toque)
        hammer.on('tap', function(event) {
          alert('Tap (Toque) detectado!');
        });
        
        // Adicionando um ouvinte de evento para detectar o gesto de double tap (toque duplo)
        hammer.on('doubletap', function(event) {
          alert('Double Tap (Toque Duplo) detectado!');
        });
        
        // Adicionando um ouvinte de evento para detectar o gesto de pinch (beliscar)
        hammer.on('pinch', function(event) {
          alert('Pinch (Beliscar) detectado!');
        });
        
        // Adicionando um ouvinte de evento para detectar o gesto de rotate (rotacionar)
        hammer.on('rotate', function(event) {
          alert('Rotate (Rotacionar) detectado!');
        });
        
        // Adicionando um ouvinte de evento para detectar o gesto de long press (pressão longa)
        hammer.on('press', function(event) {
          alert('Long Press (Pressão Longa) detectado!');
        });
        // Adicionando um ouvinte de evento para detectar o gesto swipe
        hammer.on('swipe', function(event) {
          // Verificando a direção do swipe e exibindo uma mensagem correspondente
          switch(event.direction) {
            case Hammer.DIRECTION_UP:
              alert('Swipe para cima detectado!');
              break;
            case Hammer.DIRECTION_DOWN:
              alert('Swipe para baixo detectado!');
              break;
            case Hammer.DIRECTION_LEFT:
              alert('Swipe para esquerda detectado!');
              break;
            case Hammer.DIRECTION_RIGHT:
              alert('Swipe para direita detectado!');
              break;
          }
        });
      </script> 
      <?php
        date_default_timezone_set('America/Sao_Paulo');
        $today = date("Y-m-d");
        $read_open = 1;
        
        $query = "SELECT id, origem, ref, data, read_open FROM notif WHERE read_open = 1";
        
        $resultados = $conn->query($query);
        $notifications = array();
        
        while ($row = mysqli_fetch_assoc($resultados)) {
          $notifications[] = $row;
        }
        
        $conn->close();
      ?>
      
      <script>
        if ('serviceWorker' in navigator) {
          window.addEventListener('load', function() {
            navigator.serviceWorker.register('service-worker.js').then(function(registration) {
              console.log('ServiceWorker registration successful with scope: ', registration.scope);
            }, function(err) {
              console.log('ServiceWorker registration failed: ', err);
            });
          });
        }
        
        function showNotification(title, options) {
          if (Notification.permission === "granted") {
            var notification = new Notification(title, options);
          } else if (Notification.permission !== 'denied') {
            Notification.requestPermission().then(function(permission) {
              if (permission === "granted") {
                var notification = new Notification(title, options);
              }
            });
          }
        }
        
        window.onload = function() {
          <?php foreach ($notifications as $notification): ?>
          var title = "<?php echo $notification['origem']; ?>";
          var options = {
            body: "<?php echo $notification['ref']; ?>",
            // Adicione mais opções de notificação conforme necessário
          };
          showNotification(title, options);
          <?php endforeach; ?>
        };
      </script>

    </main>
    <!-- Page ends-->

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <ul class="nav nav-pills nav-justified">
                
                
              <li class="nav-item centerbutton">
                    <div class="nav-link">
                        <span class="theme-radial-gradient">
                    <i class="fa fa-times"></i> <!-- Ícone de fechar (X) do Font Awesome -->
                  </span>
                        <div class="nav-menu-popover justify-content-between">
                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('manual-technician.php');">
                                <i class="bi bi-credit-card size-32"></i><span>Tecnico</span>
                            </button>

                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('manual-user.php');">
                                <i class="bi bi-arrow-up-right-circle size-32"></i><span>Usuario</span>
                            </button>

                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('service.php');">
                                <i class="bi bi-receipt size-32"></i><span>Serviço</span>
                            </button>

                          <button type="button" class="btn btn-lg btn-icon-text"
                            onclick="window.location.replace('notification.php');">
                            <i class="bi bi-receipt size-32"></i><span>Notificações</span>
                          </button>
                          
                          
                           
                         <!--   <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('receivemoney.html');">
                                <i class="bi bi-arrow-down-left-circle size-32"></i><span>Receive</span>
                            </button> -->
                        </div>
                    </div>
                </li>
       <!--   <li class="nav-item centerbutton">
                <div class="nav-link">
                  <span class="theme-radial-gradient">
                    <i class="close bi bi-x"></i>
                    
                    <img src="../../assets/img/" class="nav-icon" alt="" />
                  </span>
                  <div class="nav-menu-popover justify-content-between">
                    
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('calendar.php');">
                      <i class="bi bi-receipt size-32"></i><span>Calendario</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('documentation.php');">
                      <i class="bi bi-receipt size-32"></i><span>Documentação</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('graduation.php');">
                      <i class="bi bi-receipt size-32"></i><span>Educação Continuada</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('dialogflow.php');">
                      <i class="bi bi-receipt size-32"></i><span>ChatBot</span>
                    </button>
                    
                     <button type="button" class="btn btn-lg btn-icon-text"
                    onclick="window.location.replace('receivemoney.html');">
                    <i class="bi bi-arrow-down-left-circle size-32"></i><span>Receive</span>
                    </button> 
                  </div>
                </div>
              </li> -->
              <li class="nav-item">
                <a class="nav-link " href="#">
                  <span>
                    <i class="nav-icon bi bi-house"></i>
                    <span class="nav-text">Câmera</span>
                  </span>
                </a>
              </li>
                
            </ul>
        </div>
    </footer>
  <!-- Camera Modal -->
  <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xsm modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="cammodalLabel">Camera</h6>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body text-center">
          <img src="../../assets/img/infographic-camera.png" alt="" class="mb-4">
          <script src="https://unpkg.com/html5-qrcode"></script>
          <div id="qr-reader"></div>
          <div id="qr-reader-results"></div>
          <script>
            var resultContainer = document.getElementById('qr-reader-results');
            var lastResult, countResults = 0;
            function extrairTexto(url) {
              var startIndex = url.indexOf("?") + 1; // Encontra o índice do caractere "?" e avança 1 para ignorá-lo
              var endIndex = url.indexOf("="); // Encontra o índice do caractere "="
              
              if (startIndex >= 0 && endIndex >= 0 && endIndex > startIndex) {
                return url.substring(startIndex, endIndex); // Extrai o texto entre "?" e "="
              } else {
                return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
              }
            }
            function extrairTextoid(urlid) {
              var startIndexid = urlid.indexOf("=") + 1; // Encontra o índice do caractere "=" e avança 1 para ignorá-lo
              var endIndexid = urlid.length; // O índice do fim da string
              
              if (startIndexid >= 0 && endIndexid > startIndexid) {
                return urlid.substring(startIndexid, endIndexid); // Extrai o texto após o "=" até o final da string
              } else {
                return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
              }
            }
            
            
            function onScanSuccess(decodedText, decodedResult) {
              if (decodedText !== lastResult) {
                ++countResults;
                lastResult = decodedText;
                // Handle on success condition with the decoded message.
                console.log(`Scan result ${decodedText}`, decodedResult);
                //  alert(`${decodedText}`, decodedResult);
                var textoExtraido = extrairTexto(decodedText);
                if (textoExtraido == "equipamento"){
                  var textoExtraidoid = extrairTextoid(decodedText);
                  window.location.href = `equipament-manual-user?id=`+textoExtraidoid;
                  
                }
                if (textoExtraido == "os"){
                  var textoExtraidoid = extrairTextoid(decodedText);
                  window.location.href = `corrective-maintenance-open?id=`+textoExtraidoid;
                  
                }
                if (textoExtraido == "id"){
                  var textoExtraidoid = extrairTextoid(decodedText);
                  window.location.href = `preventive-maintenance-open?id=`+textoExtraidoid;
                  
                }
                
              }
            }
            
            var html5QrcodeScanner = new Html5QrcodeScanner(
              "qr-reader", { fps: 10, qrbox: 250 });
            html5QrcodeScanner.render(onScanSuccess);
          </script>
          
        </div>
        
      </div>
    </div>
  </div>
  
  
  <!-- Camera Modal ends-->
    <!-- Footer ends-->

    <!-- PWA app install toast message -->
  <!--  <div class="position-fixed bottom-0 start-50 translate-middle-x  z-index-10">
        <div class="toast mb-3" role="alert" aria-live="assertive" aria-atomic="true" id="toastinstall"
            data-bs-animation="true">
            <div class="toast-header">
                <img src="../../assets/img/favicon32.png" class="rounded me-2" alt="...">
                <strong class="me-auto">Install PWA App</strong>
                <small>now</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                <div class="row">
                    <div class="col">
                        Click "Install" to install PWA app & experience indepedent.
                    </div>
                    <div class="col-auto align-self-center ps-0">
                        <button class="btn-default btn btn-sm" id="addtohome">Install</button>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- Camera Modal -->
    <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xsm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="cammodalLabel">Autorizar</h6>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                    <img src="assets/img/infographic-camera.png" alt="" class="mb-4">
                    <h3 class="text-color-theme mb-2">Camera Acesso</h3>
                    <p class="text-muted">QRCOD.
                    </p>
                </div>
                <button type="button" class="btn btn-lg btn-primary rounded-15" data-bs-dismiss="modal">Sempre</button>
            </div>
        </div>
    </div>
    <!-- Camera Modal ends-->
    <!-- Required jquery and libraries -->
    <script src="../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/popper.min.js"></script>
           <script src="../../assets/vendor/bootstrap-5/js/bootstrap.bundle.min.js"></script> 

    <!-- cookie js -->
    <script src="../../assets/js/jquery.cookie.js"></script>

    <!-- Customized jquery file  -->
    <script src="../../assets/js/main.js"></script>
    <script src="../../assets/js/color-scheme.js"></script>

    <!-- PWA app service registration and works -->
    <script src="../../assets/js/pwa-services.js"></script>

    <!-- Chart js script -->
            <script src="../../assets/vendor/chart-js-3.3.1/chart.min.js"></script>
 
    <!-- Progress circle js script -->
    <script src="../../assets/vendor/progressbar-js/progressbar.min.js"></script>

    <!-- swiper js script -->
    <script src="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.js"></script>

   <!-- page level custom script -->
            <script src="../../assets/js/app.js"></script> 

</body>

</html>
