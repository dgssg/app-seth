<?php
include("../database/database.php");

session_start();
if (!isset($_SESSION['usuario'])) {
header("Location: ../index.php");
}
if ($_SESSION['id_nivel'] != 1) {
header("Location: ../index.php");
}
if ($_SESSION['instituicao'] != $key) {
header("Location: ../index.php");
}
$usuariologado = $_SESSION['usuario'];
$setorlogado = $_SESSION['setor'];

$setor = $_SESSION['setor'];
$usuario = $_SESSION['id_usuario'];
$instituicao = $key;

$equipamento = $_POST['equipamento'];
$solicitacao = $_POST['solicitacao'];

$restrictions = $_POST['restrictions'];
  $anexo=$_COOKIE['anexo'];

$status = "1";
// tools
$query = "SELECT os_mp_1 FROM tools";

if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($posicionamento);
while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
}
}
//tools

$carimbo = "1";
$workflow = "1";
$os_open = "2";


$equipamento_query = trim($equipamento);
$username_exist = "0";

$query = "SELECT equipamento.id_instituicao_localizacao,equipamento.duplicidade,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id from equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade where equipamento.id like'$equipamento_query' ";

if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id_instituicao_localizacao, $duplicidade, $id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area, $unidade);
while ($stmt->fetch()) {
$unidade = $unidade;
}
}

if ($duplicidade == "0") {

$result = "SELECT * FROM os WHERE id_equipamento like '$equipamento_query' ";
$resultado = mysqli_query($conn, $result);
//contas os resultados
$username_exist = mysqli_num_rows($resultado);

if ($username_exist) {
echo "<script>alert('Solicitação não aberta para o equipamento selecionado, pelo motivo de duplicidade de O.S');document.location='../corrective-maintenance'</script>";

$username_exist = 1;
}
}
if ($username_exist == 0) {
// Início do script de upload de arquivo
date_default_timezone_set('America/Sao_Paulo');
$arquivo = date('Y-m-d_H-i-s'); // Formato de arquivo simples

$ds = DIRECTORY_SEPARATOR;
  $storeFolder = 'seth.mksistemasbiomedicos.com.br/customer-ec/' . $key . '/dashboard/dropzone/os';


if (!empty($_FILES['file']['tmp_name'])) {
  $arquivo_com_extensao = $arquivo . '.' . pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
  
$tempFile = $_FILES['file']['tmp_name'];
$targetPath = dirname(__FILE__) . $ds . $storeFolder . $ds;
$targetFile = $targetPath . $arquivo_com_extensao;

if (move_uploaded_file($tempFile, $targetFile)) {
// Arquivo movido com sucesso
//echo "Arquivo enviado com sucesso!";
// Defina o nome do arquivo para uso posterior
$anexo = $arquivo_com_extensao;
} 
} 
// Fim do script de upload de arquivo

$stmt = $conn->prepare("INSERT INTO os (id_unidade, id_usuario, id_equipamento, id_setor, id_solicitacao, id_anexo, id_status, id_posicionamento,id_carimbo,id_workflow,restrictions,os_open) VALUES (?,?, ?,?, ?, ?, ?, ?, ?, ?, ?,?)");
$stmt->bind_param("ssssssssssss", $unidade, $usuario, $equipamento, $id_instituicao_localizacao, $solicitacao, $anexo, $status, $posicionamento, $carimbo, $workflow, $restrictions, $os_open);
$execval = $stmt->execute();
$last_id = $conn->insert_id;
$stmt->close();

$stmt = $conn->prepare("INSERT INTO regdate_os_carimbo (id_os, id_carimbo) VALUES (?, ?)");
$stmt->bind_param("ss", $last_id, $carimbo);
$execval = $stmt->execute();
$stmt->close();

$query = "SELECT workflow FROM tools";

if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($workflow);
while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
}
}
if ($workflow == "0") {

$query = "SELECT id,id_os_posicionamento FROM os_workflow where id_os_status = 1";

if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($workflow, $id_os_posicionamento);
while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
}
}

$stmt = $conn->prepare("INSERT INTO regdate_os_posicionamento (id_os, id_posicionamento, id_status) VALUES (?, ?, ?)");
$stmt->bind_param("sss", $last_id, $id_os_posicionamento, $status);
$execval = $stmt->execute();
$stmt->close();


	$stmt = $conn->prepare("INSERT INTO regdate_os_workflow (id_os, id_workflow) VALUES (?, ?)");
	$stmt->bind_param("ss",$last_id,$workflow);
	$execval = $stmt->execute();
	$stmt->close();

	setcookie('anexo', null, -1);
   
}
$query = "SELECT email FROM tools";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($email);
            while ($stmt->fetch()) {
              //printf("%s, %s\n", $solicitante, $equipamento);
            }
          }
		  if($email == "0"){

 $sql = "SELECT  id, id_usuario,id_equipamento,id_solicitacao  FROM os where os.id like '$last_id'  ";



 if ($stmt = $conn->prepare($sql)) {
         $stmt->execute();
      $stmt->bind_result($id,$usuario,$id_equipamento,$id_solicitacao);
      while ($stmt->fetch()) {
      $id=$id;
      $usuario=$usuario;
      $id_equipamento=$id_equipamento;
      $id_solicitacao=$id_solicitacao;
     }
 }
 $sql="SELECT nome,email,telefone FROM usuario where id like '$usuario'";
 if ($stmt = $conn->prepare($sql)) {
         $stmt->execute();
      $stmt->bind_result($primeironome,$email,$telefone);
      while ($stmt->fetch()) {
      $primeironome=$primeironome;
      $email=$email;
      $telefone=$telefone;
     }
 }
 $sql="SELECT equipamento.codigo,equipamento_familia.nome, equipamento_familia.modelo FROM equipamento INNER JOIN equipamento_familia ON equipamento.id = equipamento_familia.id where equipamento.id like '$id_equipamento'";
 if ($stmt = $conn->prepare($sql)) {
         $stmt->execute();
      $stmt->bind_result($equipamento,$modelo,$codigo);
      while ($stmt->fetch()) {
      $equipamento=$equipamento;
      $modelo=$modelo;
      $codigo=$codigo;
     }
 }
 
 $descricao="Ordem: $os\nStatus: Recebido\n Solicitante: $primeironome-$email\nEquipamento: $equipamento-$modelo-$codigo \n Solicitação: $id_solicitacao \n\n\n\n\n  -------------------------------------------------------------------
 Mensagem automática enviada via sistema. Não responda.";
 $ordem="Ordem de Serviço $os";
 // send email
 mail("$emailadministrador","$ordem",$descricao);
 mail("$email","$ordem",$descricao);
 
		  }
 
}
echo "<script>alert('Aberta Solicitação!');document.location='../corrective-maintenance'</script>";
 

?>
