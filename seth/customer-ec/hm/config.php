<?php
include("database/database.php");
//include("../../codigo.php");
//$codigoget = (int)($_GET["codigoget"]);
$os = (int)($_GET["id"]);

	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
    setcookie('file', null, -1);

	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$email=	$_SESSION['email'] ;
    $file = "preventive-maintenance";
    setcookie("file","$file");
    $query = "SELECT os_posicionamento.status,os.id_servico,os.id_category,os.restrictions,os.sla,equipamento.data_val,os.sla_term,os.standing,os.time_backlog,os.id_fornecedor,os.id_prioridade,os.id_observacao, os.id_chamado,os.id_tecnico,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo FROM os INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN usuario on usuario.id = os.id_usuario INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor INNER JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area INNER JOIN os_status on os_status.id = os.id_status INNER JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento INNER JOIN os_carimbo ON os_carimbo.id = os.id_carimbo INNER JOIN os_workflow on os_workflow.id = os.id_workflow INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia where os.id like '$os'";
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($posicionamento,$id_service,$id_category,$restrictions,$sla_second,$data_val,$sla_term,$standing,$time_backlog,$id_fornecedor,$id_prioridade,$id_observacao,$id_chamado,$id_tecnico,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo);
    
    
    
      while ($stmt->fetch()) {
    
      }
    }

?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
      <title>MK Sistema Biomedicos </title>
  <!-- Link para o Font Awesome (se ainda não estiver carregado no projeto) -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <!-- manifest meta -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="manifest" href="../../manifest.json" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="../../assets/img/favicon180.png" sizes="180x180">
    <link rel="icon" href="../../assets/img/favicon32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="../../assets/img/favicon16.png" sizes="16x16" type="image/png">

    <!-- Google fonts-->

    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&amp;display=swap" rel="stylesheet">

    <!-- bootstrap icons -->
    <link rel="stylesheet" href="../../../../../../cdn.jsdelivr.net/npm/bootstrap-icons%401.5.0/font/bootstrap-icons.css">

    <!-- swiper carousel css -->
    <link rel="stylesheet" href="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.css">

    <!-- style css for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet" id="style">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/signature_pad/1.5.3/signature_pad.min.js"></script>
      
</head>

<body class="body-scroll" data-page="index">

    <!-- loader section -->
    <div class="container-fluid loader-wrap">
        <div class="row h-100">
            <div class="col-10 col-md-6 col-lg-5 col-xl-3 mx-auto text-center align-self-center">
                <div class="loader-cube-wrap loader-cube-animate mx-auto">
                    <img src="../../../framework/img/64x64.png" alt="Logo">
                </div>
                <p class="mt-4">Carregando<br><strong>Aguarde...</strong></p>
            </div>
        </div>
    </div>
    <!-- loader section ends -->

    <!-- Sidebar main menu -->
    <div class="sidebar-wrap  sidebar-pushcontent">
        <!-- Add overlay or fullmenu instead overlay -->
        <div class="closemenu text-muted">Fechar Menu</div>
        <div class="sidebar dark-bg">
            <!-- user information -->
            <div class="row my-3">
                <div class="col-12 ">
                    <div class="card shadow-sm bg-opac text-white border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <figure class="avatar avatar-44 rounded-15">
                                        <img src="https://seth.mksistemasbiomedicos.com.br/customer-ec/<?php printf($instituicaologado); ?>/logo/clientelogo.png" alt="">
                                    </figure>
                                </div>
                                <div class="col px-0 align-self-center">
                                    <p class="mb-1"> <?php printf($usuariologado); ?></p>
                                    <p class="text-muted size-12"><?php printf($setorlogado); ?></p>
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-44 btn-light">
                                        <i class="bi bi-box-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-opac text-white border-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <h1 class="display-4"></h1>
                                    </div>
                                    <div class="col-auto">
                                        <p class="text-muted"></p>
                                    </div>
                                    <div class="col text-end">
                                        <p class="text-muted"><a href="#" ></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- user emnu navigation -->
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="dashboard">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Dashboard</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="corrective-maintenance">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manutenção Corretiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="corrective-maintenance-new">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Nova Manutenção Corretiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="preventive-maintenance">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manutenção Preventiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-technician">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Tecnico </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-user">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Usuario </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="service">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Serviço </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="config">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Configurações </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>

                    








                        <li class="nav-item">
                            <a class="nav-link" href="../../signin.html" tabindex="-1">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-box-arrow-right"></i></div>
                                <div class="col">Sair</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Sidebar main menu ends -->

    <!-- Begin page -->
    <main class="h-100">

        <!-- Header -->
        <header class="header position-fixed">
            <div class="row">
                <div class="col-auto">
                    <a href="../../javascript:void(0)" target="_self" class="btn btn-light btn-44 menu-btn" >
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="col align-self-center text-center">
                    <div class="logo-small">
                        <img src="../../../framework/img/64x64.png" alt="Logo">
                        <h5>Sistema SETH</h5>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="#" target="_self" class="btn btn-light btn-44">
                        <i class="fa fa-bell"></i>
                        <span class="count-indicator"></span>
                    </a>
                </div>
            </div>
        </header>
        <!-- Header ends -->
        <div class="row mb-4">
                <div class="col-12 text-center">
                    <img src="../../assets/img/infographic-scan.png" alt="">
                </div>
            </div>
 
        <!-- main page content -->
         <!-- main page content -->
         <div class="main-container container">
            <p class="text-muted text-center mb-4">Selecione as melhores combinações de esquema de cores para sua marca e conceitos de aplicação.
                Todos disponíveis em modo de duas cores de layout, modo claro e escuro. Também altere as imagens de fundo das telas e
                estilos de menu.</p>

            <!-- layout modes selection -->
            <div class="row mb-3">
                <div class="col-12">
                    <h6>Layout Mode</h6>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-6 d-grid">
                    <input type="radio" class="btn-check" name="layout-mode" checked id="btn-layout-modes-light">
                    <label class="btn btn-warning shadow-sm text-white" for="btn-layout-modes-light">
                        <i class="bi bi-sun fs-4 mb-2 d-block"></i>
                        Light Mode</label>
                </div>
                <div class="col-6 d-grid">
                    <input type="radio" class="btn-check" name="layout-mode" id="btn-layout-modes-dark">
                    <label class="btn btn-dark shadow-sm" for="btn-layout-modes-dark">
                        <i class="bi bi-moon-stars fs-4 mb-2 d-block"></i>
                        Dark Mode</label>
                </div>
            </div>

            <!-- color scheme selection -->
            <div class="row mb-3">
                <div class="col-12">
                    <h6>Color scheme</h6>
                </div>
            </div>

            <div class="row mb-3">                
                <div class="col-12">
                
                    <input type="radio" class="btn-check mb-2" name="color-scheme" id="btn-color-blue" data-title="">
                    <label class="btn bg-blue shadow-sm avatar avatar-44 p-0 mb-3 me-2 text-white" for="btn-color-blue">BL</label>
                
                    <input type="radio" class="btn-check mb-2" name="color-scheme" id="btn-color-indigo" data-title="theme-indigo">
                    <label class="btn bg-indigo shadow-sm avatar avatar-44 p-0 mb-3 me-2 text-white" for="btn-color-indigo">IN</label>
               
                    <input type="radio" class="btn-check mb-2" name="color-scheme" id="btn-color-purple" data-title="theme-purple">
                    <label class="btn bg-purple shadow-sm avatar avatar-44 p-0 mb-3 me-2 text-white" for="btn-color-purple">PL</label>
               
                    <input type="radio" class="btn-check mb-2" name="color-scheme" checked id="btn-color-pink" data-title="theme-pink">
                    <label class="btn bg-pink shadow-sm avatar avatar-44 p-0 mb-3 me-2 text-white" for="btn-color-pink">PK</label>
               
                    <input type="radio" class="btn-check mb-2" name="color-scheme" id="btn-color-red" data-title="theme-red">
                    <label class="btn bg-red shadow-sm avatar avatar-44 p-0 mb-3 me-2 text-white" for="btn-color-red">RD</label>
               
                    <input type="radio" class="btn-check mb-2" name="color-scheme" id="btn-color-orange" data-title="theme-orange">
                    <label class="btn bg-orange shadow-sm avatar avatar-44 p-0 mb-3 me-2 text-white" for="btn-color-orange">OG</label>
              
                    <input type="radio" class="btn-check mb-2" name="color-scheme" id="btn-color-yellow" data-title="theme-yellow">
                    <label class="btn bg-yellow shadow-sm avatar avatar-44 p-0 mb-3 me-2 text-white" for="btn-color-yellow">YL</label>
               
                    <input type="radio" class="btn-check mb-2" name="color-scheme" id="btn-color-green" data-title="theme-green">
                    <label class="btn bg-green shadow-sm avatar avatar-44 p-0 mb-3 me-2 text-white" for="btn-color-green">GN</label>
                
                    <input type="radio" class="btn-check mb-2" name="color-scheme" id="btn-color-teal" data-title="theme-teal">
                    <label class="btn bg-teal shadow-sm avatar avatar-44 p-0 mb-3 me-2 text-white" for="btn-color-teal">TL</label>
               
                    <input type="radio" class="btn-check mb-2" name="color-scheme" id="btn-color-cyan" data-title="theme-cyan">
                    <label class="btn bg-cyan shadow-sm avatar avatar-44 p-0 mb-3 me-2 text-white" for="btn-color-cyan">CN</label>
                </div>
            </div>

            <!-- layout modes selection -->
            <div class="row mb-3">
                <div class="col-12">
                    <h6>RTL -LTR Layout</h6>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-6 d-grid">
                    <input type="radio" class="btn-check" name="layout-mode" id="btn-ltr" checked>
                    <label class="btn btn-outline-primary shadow-sm" for="btn-ltr">
                        <i class="bi bi-text-left fs-4 mb-2 d-block"></i>
                        Left to Right</label>
                </div>
                <div class="col-6 d-grid">
                    <input type="radio" class="btn-check" name="layout-mode" id="btn-rtl">
                    <label class="btn btn-outline-primary shadow-sm" for="btn-rtl">
                        <i class="bi bi-text-right fs-4 mb-2 d-block"></i>
                        Right to Left</label>
                </div>
            </div>

            <!-- menu style selection -->
            <div class="row mb-3">
                <div class="col-12">
                    <h6 class="mb-2">Menu Style</h6>
                    <p class="text-muted size-12">Selecione o estilo da barra lateral do menu e abra o menu no botão do menu superior.</p>
                </div>
            </div>

            <div class="row ">
                <div class="col mb-4">
                    <input type="radio" class="btn-check" name="menu-select" id="btn-menu1"
                        data-title="overlay">
                    <label class="btn btn-outline-primary background-btn p-1 text-center border-0" for="btn-menu1">
                        <img src="assets/img/setting-menu-1%402x.png" alt="" class="mw-100 rounded-10"><br><span
                            class="py-2 d-block small">Popover</span>
                    </label>
                </div>
                <div class="col mb-4 ps-0">
                    <input type="radio" class="btn-check" name="menu-select" checked id="btn-menu2" data-title="pushcontent">
                    <label class="btn btn-outline-primary background-btn p-1 text-center border-0" for="btn-menu2">
                        <img src="assets/img/setting-menu-2%402x.png" alt="" class="mw-100 rounded-10"><br><span
                            class="py-2 d-block small">Push Page</span>
                    </label>
                </div>
                <div class="col mb-4 ps-0">
                    <input type="radio" class="btn-check" name="menu-select" id="btn-menu3" data-title="fullmenu">
                    <label class="btn btn-outline-primary background-btn p-1 text-center border-0" for="btn-menu3">
                        <img src="assets/img/setting-menu-3%402x.png" alt="" class="mw-100 rounded-10"><br><span
                            class="py-2 d-block small">Fullscreen</span>
                    </label>
                </div>
            </div>

            <!-- background selection -->
            <div class="row mb-3">
                <div class="col-12">
                    <h6 class="mb-3">Background Image</h6>
                    <p class="text-muted size-12">As imagens de fundo são visíveis 
                    </p>
                </div>
            </div>

            <div class="row ">
                <div class="col mb-4">
                    <input type="radio" class="btn-check" name="background-select" checked id="btn-bg1"
                        data-src="backgorund-image.html">
                    <label class="btn btn-outline-primary background-btn p-1 text-center border-0" for="btn-bg1">
                        <img src="assets/img/darkbg-1.png" alt="" class="mw-100 rounded-10"><br><span
                            class="py-2 d-block small">Shapes</span>
                    </label>
                </div>
                <div class="col mb-4">
                    <input type="radio" class="btn-check" name="background-select" id="btn-bg2"
                        data-src="backgorund-image2.html">
                    <label class="btn btn-outline-primary background-btn p-1 text-center border-0" for="btn-bg2">
                        <img src="assets/img/darkbg-2.png" alt="" class="mw-100 rounded-10"><br><span
                            class="py-2 d-block small">Character</span>
                    </label>
                </div>
                <div class="col mb-4">
                    <input type="radio" class="btn-check" name="background-select" id="btn-bg3"
                        data-src="backgorund-image3.html">
                    <label class="btn btn-outline-primary background-btn p-1 text-center border-0" for="btn-bg3">
                        <img src="assets/img/darkbg-3.png" alt="" class="mw-100  rounded-10"><br><span
                            class="py-2 d-block small">Bubble</span>
                    </label>
                </div>
            </div>
      <form action="backend/signature-backend.php" method="post">
    <input type="hidden" id="assinaturaCliente" name="assinaturaCliente">
<h2>Assinatura:</h2>
            <canvas id="clienteCanvas" width="400" height="200" style="border:1px solid #000;"></canvas>
            <br>
           <button id="clearButton">Limpar Assinatura</button>
           
        </div>  <div class="modal-footer">
             
              <button type="submit" class="btn btn-primary">Salvar Informações</button>
            </div>
            
           
        </form>   
        <!-- main page content ends -->


    </main>
    <!-- Page ends-->
           

           

                </div>
                <div class="tab-pane fade" id="currency" role="tabpanel" aria-labelledby="currency-tab">
                

      

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <ul class="nav nav-pills nav-justified">
                
                
                <li class="nav-item centerbutton">
                    <div class="nav-link">
                        <span class="theme-radial-gradient">
                    <i class="fa fa-times"></i> <!-- Ícone de fechar (X) do Font Awesome -->
                  </span>
                        <div class="nav-menu-popover justify-content-between">
                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('manual-technician.php');">
                                <i class="bi bi-credit-card size-32"></i><span>Posicionamento</span>
                            </button>

                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('manual-user.php');">
                                <i class="bi bi-arrow-up-right-circle size-32"></i><span>Registro</span>
                            </button>

                            <button type="button" class="btn btn-lg btn-icon-text"
                              onclick="window.location.replace('service.php');">
                                <i class="bi bi-receipt size-32"></i><span>Fechar</span>
                            </button>

                         <!--   <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('receivemoney.html');">
                                <i class="bi bi-arrow-down-left-circle size-32"></i><span>Receive</span>
                            </button> -->
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#">
                        <span>
                            <i class="nav-icon bi bi-wallet2"></i>
                            <span class="nav-text">Camêra</span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </footer>
                  <!-- Camera Modal -->
                  <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xsm modal-dialog-centered">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h6 class="modal-title" id="cammodalLabel">Camera</h6>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body text-center">
                          <img src="../../assets/img/infographic-camera.png" alt="" class="mb-4">
                          <script src="https://unpkg.com/html5-qrcode"></script>
                          <div id="qr-reader"></div>
                          <div id="qr-reader-results"></div>
                          <script>
                            var resultContainer = document.getElementById('qr-reader-results');
                            var lastResult, countResults = 0;
                            function extrairTexto(url) {
                              var startIndex = url.indexOf("?") + 1; // Encontra o índice do caractere "?" e avança 1 para ignorá-lo
                              var endIndex = url.indexOf("="); // Encontra o índice do caractere "="
                              
                              if (startIndex >= 0 && endIndex >= 0 && endIndex > startIndex) {
                                return url.substring(startIndex, endIndex); // Extrai o texto entre "?" e "="
                              } else {
                                return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                              }
                            }
                            function extrairTextoid(urlid) {
                              var startIndexid = urlid.indexOf("=") + 1; // Encontra o índice do caractere "=" e avança 1 para ignorá-lo
                              var endIndexid = urlid.length; // O índice do fim da string
                              
                              if (startIndexid >= 0 && endIndexid > startIndexid) {
                                return urlid.substring(startIndexid, endIndexid); // Extrai o texto após o "=" até o final da string
                              } else {
                                return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                              }
                            }
                            
                            
                            function onScanSuccess(decodedText, decodedResult) {
                              if (decodedText !== lastResult) {
                                ++countResults;
                                lastResult = decodedText;
                                // Handle on success condition with the decoded message.
                                console.log(`Scan result ${decodedText}`, decodedResult);
                                //  alert(`${decodedText}`, decodedResult);
                                var textoExtraido = extrairTexto(decodedText);
                                if (textoExtraido == "equipamento"){
                                  var textoExtraidoid = extrairTextoid(decodedText);
                                  window.location.href = `equipament-manual-user?id=`+textoExtraidoid;
                                  
                                }
                                if (textoExtraido == "os"){
                                  var textoExtraidoid = extrairTextoid(decodedText);
                                  window.location.href = `corrective-maintenance-open?id=`+textoExtraidoid;
                                  
                                }
                                if (textoExtraido == "id"){
                                  var textoExtraidoid = extrairTextoid(decodedText);
                                  window.location.href = `preventive-maintenance-open?id=`+textoExtraidoid;
                                  
                                }
                                
                              }
                            }
                            
                            var html5QrcodeScanner = new Html5QrcodeScanner(
                              "qr-reader", { fps: 10, qrbox: 250 });
                            html5QrcodeScanner.render(onScanSuccess);
                          </script>
                          
                        </div>
                        
                      </div>
                    </div>
                  </div>
                  
                  
                  <!-- Camera Modal ends-->
    <!-- Footer ends-->

    <!-- PWA app install toast message -->
  <!--  <div class="position-fixed bottom-0 start-50 translate-middle-x  z-index-10">
        <div class="toast mb-3" role="alert" aria-live="assertive" aria-atomic="true" id="toastinstall"
            data-bs-animation="true">
            <div class="toast-header">
                <img src="../../assets/img/favicon32.png" class="rounded me-2" alt="...">
                <strong class="me-auto">Install PWA App</strong>
                <small>now</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                <div class="row">
                    <div class="col">
                        Click "Install" to install PWA app & experience indepedent.
                    </div>
                    <div class="col-auto align-self-center ps-0">
                        <button class="btn-default btn btn-sm" id="addtohome">Install</button>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- Camera Modal -->
    <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xsm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="cammodalLabel">Autorizar</h6>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                    <img src="assets/img/infographic-camera.png" alt="" class="mb-4">
                    <h3 class="text-color-theme mb-2">Camera Acesso</h3>
                    <p class="text-muted">QRCOD.
                    </p>
                </div>
                <button type="button" class="btn btn-lg btn-primary rounded-15" data-bs-dismiss="modal">Sempre</button>
            </div>
        </div>
    </div>
    <!-- Camera Modal ends-->
    <!-- Required jquery and libraries -->
    <script src="../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/popper.min.js"></script>
           <script src="../../assets/vendor/bootstrap-5/js/bootstrap.bundle.min.js"></script> 

    <!-- cookie js -->
    <script src="../../assets/js/jquery.cookie.js"></script>

    <!-- Customized jquery file  -->
    <script src="../../assets/js/main.js"></script>
    <script src="../../assets/js/color-scheme.js"></script>

    <!-- PWA app service registration and works -->
    <script src="../../assets/js/pwa-services.js"></script>

    <!-- Chart js script -->
            <script src="../../assets/vendor/chart-js-3.3.1/chart.min.js"></script>
 
    <!-- Progress circle js script -->
    <script src="../../assets/vendor/progressbar-js/progressbar.min.js"></script>

    <!-- swiper js script -->
    <script src="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.js"></script>

   <!-- page level custom script -->
            <script src="../../assets/js/app.js"></script> 
 <script>
    var clienteCanvas = document.getElementById('clienteCanvas');
    var clienteSignaturePad = new SignaturePad(clienteCanvas);
    
    
 document.querySelector('#clearButton').addEventListener('click', () => {
  clienteSignaturePad.clear();
});

    
 document.querySelector('form').addEventListener('submit', (e) => {
  if (clienteSignaturePad.isEmpty()) {
    alert('Por favor, assine antes de enviar.');
    e.preventDefault(); // Impede o envio
  } else {
    const assinatura = clienteSignaturePad.toDataURL();
    document.querySelector('#assinaturaCliente').value = assinatura;
  }
});


</script>


 

</body>

</html>
