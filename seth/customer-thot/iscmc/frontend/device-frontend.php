<?php
include("database/database.php");
$query = "SELECT sensor.id, sensor.macadress, sensor_type.nome as 'id_sensor_type', sensor.nome, sensor.id_area, sensor_status.nome as 'id_status', sensor.up_time, sensor.file, sensor.reg_date, sensor.upgrade, sensor.bat, sensor.last_alarm, sensor.trash, sensor_alarm.nome as 'id_alarm', sensor.imei, sensor.firmware, sensor.linha, sensor.iccid, sensor.id_location, sensor_usb.nome as 'usb', sensor_charger.nome as 'charger', sensor.ativo, sensor.dados, sensor.id_dados,area.nome as 'area',setor.nome as 'setor',unidade.unidade FROM sensor LEFT JOIN sensor_type ON sensor_type.id =  sensor.id_sensor_type LEFT JOIN area ON area.id = sensor.id_area LEFT join setor ON setor.id = area.id_setor LEFT JOIN unidade ON unidade.id = setor.id_unidade LEFT JOIN sensor_usb ON sensor_usb.id =  sensor.usb LEFT JOIN sensor_charger ON sensor_charger.id =  sensor.charger LEFT JOIN sensor_status ON sensor_status.id = sensor.id_status LEFT JOIN sensor_alarm ON sensor_alarm.id = sensor.id_alarm ORDER by sensor.id DESC";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result(
       $id,
$macadress,
$id_sensor_type,
$nome,
$id_area,
$id_status,
$up_time,
$file,
$reg_date,
$upgrade,
$bat,
$last_alarm,
$trash,
$id_alarm,
$imei,
$firmware,
$linha,
$iccid,
$id_location,
$usb,
$charger,
$ativo,
$dados,
$id_dados,
$area,
$setor,
$unidade
       );
 
  ?>
  <style>
  * {
    box-sizing: border-box;
  }

  #myInput {

    background-position: 10px 10px;
    background-repeat: no-repeat;
    width: 100%;
    font-size: 16px;
    padding: 12px 20px 12px 40px;
    border: 1px solid #ddd;
    margin-bottom: 12px;
  }

  #myTable {
    border-collapse: collapse;
    width: 100%;
    border: 1px solid #ddd;
    font-size: 18px;
  }

  #myTable th, #myTable td {
    text-align: left;
    padding: 12px;
  }

  #myTable tr {
    border-bottom: 1px solid #ddd;
  }

  #myTable tr.header, #myTable tr:hover {
    background-color: #f1f1f1;
  }
    .custom-black-button {
      background-color: black;
      color: white; /* Altere a cor do texto conforme necessário */
    }

</style>



<h2>Dispositivo</h2>



<div class="row">
  <div class="col-12">
    <input type="text" id="filtro" placeholder="Digite">
    <div class="card shadow-sm mb-4">
      <ul id="lista-usuarios" class="list-group list-group-flush bg-none">
        <?php   while ($stmt->fetch()) {  
        if($id_status === "Offline"){
        $color = "red";
        }else{
        $color = "green";

        }
         ?>
        
        <style>
          .custom-bg-color {
            background-color: <?php echo $color; ?>; /* Use a variável PHP $color aqui */
          }
        </style>
        <div id="box_<?php echo $id; ?>">
          <li class="list-group-item">
            <div class="row">
              <div class="col-auto">
                
                <figure class="avatar avatar-50 rounded-10 shadow-sm">
                  <img src="" alt="<?php printf($macadress); ?>" >
                </figure>
              </div>
              <div class="col px-0">
                <p><?php printf($nome); ?>  <br><small class="text-muted"><?php printf($unidade); ?> - <?php printf($setor); ?></small></p>
              </div>
              <div class="col-auto text-end">
                <p><small class="text-muted size-12"><?php printf($dados); ?>   
                <?php    
        if($id_status === "Offline"){
echo "<span class='avatar avatar-6 rounded-circle bg-danger  d-inline-block'>"; 
       }
               if($id_status === "Online"){

echo "<span class='avatar avatar-6 rounded-circle bg-success  d-inline-block'>"; 
        }
         ?></span></small></p>
              </div>
            </div>
          </li>
          <script>
            // Selecionando o elemento para aplicar o swipe
            var box = document.getElementById('box_<?php echo $id; ?>');
            
            // Criando um objeto Hammer para o elemento
            var hammer = new Hammer(box);
            // Adicionando um ouvinte de evento para detectar o gesto de tap (toque)
            hammer.on('tap', function(event) {
            //  alert('Tap (Toque) detectado!');
              window.location.href = 'device-info?id=<?php echo $id; ?>';
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de double tap (toque duplo)
            hammer.on('doubletap', function(event) {
              alert('Double Tap (Toque Duplo) detectado!');
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de pinch (beliscar)
            hammer.on('pinch', function(event) {
              alert('Pinch (Beliscar) detectado!');
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de rotate (rotacionar)
            hammer.on('rotate', function(event) {
              alert('Rotate (Rotacionar) detectado!');
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de long press (pressão longa)
            hammer.on('press', function(event) {
              alert('Sensor: <?php echo $macadress; ?>\nUnidade: <?php printf($unidade); ?>\nSetor: <?php printf($setor); ?>\nArea: <?php printf($area); ?>\nStatus: <?php printf($id_status); ?>\nUp Time: <?php printf($up_time); ?>\nAlarm: <?php printf($id_alarm); ?>');

                           
            });
            // Adicionando um ouvinte de evento para detectar o gesto swipe
            hammer.on('swipe', function(event) {
              // Verificando a direção do swipe e exibindo uma mensagem correspondente
              switch(event.direction) {
                case Hammer.DIRECTION_UP:
                  alert('Swipe para cima detectado!');
                  break;
                case Hammer.DIRECTION_DOWN:
                  alert('Swipe para baixo detectado!');
                  break;
                case Hammer.DIRECTION_LEFT:
                  //alert('Swipe para esquerda detectado!');
                  window.location.href = 'device-alarm?id=<?php echo $id; ?>';
                  break;
                case Hammer.DIRECTION_RIGHT:
                  //alert('Swipe para direita detectado!');
                  // Redirecionando a página
                  window.location.href = 'device-iot?id=<?php echo $id; ?>';


                  break;
              }
            });
          </script> 
        </div>
        <?php   } }  ?>

        <!-- Adicione mais itens de lista conforme necessário -->
      </ul>
    </div>
  </div>
</div>

<script>
  document.getElementById("filtro").addEventListener("input", function () {
    var filtro = this.value.toLowerCase();
    var listaUsuarios = document.getElementById("lista-usuarios").getElementsByTagName("li");
    
    for (var i = 0; i < listaUsuarios.length; i++) {
      var nome = listaUsuarios[i].getElementsByTagName("p")[0].innerText.toLowerCase();
      var cargo = listaUsuarios[i].getElementsByTagName("small")[0].innerText.toLowerCase();
      
      if (nome.includes(filtro) || cargo.includes(filtro)) {
        listaUsuarios[i].style.display = "";
      } else {
        listaUsuarios[i].style.display = "none";
      }
    }
  });
</script>
