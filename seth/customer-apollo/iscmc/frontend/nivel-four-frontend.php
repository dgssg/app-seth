<?php
$nivel = "4";
include("database/database.php");
$codigoget = 5961;
$id_device = 3;
session_start();
$id_usuario=$_SESSION['id_usuario'];
?>


<script src="frontend/mqttws31.js" type="text/javascript"></script>
  <script src="frontend/jquery.min.js" type="text/javascript"></script>
  <script src="frontend/config.js" type="text/javascript"></script>
  <?php if($nivel == 1) { ?>
  <script type="text/javascript">
  var mqtt;
  var reconnectTimeout = 2000;

  function MQTTconnect() {
    if (typeof path == "undefined") {
      path = '/mqtt';
    }
    mqtt = new Paho.MQTT.Client(
      host,
      port,
      path,
      "web_" + parseInt(Math.random() * 100, 10)
    );



    var options = {
      timeout: 3,
      useSSL: useTLS,
      cleanSession: cleansession,
      onSuccess: onConnect,
      onFailure: function (message) {
        $('#status').val("Connection failed: " + message.errorMessage + "Retrying");
        setTimeout(MQTTconnect, reconnectTimeout);
      }
    };

    mqtt.onConnectionLost = onConnectionLost;
    mqtt.onMessageArrived = onMessageArrived;


    if (username != null) {
      options.userName = username;
      options.password = password;
    }
    console.log("Host="+ host + ", port=" + port + ", path=" + path + " TLS = " + useTLS + " username=" + username + " password=" + password);
    mqtt.connect(options);
  }

  function onConnect() {
    $('#status').val('Connected to ' + host + ':' + port + path);
    // Connection succeeded; subscribe to our topic


        //use the below if you want to publish to a topic on connect
        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r1_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r4_in_5961;
        mqtt.send(message);

        //use the below if you want to publish to a topic on connect
        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8853;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8853;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8853;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r1_in_8853;
        mqtt.send(message);

       

         //Placa 8975
         message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8975;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8975;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8975;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r4_in_8975;
        mqtt.send(message);


        //Placa 8971
        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8971;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8971;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8971;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r4_in_8971;
        mqtt.send(message);


        //Placa 8859
        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8859;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8859;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8859;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r4_in_8859;
        mqtt.send(message);


        //Placa 8972
        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8972;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8972;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8972;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r4_in_8972;
        mqtt.send(message);





  }

  function onConnectionLost(response) {
    setTimeout(MQTTconnect, reconnectTimeout);
    $('#status').val("connection lost: " + responseObject.errorMessage + ". Reconnecting");

  };

  function onMessageArrived(message) {

    var topic = message.destinationName;
    var payload = message.payloadString;



    $('#ws').prepend('<li>' + topic + ' = ' + payload + '</li>');


  };


  $(document).ready(function() {
    MQTTconnect();

  });

  </script>
  <?php
  //  sleep(5);
    $id_nivel_1=1;
    $id_nivel_2=2;
    $id_nivel_3=3;
    $id_nivel_4=4;
    $on="ON";
    $off="OFF";

    $stmt = $conn->prepare("UPDATE device SET id_nivel = ? ");
    $stmt->bind_param("s",  $id_nivel_1);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("INSERT INTO report (id_nivel,id_device,id_user ) VALUES (?,?,? )");
    $stmt->bind_param("sss",$id_nivel_1,$id_device,$id_usuario );
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r1 = ? ");
    $stmt->bind_param("s",  $on);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r2 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r3 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r4 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();


//echo "<script>alert('Atualizado!');document.location='../../dashboard'</script>";

     ?>

<?php } ?>
<?php if($nivel == 2) { ?>
    <script type="text/javascript">
  var mqtt;
  var reconnectTimeout = 2000;

  function MQTTconnect() {
    if (typeof path == "undefined") {
      path = '/mqtt';
    }
    mqtt = new Paho.MQTT.Client(
      host,
      port,
      path,
      "web_" + parseInt(Math.random() * 100, 10)
    );



    var options = {
      timeout: 3,
      useSSL: useTLS,
      cleanSession: cleansession,
      onSuccess: onConnect,
      onFailure: function (message) {
        $('#status').val("Connection failed: " + message.errorMessage + "Retrying");
        setTimeout(MQTTconnect, reconnectTimeout);
      }
    };

    mqtt.onConnectionLost = onConnectionLost;
    mqtt.onMessageArrived = onMessageArrived;


    if (username != null) {
      options.userName = username;
      options.password = password;
    }
    console.log("Host="+ host + ", port=" + port + ", path=" + path + " TLS = " + useTLS + " username=" + username + " password=" + password);
    mqtt.connect(options);
  }

  function onConnect() {
    $('#status').val('Connected to ' + host + ':' + port + path);
    // Connection succeeded; subscribe to our topic


        //use the below if you want to publish to a topic on connect
        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r2_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r4_in_5961;
        mqtt.send(message);


  }

  function onConnectionLost(response) {
    setTimeout(MQTTconnect, reconnectTimeout);
    $('#status').val("connection lost: " + responseObject.errorMessage + ". Reconnecting");

  };

  function onMessageArrived(message) {

    var topic = message.destinationName;
    var payload = message.payloadString;



    $('#ws').prepend('<li>' + topic + ' = ' + payload + '</li>');


  };


  $(document).ready(function() {
    MQTTconnect();

  });

  </script>
 <?php
  //  sleep(5);
    $id_nivel_1=1;
    $id_nivel_2=2;
    $id_nivel_3=3;
    $id_nivel_4=4;

    $stmt = $conn->prepare("UPDATE device SET id_nivel = ? ");
    $stmt->bind_param("s",  $id_nivel_2);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("INSERT INTO report (id_nivel,id_device,id_user ) VALUES (?,?,? )");
    $stmt->bind_param("sss",$id_nivel_2,$id_device,$id_usuario );
    $execval = $stmt->execute();
    $stmt->close();

    $on="ON";
    $off="OFF";

    $stmt = $conn->prepare("UPDATE device SET r1 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r2 = ?");
    $stmt->bind_param("s",  $on);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r3 = ?");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r4 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();


// echo "<script>alert('Atualizado!');document.location='../../dashboard'</script>";

     ?>
     <?php } ?>
<?php if($nivel == 3) { ?>
    <script type="text/javascript">
  var mqtt;
  var reconnectTimeout = 2000;

  function MQTTconnect() {
    if (typeof path == "undefined") {
      path = '/mqtt';
    }
    mqtt = new Paho.MQTT.Client(
      host,
      port,
      path,
      "web_" + parseInt(Math.random() * 100, 10)
    );



    var options = {
      timeout: 3,
      useSSL: useTLS,
      cleanSession: cleansession,
      onSuccess: onConnect,
      onFailure: function (message) {
        $('#status').val("Connection failed: " + message.errorMessage + "Retrying");
        setTimeout(MQTTconnect, reconnectTimeout);
      }
    };

    mqtt.onConnectionLost = onConnectionLost;
    mqtt.onMessageArrived = onMessageArrived;


    if (username != null) {
      options.userName = username;
      options.password = password;
    }
    console.log("Host="+ host + ", port=" + port + ", path=" + path + " TLS = " + useTLS + " username=" + username + " password=" + password);
    mqtt.connect(options);
  }

  function onConnect() {
    $('#status').val('Connected to ' + host + ':' + port + path);
    // Connection succeeded; subscribe to our topic


        //use the below if you want to publish to a topic on connect
        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r3_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r4_in_5961;
        mqtt.send(message);


  }

  function onConnectionLost(response) {
    setTimeout(MQTTconnect, reconnectTimeout);
    $('#status').val("connection lost: " + responseObject.errorMessage + ". Reconnecting");

  };

  function onMessageArrived(message) {

    var topic = message.destinationName;
    var payload = message.payloadString;



    $('#ws').prepend('<li>' + topic + ' = ' + payload + '</li>');


  };


  $(document).ready(function() {
    MQTTconnect();

  });

  </script>
 <?php
  //sleep(5);
    $id_nivel_1=1;
    $id_nivel_2=2;
    $id_nivel_3=3;
    $id_nivel_4=4;
    $stmt = $conn->prepare("UPDATE device SET id_nivel = ? ");
    $stmt->bind_param("s",  $id_nivel_3);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("INSERT INTO report (id_nivel,id_device,id_user ) VALUES (?,?,? )");
    $stmt->bind_param("sss",$id_nivel_3,$id_device,$id_usuario );
    $execval = $stmt->execute();
    $stmt->close();

    $on="ON";
    $off="OFF";

    $stmt = $conn->prepare("UPDATE device SET r1 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r2 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r3 = ? ");
    $stmt->bind_param("s",  $on);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r4 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();
//echo "<script>alert('Atualizado!');document.location='../../dashboard'</script>";

     ?>

    <?php } ?>
<?php if($nivel == 4) { ?>

    <script type="text/javascript">
  var mqtt;
  var reconnectTimeout = 2000;

  function MQTTconnect() {
    if (typeof path == "undefined") {
      path = '/mqtt';
    }
    mqtt = new Paho.MQTT.Client(
      host,
      port,
      path,
      "web_" + parseInt(Math.random() * 100, 10)
    );



    var options = {
      timeout: 3,
      useSSL: useTLS,
      cleanSession: cleansession,
      onSuccess: onConnect,
      onFailure: function (message) {
        $('#status').val("Connection failed: " + message.errorMessage + "Retrying");
        setTimeout(MQTTconnect, reconnectTimeout);
      }
    };

    mqtt.onConnectionLost = onConnectionLost;
    mqtt.onMessageArrived = onMessageArrived;


    if (username != null) {
      options.userName = username;
      options.password = password;
    }
    console.log("Host="+ host + ", port=" + port + ", path=" + path + " TLS = " + useTLS + " username=" + username + " password=" + password);
    mqtt.connect(options);
  }

  function onConnect() {
    $('#status').val('Connected to ' + host + ':' + port + path);
    // Connection succeeded; subscribe to our topic


        //use the below if you want to publish to a topic on connect
        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r4_in_5961;
        mqtt.send(message);


        //use the below if you want to publish to a topic on connect
        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8853;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8853;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8853;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r4_in_8853;
        mqtt.send(message);


        //use the below if you want to publish to a topic on connect
        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8975;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8975;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8975;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r4_in_8975;
        mqtt.send(message);

                //use the below if you want to publish to a topic on connect
                message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8971;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8971;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8971;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r4_in_8971;
        mqtt.send(message);
                //use the below if you want to publish to a topic on connect
                message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8859;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8859;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8859;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r4_in_8859;
        mqtt.send(message);
                //use the below if you want to publish to a topic on connect
                message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in_8872;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8872;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8872;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r4_in_8872;
        mqtt.send(message);


  }

  function onConnectionLost(response) {
    setTimeout(MQTTconnect, reconnectTimeout);
    $('#status').val("connection lost: " + responseObject.errorMessage + ". Reconnecting");

  };

  function onMessageArrived(message) {

    var topic = message.destinationName;
    var payload = message.payloadString;



    $('#ws').prepend('<li>' + topic + ' = ' + payload + '</li>');


  };


  $(document).ready(function() {
    MQTTconnect();

  });

  </script>
 <?php
  //  sleep(5);
    $id_nivel_1=1;
    $id_nivel_2=2;
    $id_nivel_3=3;
    $id_nivel_4=4;
    $stmt = $conn->prepare("UPDATE device SET id_nivel = ? ");
    $stmt->bind_param("s",  $id_nivel_4);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("INSERT INTO report (id_nivel,id_device,id_user ) VALUES (?,?,? )");
    $stmt->bind_param("sss",$id_nivel_4,$id_device,$id_usuario );
    $execval = $stmt->execute();
    $stmt->close();

    $on="ON";
    $off="OFF";

    $stmt = $conn->prepare("UPDATE device SET r1 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r2 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r3 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r4 = ? ");
    $stmt->bind_param("s",  $on);
    $execval = $stmt->execute();
    $stmt->close();
//echo "<script>alert('Atualizado!');document.location='../../dashboard'</script>";

     ?>


    <?php } ?>

 <p class="glyphicon glyphicon-time" id="countdown"></p>   
    <script type="text/javascript">

// Total seconds to wait
var seconds = 5;

function countdown() {
  seconds = seconds - 1;
  if (seconds < 0) {
    // Chnage your redirection link here
    window.close();
  } else {
    // Update remaining seconds
    document.getElementById("countdown").innerHTML = seconds;
    // Count down using javascript
    window.setTimeout("countdown()", 1000);
  }
}

// Run countdown function
countdown();

</script>

  