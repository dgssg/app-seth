
<?php
include("database/database.php");
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
    setcookie('file', null, -1);

	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$email=	$_SESSION['email'] ;
    $file = "dashboard";
    setcookie("file","$file");
 if(isset($_GET['id']) && $_GET['id'] !== ""){
    // Se 'id' estiver definido e não vazio
    $codigoget = $_GET['id'];
    
    $query = "SELECT sensor.id, sensor.macadress, sensor_type.nome as 'id_sensor_type', sensor.nome, sensor.id_area, sensor.id_status, sensor.up_time, sensor.file, sensor.reg_date, sensor.upgrade, sensor.bat, sensor.last_alarm, sensor.trash, sensor.id_alarm, sensor.imei, sensor.firmware, sensor.linha, sensor.iccid, sensor.id_location, sensor_usb.nome as 'usb', sensor_charger.nome as 'charger', sensor.ativo, sensor.dados, sensor.id_dados,area.nome as 'area',setor.nome as 'setor',unidade.unidade FROM sensor LEFT JOIN sensor_type ON sensor_type.id =  sensor.id_sensor_type LEFT JOIN area ON area.id = sensor.id_area LEFT join setor ON setor.id = area.id_setor LEFT JOIN unidade ON unidade.id = setor.id_unidade LEFT JOIN sensor_usb ON sensor_usb.id =  sensor.usb LEFT JOIN sensor_charger ON sensor_charger.id =  sensor.charger WHERE sensor.id = $codigoget";
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($id,$macadress,$id_sensor_type, $nome, $id_area, $id_status,$up_time, $file, $reg_date,$upgrade, $bat, $last_alarm, $trash, $id_alarm, $imei, $firmware, $linha, $iccid, $id_location, $usb,$charger, $ativo, $dados, $id_dados,$area,$setor,$unidade);
      while ($stmt->fetch()) {
        
      }
    }
    }
    
?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
      <title>MK Sistema Biomedicos </title>
  <!-- Link para o Font Awesome (se ainda não estiver carregado no projeto) -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <!-- manifest meta -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="manifest" href="../../manifest.json" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="../../assets/img/favicon180.png" sizes="180x180">
    <link rel="icon" href="../../assets/img/favicon32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="../../assets/img/favicon16.png" sizes="16x16" type="image/png">

    <!-- Google fonts-->

    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&amp;display=swap" rel="stylesheet">

    <!-- bootstrap icons -->
    <link rel="stylesheet" href="../../../../../../cdn.jsdelivr.net/npm/bootstrap-icons%401.5.0/font/bootstrap-icons.css">

    <!-- swiper carousel css -->
    <link rel="stylesheet" href="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.css">

    <!-- style css for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet" id="style">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</head>

<body class="body-scroll" data-page="index">

    <!-- loader section -->
    <div class="container-fluid loader-wrap">
        <div class="row h-100">
            <div class="col-10 col-md-6 col-lg-5 col-xl-3 mx-auto text-center align-self-center">
                <div class="loader-cube-wrap loader-cube-animate mx-auto">
                    <img src="../../../framework/img/64x64.png" alt="Logo">
                </div>
                <p class="mt-4">Carregando<br><strong>Aguarde...</strong></p>
            </div>
        </div>
    </div>
    <!-- loader section ends -->

    <!-- Sidebar main menu -->
    <div class="sidebar-wrap  sidebar-pushcontent">
        <!-- Add overlay or fullmenu instead overlay -->
        <div class="closemenu text-muted">Fechar Menu</div>
        <div class="sidebar dark-bg">
            <!-- user information -->
            <div class="row my-3">
                <div class="col-12 ">
                    <div class="card shadow-sm bg-opac text-white border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <figure class="avatar avatar-44 rounded-15">
                                        <img src="logo/clientelogo.png" alt="">
                                    </figure>
                                </div>
                                <div class="col px-0 align-self-center">
                                    <p class="mb-1"> <?php printf($usuariologado); ?></p>
                                    <p class="text-muted size-12"><?php printf($setorlogado); ?></p>
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-44 btn-light">
                                        <i class="bi bi-box-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-opac text-white border-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <h1 class="display-4"></h1>
                                    </div>
                                    <div class="col-auto">
                                        <p class="text-muted"></p>
                                    </div>
                                    <div class="col text-end">
                                        <p class="text-muted"><a href="#" ></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- user emnu navigation -->
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="dashboard">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Dashboard</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="device">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Dispositivos</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="alarm">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Alarmes</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                       

                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="config">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Configurações </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>









                        <li class="nav-item">
                            <a class="nav-link" href="../../signin.html" tabindex="-1">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-box-arrow-right"></i></div>
                                <div class="col">Sair</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Sidebar main menu ends -->

    <!-- Begin page -->
    <main class="h-100">

        <!-- Header -->
        <header class="header position-fixed">
            <div class="row">
                <div class="col-auto">
                    <a href="../../javascript:void(0)" target="_self" class="btn btn-light btn-44 menu-btn" >
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="col align-self-center text-center">
                    <div class="logo-small">
                        <img src="../../../framework/img/64x64.png" alt="Logo">
                        <h5>Sistema OSIRIS</h5>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="#" target="_self" class="btn btn-light btn-44">
                        <i class="fa fa-bell"></i>
                        <span class="count-indicator"></span>
                    </a>
                </div>
            </div>
        </header>
        <!-- Header ends -->

        <!-- main page content -->
        <div class="main-container container">
            <!-- welcome user -->
            <div class="row mb-4">
                <div class="col-auto">
                    <div class="avatar avatar-50 shadow rounded-10">
                           <img src="logo/clientelogo.png" alt="">
                    </div>
                </div>
                <div class="col align-self-center ps-0">
                    <h4 class="text-color-theme"><span class="fw-normal">Olá!</span>, <?php printf($usuariologado); ?></h4>
                    <p class="text-muted">Bem Vindo!</p>
                </div>
            </div>
             <!-- offers banner -->
            <div class="row mb-4">
                <div class="col-12">
                    <div class="card theme-bg text-center">
                        <div class="card-body">
                            <div class="row">
                                <div class="col align-self-center">
                                    <h1>Telemetria Hospitalar e Operação de Telemonitoramento</h1>
                                    <p class="size-12 text-muted">
                                       Gerenciamento
                                    </p>
                                    <div class="tag border-dashed border-opac">
                                   
                                    </div>
                                </div>
                                <div class="col-6 align-self-center ps-0">
                                    <!--  <img src="../../../framework/img/logo.png" alt="" class="mw-100"> -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Dark mode switch -->
            <div class="row mb-4">
                <div class="col-12">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" id="darkmodeswitch">
                                <label class="form-check-label text-muted px-2 " for="darkmodeswitch">Modo Escuro</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Saving targets -->
            <div class="row mb-3">
                <div class="col">
                    <h6 class="title">Dispositivos</h6>
                </div>
                <div class="col-auto">

                </div>
            </div>
 
             
                <!-- main page content -->
        <div class="main-container container">

            <!-- Contact us form -->
            <div class="row mb-4">
                <div class="col-12 col-md-6 col-lg-4 mx-auto">
                    <h3 class="mb-2 text-center text-color-theme">Dados Sensor</h3>
                    
                     <?php
                      // Defina seus dados, suponha que você tenha esses valores em um array associativo chamado $dados
                      $dados = array(
                        "Macadress" => "$macadress",
                        "Sensor" => "$id_sensor_type",
                        "Nome" => "$nome",
                        "Unidade" => "$unidade",
                        "Setor" => "$setor",
                        "Área" => "$area",
                        "Status" => "$id_status",
                        "Up Time" => "$up_time"
                       
                      );
                    ?>
                    
                    <?php foreach ($dados as $campo => $valor): ?>
                    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="<?php echo strtolower(str_replace(' ', '_', $campo)); ?>"><?php echo $campo; ?> <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input type="text" id="<?php echo strtolower(str_replace(' ', '_', $campo)); ?>" name="<?php echo strtolower(str_replace(' ', '_', $campo)); ?>" value="<?php echo htmlspecialchars($valor); ?>" readonly="readonly" required="required" class="form-control">
                      </div>
                    </div>
                    <?php endforeach; ?></div>
</div>
</div>
</div>
                 
                              <?php include("frontend/device-alarm-frontend.php"); ?>

                <!-- main page content ends -->


          </main>
    <!-- Page ends-->

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <ul class="nav nav-pills nav-justified">
                
                
                
               
         
            
            </ul>
        </div>
    </footer>
    <!-- Footer ends-->
          <!-- Camera Modal -->
          <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xsm modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h6 class="modal-title" id="cammodalLabel">Camera</h6>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                  <img src="../../assets/img/infographic-camera.png" alt="" class="mb-4">
                  <script src="https://unpkg.com/html5-qrcode"></script>
                  <div id="qr-reader"></div>
                  <div id="qr-reader-results"></div>
                  <script>
                    var resultContainer = document.getElementById('qr-reader-results');
                    var lastResult, countResults = 0;
                    function extrairTexto(url) {
                      var startIndex = url.indexOf("?") + 1; // Encontra o índice do caractere "?" e avança 1 para ignorá-lo
                      var endIndex = url.indexOf("="); // Encontra o índice do caractere "="
                      
                      if (startIndex >= 0 && endIndex >= 0 && endIndex > startIndex) {
                        return url.substring(startIndex, endIndex); // Extrai o texto entre "?" e "="
                      } else {
                        return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                      }
                    }
                    function extrairTextoid(urlid) {
                      var startIndexid = urlid.indexOf("=") + 1; // Encontra o índice do caractere "=" e avança 1 para ignorá-lo
                      var endIndexid = urlid.length; // O índice do fim da string
                      
                      if (startIndexid >= 0 && endIndexid > startIndexid) {
                        return urlid.substring(startIndexid, endIndexid); // Extrai o texto após o "=" até o final da string
                      } else {
                        return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                      }
                    }


                    function onScanSuccess(decodedText, decodedResult) {
                      if (decodedText !== lastResult) {
                        ++countResults;
                        lastResult = decodedText;
                        // Handle on success condition with the decoded message.
                        console.log(`Scan result ${decodedText}`, decodedResult);
                      //  alert(`${decodedText}`, decodedResult);
                        var textoExtraido = extrairTexto(decodedText);
                        if (textoExtraido == "equipamento"){
                       var textoExtraidoid = extrairTextoid(decodedText);
                          window.location.href = `equipament-manual-user?id=`+textoExtraidoid;

                        }
                        if (textoExtraido == "os"){
                          var textoExtraidoid = extrairTextoid(decodedText);
                          window.location.href = `corrective-maintenance-open?id=`+textoExtraidoid;
                          
                        }
                        if (textoExtraido == "id"){
                          var textoExtraidoid = extrairTextoid(decodedText);
                          window.location.href = `preventive-maintenance-open?id=`+textoExtraidoid;
                          
                        }
                        
                      }
                    }
                    
                    var html5QrcodeScanner = new Html5QrcodeScanner(
                      "qr-reader", { fps: 10, qrbox: 250 });
                    html5QrcodeScanner.render(onScanSuccess);
                  </script>

                </div>
                
              </div>
            </div>
          </div>
          
          
          <!-- Camera Modal ends-->
          <?php
            date_default_timezone_set('America/Sao_Paulo');
            $today = date("Y-m-d");
            $read_open = 1;
            
            $query = "SELECT sensor.nome,notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor WHERE read_open = 1 ORDER BY notif.data ";
            
            $resultados = $conn->query($query);
            $notifications = array();
            
            while ($row = mysqli_fetch_assoc($resultados)) {
              $notifications[] = $row;
            }
            
            $conn->close();
          ?>
          
          <script>
            if ('serviceWorker' in navigator) {
              window.addEventListener('load', function() {
                navigator.serviceWorker.register('service-worker.js').then(function(registration) {
                  console.log('ServiceWorker registration successful with scope: ', registration.scope);
                }, function(err) {
                  console.log('ServiceWorker registration failed: ', err);
                });
              });
            }
            
            function showNotification(title, options) {
              if (Notification.permission === "granted") {
                var notification = new Notification(title, options);
              } else if (Notification.permission !== 'denied') {
                Notification.requestPermission().then(function(permission) {
                  if (permission === "granted") {
                    var notification = new Notification(title, options);
                  }
                });
              }
            }
            
            window.onload = function() {
              <?php foreach ($notifications as $notification): ?>
              var title = "<?php echo $notification['origem']; ?>";
              var options = {
                body: "<?php echo $notification['ref']; ?>",
                // Adicione mais opções de notificação conforme necessário
              };
              showNotification(title, options);
              <?php endforeach; ?>
            };
          </script>
    <!-- Required jquery and libraries -->
    <script src="../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/popper.min.js"></script>
    <script src="../../assets/vendor/bootstrap-5/js/bootstrap.bundle.min.js"></script>

    <!-- cookie js -->
    <script src="../../assets/js/jquery.cookie.js"></script>

    <!-- Customized jquery file  -->
    <script src="../../assets/js/main.js"></script>
    <script src="../../assets/js/color-scheme.js"></script>

    <!-- PWA app service registration and works -->
    <script src="../../assets/js/pwa-services.js"></script>

    <!-- Chart js script -->
    <script src="../../assets/vendor/chart-js-3.3.1/chart.min.js"></script>
 
    <!-- Progress circle js script -->
    <script src="../../assets/vendor/progressbar-js/progressbar.min.js"></script>

    <!-- swiper js script -->
    <script src="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.js"></script>

    <!-- page level custom script -->
     <script src="../../assets/js/app.js"></script> 

</body>

</html>
          