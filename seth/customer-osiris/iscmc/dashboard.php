<?php
include("database/database.php");
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
    setcookie('file', null, -1);

	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$email=	$_SESSION['email'] ;
    $file = "dashboard";
    setcookie("file","$file")


?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
      <title>MK Sistema Biomedicos </title>
  <!-- Link para o Font Awesome (se ainda não estiver carregado no projeto) -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <!-- manifest meta -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="manifest" href="../../manifest.json" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="../../assets/img/favicon180.png" sizes="180x180">
    <link rel="icon" href="../../assets/img/favicon32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="../../assets/img/favicon16.png" sizes="16x16" type="image/png">

    <!-- Google fonts-->

    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&amp;display=swap" rel="stylesheet">

    <!-- bootstrap icons -->
    <link rel="stylesheet" href="../../../../cdn.jsdelivr.net/npm/bootstrap-icons%401.5.0/font/bootstrap-icons.css">

    <!-- swiper carousel css -->
    <link rel="stylesheet" href="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.css">

    <!-- style css for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet" id="style">
</head>

<body class="body-scroll" data-page="index">

    <!-- loader section -->
    <div class="container-fluid loader-wrap">
        <div class="row h-100">
            <div class="col-10 col-md-6 col-lg-5 col-xl-3 mx-auto text-center align-self-center">
                <div class="loader-cube-wrap loader-cube-animate mx-auto">
                    <img src="../../../framework/img/64x64.png" alt="Logo">
                </div>
                <p class="mt-4">Carregando<br><strong>Aguarde...</strong></p>
            </div>
        </div>
    </div>
    <!-- loader section ends -->

    <!-- Sidebar main menu -->
    <div class="sidebar-wrap  sidebar-pushcontent">
        <!-- Add overlay or fullmenu instead overlay -->
        <div class="closemenu text-muted">Fechar Menu</div>
        <div class="sidebar dark-bg">
            <!-- user information -->
            <div class="row my-3">
                <div class="col-12 ">
                    <div class="card shadow-sm bg-opac text-white border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <figure class="avatar avatar-44 rounded-15">
                                        <img src="logo/clientelogo.png" alt="">
                                    </figure>
                                </div>
                                <div class="col px-0 align-self-center">
                                    <p class="mb-1"> <?php printf($usuariologado); ?></p>
                                    <p class="text-muted size-12"><?php printf($setorlogado); ?></p>
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-44 btn-light">
                                        <i class="bi bi-box-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-opac text-white border-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <h1 class="display-4"></h1>
                                    </div>
                                    <div class="col-auto">
                                        <p class="text-muted"></p>
                                    </div>
                                    <div class="col text-end">
                                        <p class="text-muted"><a href="#" ></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- user emnu navigation -->
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="dashboard">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Dashboard</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                    
                     
                       

                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="config">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Configurações </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>









                        <li class="nav-item">
                            <a class="nav-link" href="../../signin.html" tabindex="-1">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-box-arrow-right"></i></div>
                                <div class="col">Sair</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Sidebar main menu ends -->

    <!-- Begin page -->
    <main class="h-100">

        <!-- Header -->
        <header class="header position-fixed">
            <div class="row">
                <div class="col-auto">
                    <a href="../../javascript:void(0)" target="_self" class="btn btn-light btn-44 menu-btn" >
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="col align-self-center text-center">
                    <div class="logo-small">
                        <img src="../../../framework/img/64x64.png" alt="Logo">
                        <h5>Sistema OSIRIS</h5>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="#" target="_self" class="btn btn-light btn-44">
                        <i class="fa fa-bell"></i>
                        <span class="count-indicator"></span>
                    </a>
                </div>
            </div>
        </header>
        <!-- Header ends -->

        <!-- main page content -->
        <div class="main-container container">
            <!-- welcome user -->
            <div class="row mb-4">
                <div class="col-auto">
                    <div class="avatar avatar-50 shadow rounded-10">
                           <img src="logo/clientelogo.png" alt="">
                    </div>
                </div>
                <div class="col align-self-center ps-0">
                    <h4 class="text-color-theme"><span class="fw-normal">Olá!</span>, <?php printf($usuariologado); ?></h4>
                    <p class="text-muted">Bem Vindo!</p>
                </div>
            </div>
             <!-- offers banner -->
            <div class="row mb-4">
                <div class="col-12">
                    <div class="card theme-bg text-center">
                        <div class="card-body">
                            <div class="row">
                                <div class="col align-self-center">
                                    <h1>Observação e Supervisão de Inspeção, Rastreamento e Integração de Sensores</h1>
                                    <p class="size-12 text-muted">
                                       Gerenciamento
                                    </p>
                                    <div class="tag border-dashed border-opac">
                                   
                                    </div>
                                </div>
                                <div class="col-6 align-self-center ps-0">
                                    <!--  <img src="../../../framework/img/logo.png" alt="" class="mw-100"> -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Dark mode switch -->
            <div class="row mb-4">
                <div class="col-12">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" id="darkmodeswitch">
                                <label class="form-check-label text-muted px-2 " for="darkmodeswitch">Modo Escuro</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Saving targets -->
            <div class="row mb-3">
                <div class="col">
                    <h6 class="title">Painel</h6>
                </div>
                <div class="col-auto">

                </div>
            </div>
 
             
            
                 
                  <div class="col-12 col-md-12">
                    <div class="row">
                  

              
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12">
          <div class>
            <div class="x_content">
              <div class="row">
                <?php
                  // Consultar os últimos dados de cada dispositivo
                  $sql = "SELECT painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
                          FROM painel 
                          INNER JOIN parametro ON parametro.id = painel.id_parametro
                          WHERE painel.id IN (
                              SELECT MAX(id) 
                              FROM painel 
                              GROUP BY id_parametro
                          )";
                  
                  $result = mysqli_query($conn, $sql);
                  
                  if (mysqli_num_rows($result) > 0) {
                    // Exibir os dados em gráficos de medidores
                    $rows = array(); // Array para armazenar os dados para os gráficos
                    while ($row = mysqli_fetch_assoc($result)) {
                      $deviceId = strtolower(preg_replace('/[^a-zA-Z0-9_]/', '', $row['nome'] . $row['unidade']));
                      $rows[] = array('id' => $deviceId, 'value' => $row['vlr']); // Armazenar dados no array

                      echo "<div class='animated flipInY col-lg-3 col-md-3 col-sm-6'>
                              <div class='tile-stats'>
                                  <h5>" . $row['nome'] . "</h5>
                                  <p>" . $row['reg_date'] . "</p>
                                  <p>" . $row['unidade'] . "</p>
                                  <div id='{$deviceId}' style='width: 400px; height: 120px;'></div>
                              </div>
                            </div>";
                    }
                  }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', { 'packages': ['gauge', 'corechart'] });
      google.charts.setOnLoadCallback(drawCharts);

      function drawCharts() {
        <?php
          // Exibir os gráficos de medidores
          foreach ($rows as $row) {
            echo "drawChart('{$row['id']}', {$row['value']});";
          }
        ?>
      }

      function drawChart(device, value) {
        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['', value] // Remova o rótulo, deixando-o vazio
        ]);
        
        var options = {
          width: 400,
          height: 120,
          redFrom: 8,
          redTo: 10,
          yellowFrom: -10,
          yellowTo: 2,
          greenFrom: 2,
          greenTo: 8,
          minorTicks: 5,
          min: -10,
          max: 10
        };
        
        var chart = new google.visualization.Gauge(document.getElementById(device));
        chart.draw(data, options);
      }
    </script>
 
<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 1 AND reg_date >= '$date_limit') OR (id_parametro = 4 AND reg_date >= '$date_limit')
)";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Array para armazenar os dados de temperatura e umidade
  $temp_data = array();
  $hum_data = array();
  
  // Extrair os dados de temperatura e umidade e armazená-los nos arrays correspondentes
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 1) {
      // Temperatura
      $temp_data[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 4) {
      // Umidade
      $hum_data[] = floatval($row5['vlr']);
    }
  }
  
  // Converter os arrays em JSON
  $json_temp_data = json_encode($temp_data);
  $json_hum_data = json_encode($hum_data);
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline2);
  
  function drawChartline2() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_data; ?>;
    var humData = <?php echo $json_hum_data; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Temperatura');
    data.addColumn('number', 'Umidade');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Nível de Temperatura e Umidade Sala Tomografia',
      curveType: 'function',
      legend: { position: 'bottom' }
    };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart2'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart2" style="width: 900px; height: 500px"></div>

<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 2 AND reg_date >= '$date_limit') OR (id_parametro = 5 AND reg_date >= '$date_limit')
)";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Array para armazenar os dados de temperatura e umidade
  $temp_data = array();
  $hum_data = array();
  
  // Extrair os dados de temperatura e umidade e armazená-los nos arrays correspondentes
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 2) {
      // Temperatura
      $temp_data[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 5) {
      // Umidade
      $hum_data[] = floatval($row5['vlr']);
    }
  }
  
  // Converter os arrays em JSON
  $json_temp_data3 = json_encode($temp_data);
  $json_hum_data3 = json_encode($hum_data);
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline3);
  
  function drawChartline3() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_data3; ?>;
    var humData = <?php echo $json_hum_data3; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Temperatura');
    data.addColumn('number', 'Umidade');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Nível de Temperatura e Umidade Sala Tecnica',
      curveType: 'function',
      legend: { position: 'bottom' }
    };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart3'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart3" style="width: 900px; height: 500px"></div>

<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 3 AND reg_date >= '$date_limit') OR (id_parametro = 6 AND reg_date >= '$date_limit')
)";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Array para armazenar os dados de temperatura e umidade
  $temp_data = array();
  $hum_data = array();
  
  // Extrair os dados de temperatura e umidade e armazená-los nos arrays correspondentes
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 3) {
      // Temperatura
      $temp_data[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 6) {
      // Umidade
      $hum_data[] = floatval($row5['vlr']);
    }
  }
  
  // Converter os arrays em JSON
  $json_temp_data4 = json_encode($temp_data);
  $json_hum_data4 = json_encode($hum_data);
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline4);
  
  function drawChartline4() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_data4; ?>;
    var humData = <?php echo $json_hum_data4; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Temperatura');
    data.addColumn('number', 'Umidade');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Nível de Temperatura e Umidade Sala Ressonância Magnetica',
      curveType: 'function',
      legend: { position: 'bottom' }
    };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart4'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart4" style="width: 900px; height: 500px"></div>
    <?php
              // Defina a data de 48 horas atrás
              $date_limit = date('Y-m-d', strtotime('-48 hours'));
              
              // Consulta SQL para selecionar os dados dos sensores com sensor.id_sensor_type = 3 nas últimas 48 horas
              $sql = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 7 AND reg_date >= '$date_limit')
)";
              
              // Execute a consulta e obtenha os resultados
              $result4 = mysqli_query($conn, $sql);
              
             
              // Converta os resultados em um array associativo para JSON
              $rows4 = array();
              while ($row4 = mysqli_fetch_assoc($result4)) {
                $rows4[] = $row4;
              }
              $json_data5 = json_encode($rows4);
            ?>
            
            <script type="text/javascript">
              google.charts.load('current', {'packages':['corechart']});
              google.charts.setOnLoadCallback(drawChartline5);
              
              function drawChartline5() {
                var jsonData4 = <?php echo $json_data5; ?>;
                
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Data');
                data.addColumn('number', 'Corrente');
                
                // Adiciona as linhas com base nos dados fornecidos
                for (var i = 0; i < jsonData4.length; i++) {
                  var rowData = [
                    jsonData4[i].reg_date, // Assumindo que up_time contém os valores para o eixo X (Data)
                    parseFloat(jsonData4[i].vlr) // Assumindo que dados contém os valores para o eixo Y (Sensor)
                  ];
                  data.addRow(rowData);
                }
                
                
                var options = {
                  title: 'Corrente Criogenia 48 (horas)',
                  curveType: 'function',
                  legend: { position: 'bottom' }
                };
                
                var chart = new google.visualization.LineChart(document.getElementById('curve_chart5'));
                chart.draw(data, options);
              }
            </script>
            
            
            
            <div class="clearfix"></div>
            <br>
            
            <div id="curve_chart5"  style="width: 100%; height: 500px"></div>
            
            <div class="clearfix"></div>
            <br>
            
<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 8 AND reg_date >= '$date_limit') OR (id_parametro = 9 AND reg_date >= '$date_limit')
)";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Array para armazenar os dados de temperatura e umidade
  $temp_data = array();
  $hum_data = array();
  
  // Extrair os dados de temperatura e umidade e armazená-los nos arrays correspondentes
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 8) {
      // Temperatura
      $temp_data[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 9) {
      // Umidade
      $hum_data[] = floatval($row5['vlr']);
    }
  }
  
  // Converter os arrays em JSON
  $json_temp_data6 = json_encode($temp_data);
  $json_hum_data6 = json_encode($hum_data);
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline6);
  
  function drawChartline6() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_data6; ?>;
    var humData = <?php echo $json_hum_data6; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Temperatura IN');
    data.addColumn('number', 'Temperatura OUT');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Nível de Temperatura Entrada e Saida Criogenia',
      curveType: 'function',
      legend: { position: 'bottom' }
    };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart6'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart6" style="width: 900px; height: 500px"></div>


<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 10 AND reg_date >= '$date_limit') OR (id_parametro = 11 AND reg_date >= '$date_limit')
)";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Array para armazenar os dados de temperatura e umidade
  $temp_data = array();
  $hum_data = array();
  
  // Extrair os dados de temperatura e umidade e armazená-los nos arrays correspondentes
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 10) {
      // Temperatura
      $temp_data[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 11) {
      // Umidade
      $hum_data[] = floatval($row5['vlr']);
    }
  }
  
  // Converter os arrays em JSON
  $json_temp_data7 = json_encode($temp_data);
  $json_hum_data7 = json_encode($hum_data);
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline7);
  
  function drawChartline7() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_data7; ?>;
    var humData = <?php echo $json_hum_data7; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Fluxo IN');
    data.addColumn('number', 'Fluxo OUT');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Fluxo de agua de entrada e saida da criogenia',
      curveType: 'function',
      legend: { position: 'bottom' }
    };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart7'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart7" style="width: 900px; height: 500px"></div>
<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
    FROM painel 
    INNER JOIN parametro ON parametro.id = painel.id_parametro
    WHERE painel.id IN (
      SELECT id
      FROM painel 
      WHERE (id_parametro = 10 AND reg_date >= '$date_limit') OR 
            (id_parametro = 11 AND reg_date >= '$date_limit') OR 
            (id_parametro = 8 AND reg_date >= '$date_limit') OR 
            (id_parametro = 9 AND reg_date >= '$date_limit') 
    )";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Arrays para armazenar as diferenças de temperatura e umidade
  $diff_temp_8_9 = array();
  $diff_hum_10_11 = array();
  
  // Extrair os dados e calcular as diferenças
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 8) {
      // Temperatura
      $diff_temp_8_9[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 11) {
      // Umidade
      $diff_hum_10_11[] = floatval($row5['vlr']);
    }
  }
  
  // Calcular as diferenças entre os pares de valores
  $temp_diff = array();
  $hum_diff = array();
  for ($i = 0; $i < count($diff_temp_8_9); $i++) {
    $temp_diff[] = $diff_temp_8_9[$i] - $diff_temp_8_9[$i];
    $hum_diff[] = $diff_hum_10_11[$i] - $diff_hum_10_11[$i];
  }
  
  // Converter os arrays em JSON
  $json_temp_diff = json_encode($temp_diff);
  $json_hum_diff = json_encode($hum_diff);
?>



<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline8);
  
  function drawChartline8() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_diff; ?>;
    var humData = <?php echo $json_hum_diff; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Delta Temperatura');
    data.addColumn('number', 'Delta Fluxo');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Delta Criogenia',
      curveType: 'function',
      legend: { position: 'bottom' }
    };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart8'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart8" style="width: 900px; height: 500px"></div>

<?php
  date_default_timezone_set('America/Sao_Paulo');
  $today = date("Y-m-d");
  $read_open = 1;
  
  $query = "SELECT sensor.nome, notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor WHERE read_open = 1";
  
  $resultados = $conn->query($query);
  $notifications = array();
  
  while ($row = mysqli_fetch_assoc($resultados)) {
    $notifications[] = $row;
  }
?>

<script>
  if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('service-worker.js').then(function(registration) {
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
        // Após o registro do Service Worker, mostrar notificações
        <?php foreach ($notifications as $notification): ?>
        var title = "<?php echo $notification['nome']; ?>";
        var options = {
          body: "<?php echo $notification['dados']; ?>"
        };
        showNotification(title, options);
        <?php endforeach; ?>
      }, function(err) {
        console.log('ServiceWorker registration failed: ', err);
      });
    });
  }
  
  function showNotification(title, options) {
    if (Notification.permission === "granted") {
      var notification = new Notification(title, options);
    } else if (Notification.permission !== 'denied') {
      Notification.requestPermission().then(function(permission) {
        if (permission === "granted") {
          var notification = new Notification(title, options);
        }
      });
    }
  }
</script>

 

                
 
             
                  <div class="col-12 col-md-12">
                    <div class="row">           
               
        <?php
        // Consultar os últimos dados de cada dispositivo
        $sql = "SELECT sd.device, sd.dados, dados_ids.nome, dados_ids.up_time, dados_ids.id_status,dados_ids.id_sensor_type
                FROM sensor_dados sd
                INNER JOIN (
                    SELECT sensor_dados.device, MAX(sensor_dados.id) AS ultimo_id
                    FROM sensor_dados
                    GROUP BY device
                ) ultimos_ids ON sd.device = ultimos_ids.device AND sd.id = ultimos_ids.ultimo_id
                INNER JOIN (
                    SELECT nome AS nome, up_time, macadress, id_status, id_sensor_type
                    FROM sensor 
                ) dados_ids ON sd.device = dados_ids.macadress";

        $result = mysqli_query($conn, $sql);

        if (mysqli_num_rows($result) > 0) {
            // Exibir os dados em cartões lado a lado
            while ($row = mysqli_fetch_assoc($result)) {
                echo "                         <div class='col-4 col-md-4'>

                        <div class='card shadow-sm mb-4 ";
                if ($row['id_status'] == 2) {
                    echo "alert-danger";
                } else {
                    echo "alert-success";
                }
                echo "'>
                                <div class='card-body'>
                                    <div class='row'>
                                        <div class='col'>
                                            <h6 class='mb-0'>Dados: " . $row['dados'] . "</h6>
                                            <p class='text-muted small'>" . $row['nome'] . "</p>
                                            <p class='text-muted small'>" . $row['up_time'] . "</p>
                                            <p class='text-muted small'>" . $row['device'] . "</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             </div>

                          
";
            }
        }
        ?>
  </div>
                         </div>
                </div>
             
                <!-- main page content ends -->


          </main>
    <!-- Page ends-->

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <ul class="nav nav-pills nav-justified">
                
                
                
               
         
            
            </ul>
        </div>
    </footer>
    <!-- Footer ends-->
          <!-- Camera Modal -->
          <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xsm modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h6 class="modal-title" id="cammodalLabel">Camera</h6>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                  <img src="../../assets/img/infographic-camera.png" alt="" class="mb-4">
                  <script src="https://unpkg.com/html5-qrcode"></script>
                  <div id="qr-reader"></div>
                  <div id="qr-reader-results"></div>
                  <script>
                    var resultContainer = document.getElementById('qr-reader-results');
                    var lastResult, countResults = 0;
                    function extrairTexto(url) {
                      var startIndex = url.indexOf("?") + 1; // Encontra o índice do caractere "?" e avança 1 para ignorá-lo
                      var endIndex = url.indexOf("="); // Encontra o índice do caractere "="
                      
                      if (startIndex >= 0 && endIndex >= 0 && endIndex > startIndex) {
                        return url.substring(startIndex, endIndex); // Extrai o texto entre "?" e "="
                      } else {
                        return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                      }
                    }
                    function extrairTextoid(urlid) {
                      var startIndexid = urlid.indexOf("=") + 1; // Encontra o índice do caractere "=" e avança 1 para ignorá-lo
                      var endIndexid = urlid.length; // O índice do fim da string
                      
                      if (startIndexid >= 0 && endIndexid > startIndexid) {
                        return urlid.substring(startIndexid, endIndexid); // Extrai o texto após o "=" até o final da string
                      } else {
                        return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                      }
                    }


                    function onScanSuccess(decodedText, decodedResult) {
                      if (decodedText !== lastResult) {
                        ++countResults;
                        lastResult = decodedText;
                        // Handle on success condition with the decoded message.
                        console.log(`Scan result ${decodedText}`, decodedResult);
                      //  alert(`${decodedText}`, decodedResult);
                        var textoExtraido = extrairTexto(decodedText);
                        if (textoExtraido == "equipamento"){
                       var textoExtraidoid = extrairTextoid(decodedText);
                          window.location.href = `equipament-manual-user?id=`+textoExtraidoid;

                        }
                        if (textoExtraido == "os"){
                          var textoExtraidoid = extrairTextoid(decodedText);
                          window.location.href = `corrective-maintenance-open?id=`+textoExtraidoid;
                          
                        }
                        if (textoExtraido == "id"){
                          var textoExtraidoid = extrairTextoid(decodedText);
                          window.location.href = `preventive-maintenance-open?id=`+textoExtraidoid;
                          
                        }
                        
                      }
                    }
                    
                    var html5QrcodeScanner = new Html5QrcodeScanner(
                      "qr-reader", { fps: 10, qrbox: 250 });
                    html5QrcodeScanner.render(onScanSuccess);
                  </script>

                </div>
                
              </div>
            </div>
          </div>
          
          
          <!-- Camera Modal ends-->
          <?php
            date_default_timezone_set('America/Sao_Paulo');
            $today = date("Y-m-d");
            $read_open = 1;
            
            $query = "SELECT sensor.nome,notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor WHERE read_open = 1 ORDER BY notif.data ";
            
            $resultados = $conn->query($query);
            $notifications = array();
            
            while ($row = mysqli_fetch_assoc($resultados)) {
              $notifications[] = $row;
            }
            
            $conn->close();
          ?>
          
          <script>
            if ('serviceWorker' in navigator) {
              window.addEventListener('load', function() {
                navigator.serviceWorker.register('service-worker.js').then(function(registration) {
                  console.log('ServiceWorker registration successful with scope: ', registration.scope);
                }, function(err) {
                  console.log('ServiceWorker registration failed: ', err);
                });
              });
            }
            
            function showNotification(title, options) {
              if (Notification.permission === "granted") {
                var notification = new Notification(title, options);
              } else if (Notification.permission !== 'denied') {
                Notification.requestPermission().then(function(permission) {
                  if (permission === "granted") {
                    var notification = new Notification(title, options);
                  }
                });
              }
            }
            
            window.onload = function() {
              <?php foreach ($notifications as $notification): ?>
              var title = "<?php echo $notification['origem']; ?>";
              var options = {
                body: "<?php echo $notification['ref']; ?>",
                // Adicione mais opções de notificação conforme necessário
              };
              showNotification(title, options);
              <?php endforeach; ?>
            };
          </script>
    <!-- Required jquery and libraries -->
    <script src="../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/popper.min.js"></script>
    <script src="../../assets/vendor/bootstrap-5/js/bootstrap.bundle.min.js"></script>

    <!-- cookie js -->
    <script src="../../assets/js/jquery.cookie.js"></script>

    <!-- Customized jquery file  -->
    <script src="../../assets/js/main.js"></script>
    <script src="../../assets/js/color-scheme.js"></script>

    <!-- PWA app service registration and works -->
    <script src="../../assets/js/pwa-services.js"></script>

    <!-- Chart js script -->
    <script src="../../assets/vendor/chart-js-3.3.1/chart.min.js"></script>
 
    <!-- Progress circle js script -->
    <script src="../../assets/vendor/progressbar-js/progressbar.min.js"></script>

    <!-- swiper js script -->
    <script src="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.js"></script>

    <!-- page level custom script -->
     <script src="../../assets/js/app.js"></script> 

</body>

</html>
          