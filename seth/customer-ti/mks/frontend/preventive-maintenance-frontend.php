<?php
include("database/database.php");
$query = "SELECT equipamento.serie,equipamento_familia.fabricante,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine LEFT JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  LEFT JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id LEFT JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id LEFT JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao LEFT JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  LEFT JOIN instituicao ON instituicao.id = instituicao_area.id_unidade where maintenance_preventive.id_status like '1'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($serie,$fabricante,$instituicao,$area,$setor,$programada,$id,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);

  ?>
  <style>
  * {
    box-sizing: border-box;
  }

  #myInput {

    background-position: 10px 10px;
    background-repeat: no-repeat;
    width: 100%;
    font-size: 16px;
    padding: 12px 20px 12px 40px;
    border: 1px solid #ddd;
    margin-bottom: 12px;
  }

  #myTable {
    border-collapse: collapse;
    width: 100%;
    border: 1px solid #ddd;
    font-size: 18px;
  }

  #myTable th, #myTable td {
    text-align: left;
    padding: 12px;
  }

  #myTable tr {
    border-bottom: 1px solid #ddd;
  }

  #myTable tr.header, #myTable tr:hover {
    background-color: #f1f1f1;
  }
    .custom-black-button {
      background-color: black;
      color: white; /* Altere a cor do texto conforme necessário */
    }
</style>



<h2>Preventiva</h2>

<div class="row">
  <div class="col-12">
    <input type="text" id="filtro" placeholder="Digite">
    <div class="card shadow-sm mb-4">
      <ul id="lista-usuarios" class="list-group list-group-flush bg-none">
        <?php   while ($stmt->fetch()) {   ?>
        
        <style>
          .custom-bg-color {
            background-color: green; /* Use a variável PHP $color aqui */
          }
        </style>
        <div id="box_<?php echo $id; ?>">
          <li class="list-group-item">
            <div class="row">
              <div class="col-auto">
                
                <figure class="avatar avatar-50 rounded-10 shadow-sm">
                  <img src="" alt="<?printf($rotina); ?>">
                </figure>
              </div>
              <div class="col px-0">
                <p><?php printf($codigo); ?> - <?php printf($nome); ?> - <?php printf($modelo); ?> - <?php printf($fabricante); ?><br><small class="text-muted"><?php printf($programada); ?> </small></p>
              </div>
              <div class="col-auto text-end">
                <p><small class="text-muted size-12"><?php printf($periodicidade); ?> <span class="avatar avatar-6 rounded-circle bg-success d-inline-block"></span></small></p>
              </div>
            </div>
          </li>
          <script>
            // Selecionando o elemento para aplicar o swipe
            var box = document.getElementById('box_<?php echo $id; ?>');
            
            // Criando um objeto Hammer para o elemento
            var hammer = new Hammer(box);
            // Adicionando um ouvinte de evento para detectar o gesto de tap (toque)
            hammer.on('tap', function(event) {
              //  alert('Tap (Toque) detectado!');
              window.location.href = 'preventive-maintenance-open-procedure?id=<?php echo $id; ?>';
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de double tap (toque duplo)
            hammer.on('doubletap', function(event) {
              alert('Double Tap (Toque Duplo) detectado!');
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de pinch (beliscar)
            hammer.on('pinch', function(event) {
              alert('Pinch (Beliscar) detectado!');
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de rotate (rotacionar)
            hammer.on('rotate', function(event) {
              alert('Rotate (Rotacionar) detectado!');
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de long press (pressão longa)
            hammer.on('press', function(event) {
              alert('Rotina: <?php echo $rotina; ?>\nUnidade: <?php printf($instituicao); ?>\nSetor: <?php printf($area); ?>\nArea: <?php printf($setor); ?>\nProgramada: <?php printf($programada); ?>\nPeriodicidade: <?php printf($periodicidade); ?> ');
              
              
            });
            // Adicionando um ouvinte de evento para detectar o gesto swipe
            hammer.on('swipe', function(event) {
              // Verificando a direção do swipe e exibindo uma mensagem correspondente
              switch(event.direction) {
                case Hammer.DIRECTION_UP:
                  alert('Swipe para cima detectado!');
                  break;
                case Hammer.DIRECTION_DOWN:
                  alert('Swipe para baixo detectado!');
                  break;
                case Hammer.DIRECTION_LEFT:
                  //alert('Swipe para esquerda detectado!');
                  window.location.href = 'preventive-maintenance-open-close?id=<?php echo $id; ?>';
                  break;
                case Hammer.DIRECTION_RIGHT:
                  //alert('Swipe para direita detectado!');
                  // Redirecionando a página
                  window.location.href = 'preventive-maintenance-open?id=<?php echo $id; ?>';
                  
                  
                  break;
              }
            });
          </script> 
        </div>
        <?php   } }  ?>
        
        <!-- Adicione mais itens de lista conforme necessário -->
      </ul>
    </div>
  </div>
</div>

<script>
  document.getElementById("filtro").addEventListener("input", function () {
    var filtro = this.value.toLowerCase();
    var listaUsuarios = document.getElementById("lista-usuarios").getElementsByTagName("li");
    
    for (var i = 0; i < listaUsuarios.length; i++) {
      var nome = listaUsuarios[i].getElementsByTagName("p")[0].innerText.toLowerCase();
      var cargo = listaUsuarios[i].getElementsByTagName("small")[0].innerText.toLowerCase();
      
      if (nome.includes(filtro) || cargo.includes(filtro)) {
        listaUsuarios[i].style.display = "";
      } else {
        listaUsuarios[i].style.display = "none";
      }
    }
  });
</script>
