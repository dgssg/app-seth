<?php
    include("../database/database.php");
    
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    session_start();
    if(!isset($_SESSION['usuario'])){
        header ("Location: ../index.php");
    }
    $usuariologado=$_SESSION['id_usuario'];
    $instituicaologado=$_SESSION['instituicao'];
    $setorlogado=$_SESSION['setor'];
    
    
     $codigoget = $_GET['os'];
    
    $causa = $_POST['causa'];
    $defeito = $_POST['defeito'];
    $date_start= $_POST['date_start'];
    $date_end = $_POST['date_end'];
    $time = $_POST['time'];
    $services = $_POST['service'];
    $v4uuid = $_POST['v4uuid'];
    $assinature = $_POST['assinature'];
    $chave= $_POST['chave'];
    

    // Receber os dados da assinatura do cliente e do vendedor
    $assinaturaClienteBase64 = $_POST['assinaturaCliente'];
    $assinaturaVendedorBase64 = $_POST['assinaturaVendedor'];

    // Decodificar as assinaturas da base64
    $assinaturaClienteDecodificada = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $assinaturaClienteBase64));
    $assinaturaVendedorDecodificada = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $assinaturaVendedorBase64));

    // Nome dos arquivos para as assinaturas
    $timestamp = date('YmdHis');
    $caminhoArquivoCliente = '../../../../../seth.mksistemasbiomedicos.com.br/customer-ec/' . $key . '/dropzone/mc-signature/assinatura_tecnico_' . $timestamp . '.png';
    $caminhoArquivoVendedor = '../../../../../seth.mksistemasbiomedicos.com.br/customer-ec/' . $key . '/dropzone/mc-signature/assinatura_solicitante_' . $timestamp . '.png';

    // Salvar as assinaturas como arquivos
    file_put_contents($caminhoArquivoCliente, $assinaturaClienteDecodificada);
    file_put_contents($caminhoArquivoVendedor, $assinaturaVendedorDecodificada);
    $assinatura_exe = "assinatura_tecnico_$timestamp.png"; 
    $assinatura_sol = "assinatura_solicitante_$timestamp.png"; 

    $status=6;
    $os_close=2;
    
      
            $query="SELECT os_rating FROM tools WHERE 1";
            if ($stmt = $conn->prepare($query)) {
                $stmt->execute();
                $stmt->bind_result($os_rating);
                while ($stmt->fetch()) {
                }
            }
            
    $stmt = $conn->prepare("UPDATE os SET signature_app_exe= ? WHERE id= ?");
    $stmt->bind_param("ss",$assinatura_exe,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();
    
    $stmt = $conn->prepare("UPDATE os SET signature_app_sol= ? WHERE id= ?");
    $stmt->bind_param("ss",$assinatura_sol,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();
                
                $stmt = $conn->prepare("UPDATE os SET os_close= ? WHERE id= ?");
                $stmt->bind_param("ss",$os_close,$codigoget);
                $execval = $stmt->execute();
                $stmt->close();
                
                
                $stmt = $conn->prepare("UPDATE os SET services= ? WHERE id= ?");
                $stmt->bind_param("ss",$services,$codigoget);
                $execval = $stmt->execute();
                $stmt->close();
                
                $stmt = $conn->prepare("UPDATE os SET time= ? WHERE id= ?");
                $stmt->bind_param("ss",$time,$codigoget);
                $execval = $stmt->execute();
                $stmt->close();
                
                $stmt = $conn->prepare("UPDATE os SET date_end= ? WHERE id= ?");
                $stmt->bind_param("ss",$date_end,$codigoget);
                $execval = $stmt->execute();
                $stmt->close();
                
                
                $stmt = $conn->prepare("UPDATE os SET date_start= ? WHERE id= ?");
                $stmt->bind_param("ss",$date_start,$codigoget);
                $execval = $stmt->execute();
                $stmt->close();
                
                $stmt = $conn->prepare("UPDATE os SET defeito= ? WHERE id= ?");
                $stmt->bind_param("ss",$defeito,$codigoget);
                $execval = $stmt->execute();
                $stmt->close();
                
                $stmt = $conn->prepare("UPDATE os SET id_defeito= ? WHERE id= ?");
                $stmt->bind_param("ss",$causa,$codigoget);
                $execval = $stmt->execute();
                $stmt->close();
                
                $stmt = $conn->prepare("UPDATE os SET id_status= ? WHERE id= ?");
                $stmt->bind_param("ss",$status,$codigoget);
                $execval = $stmt->execute();
                $stmt->close();
                
                
                if($os_rating == "0"){
                $stmt = $conn->prepare("INSERT INTO regdate_os_rating (id_os, status) VALUES (?, ?)");
                $stmt->bind_param("ss",$codigoget,$os_rating);
                $execval = $stmt->execute();
                $stmt->close();
                }
                
                $query = "SELECT workflow FROM tools";
                
                
                if ($stmt = $conn->prepare($query)) {
                    $stmt->execute();
                    $stmt->bind_result($workflow);
                    while ($stmt->fetch()) {
                        //printf("%s, %s\n", $solicitante, $equipamento);
                    }
                }
                if($workflow == "0"){
                    
                    $query = "SELECT id,id_os_posicionamento FROM os_workflow WHERE id_os_status = 6";

                    if ($stmt = $conn->prepare($query)) {
                        $stmt->execute();
                        $stmt->bind_result($workflow,$id_os_posicionamento);
                        while ($stmt->fetch()) {
                            //printf("%s, %s\n", $solicitante, $equipamento);
                        }
                    }
                    
                    $stmt = $conn->prepare("UPDATE os SET id_posicionamento= ? WHERE id= ?");
                    $stmt->bind_param("ss",$posicionamento,$codigoget);
                    $execval = $stmt->execute();
                    $stmt->close();
                    
                    $stmt = $conn->prepare("UPDATE os SET id_workflow= ? WHERE id= ?");
                    $stmt->bind_param("ss",$workflow,$codigoget);
                    $execval = $stmt->execute();
                    $stmt->close();
                    
                    $stmt = $conn->prepare("INSERT INTO regdate_os_posicionamento (id_os, id_posicionamento, id_status) VALUES (?, ?, ?)");
                    $stmt->bind_param("sss",$codigoget,$id_os_posicionamento,$status);
                    $execval = $stmt->execute();
                    $stmt->close();
                
                    $stmt = $conn->prepare("INSERT INTO regdate_os_workflow (id_os, id_workflow) VALUES (?, ?)");
                    $stmt->bind_param("ss",$codigoget,$workflow);
                    $execval = $stmt->execute();
                    $stmt->close();
                    
                }
                $query = "SELECT email FROM tools";
                
                
                if ($stmt = $conn->prepare($query)) {
                    $stmt->execute();
                    $stmt->bind_result($email);
                    while ($stmt->fetch()) {
                        //printf("%s, %s\n", $solicitante, $equipamento);
                    }
                }
                if($email == "0"){
                    
                    
                    
                    //header('Location: ../workflow/os-progress-upgrade-close-workflow.php?os='.$codigoget);
                }
    
    $query = "SELECT os_mp_4 FROM tools";
    
    
    if ($stmt = $conn->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($posicionamento);
        while ($stmt->fetch()) {
            //printf("%s, %s\n", $solicitante, $equipamento);
        }
    }
    
    if($posicionamento != ""){
        
        $stmt = $conn->prepare("UPDATE os SET id_posicionamento= ? WHERE id= ?");
        $stmt->bind_param("ss",$posicionamento,$codigoget);
        $execval = $stmt->execute();
        $stmt->close();
        
    
        
        $stmt = $conn->prepare("INSERT INTO regdate_os_posicionamento (id_os, id_posicionamento, id_status) VALUES (?, ?, ?)");
        $stmt->bind_param("sss",$codigoget,$id_os_posicionamento,$status);
        $execval = $stmt->execute();
        $stmt->close();

        
    }
                
                echo "<script>alert('Ordem de Serviço Fechada!');document.location='../corrective-maintenance'</script>";
                
    
            }
            

?>
