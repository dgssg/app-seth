<?php
include("database/database.php");
//include("../../codigo.php");
//$codigoget = (int)($_GET["codigoget"]);
$rotina = (int)($_GET["id"]);

	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
    setcookie('file', null, -1);

	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$email=	$_SESSION['email'] ;
    $file = "preventive-maintenance";
    setcookie("file","$file");

    $query = "SELECT maintenance_preventive.id,maintenance_routine.id_category,maintenance_preventive.id_mp,maintenance_preventive.id_fornecedor,maintenance_preventive.date_end,maintenance_preventive.file,maintenance_preventive.time_mp,maintenance_preventive.obs_tc,maintenance_preventive.obs_mp,maintenance_preventive.date_start,maintenance_preventive.upgrade,maintenance_preventive.id_status,maintenance_preventive.date_mp_end,maintenance_preventive.date_mp_start,maintenance_routine.time_ms,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine LEFT JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  LEFT JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id LEFT JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id where maintenance_preventive.id like '$rotina'";
    if ($stmt = $conn->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($maintenance_preventive,$id_category,$id_tecnico,$id_fornecedor,$date_end,$id_anexo,$time_mp,$obs_tc,$obs_mp,$programada,$ms_upgrade,$status,$date_end_ms,$date_start_ms,$time_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
       while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
      //  }
    }
    }

?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
      <title>MK Sistema Biomedicos </title>
  <!-- Link para o Font Awesome (se ainda não estiver carregado no projeto) -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <!-- manifest meta -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="manifest" href="../../manifest.json" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="../../assets/img/favicon180.png" sizes="180x180">
    <link rel="icon" href="../../assets/img/favicon32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="../../assets/img/favicon16.png" sizes="16x16" type="image/png">

    <!-- Google fonts-->

    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&amp;display=swap" rel="stylesheet">

    <!-- bootstrap icons -->
    <link rel="stylesheet" href="../../../../../../cdn.jsdelivr.net/npm/bootstrap-icons%401.5.0/font/bootstrap-icons.css">

    <!-- swiper carousel css -->
    <link rel="stylesheet" href="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.css">

    <!-- style css for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet" id="style">
</head>

<body class="body-scroll" data-page="index">

    <!-- loader section -->
    <div class="container-fluid loader-wrap">
        <div class="row h-100">
            <div class="col-10 col-md-6 col-lg-5 col-xl-3 mx-auto text-center align-self-center">
                <div class="loader-cube-wrap loader-cube-animate mx-auto">
                    <img src="../../../framework/img/64x64.png" alt="Logo">
                </div>
                <p class="mt-4">Carregando<br><strong>Aguarde...</strong></p>
            </div>
        </div>
    </div>
    <!-- loader section ends -->

    <!-- Sidebar main menu -->
    <div class="sidebar-wrap  sidebar-pushcontent">
        <!-- Add overlay or fullmenu instead overlay -->
        <div class="closemenu text-muted">Fechar Menu</div>
        <div class="sidebar dark-bg">
            <!-- user information -->
            <div class="row my-3">
                <div class="col-12 ">
                    <div class="card shadow-sm bg-opac text-white border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <figure class="avatar avatar-44 rounded-15">
                                        <img src="logo/clientelogo.png" alt="">
                                    </figure>
                                </div>
                                <div class="col px-0 align-self-center">
                                    <p class="mb-1"> <?php printf($usuariologado); ?></p>
                                    <p class="text-muted size-12"><?php printf($setorlogado); ?></p>
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-44 btn-light">
                                        <i class="bi bi-box-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-opac text-white border-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <h1 class="display-4"></h1>
                                    </div>
                                    <div class="col-auto">
                                        <p class="text-muted"></p>
                                    </div>
                                    <div class="col text-end">
                                        <p class="text-muted"><a href="#" ></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- user emnu navigation -->
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="dashboard">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Dashboard</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="corrective-maintenance">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manutenção Corretiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="corrective-maintenance-new">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Nova Manutenção Corretiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="preventive-maintenance">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manutenção Preventiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-technician">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Tecnico </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-user">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Usuario </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="service">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Serviço </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>

                      <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="config">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Configurações </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>









                        <li class="nav-item">
                            <a class="nav-link" href="../../signin.html" tabindex="-1">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-box-arrow-right"></i></div>
                                <div class="col">Sair</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Sidebar main menu ends -->

    <!-- Begin page -->
    <main class="h-100">

        <!-- Header -->
        <header class="header position-fixed">
            <div class="row">
                <div class="col-auto">
                    <a href="../../javascript:void(0)" target="_self" class="btn btn-light btn-44 menu-btn" >
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="col align-self-center text-center">
                    <div class="logo-small">
                        <img src="../../../framework/img/64x64.png" alt="Logo">
                        <h5>Sistema SETH</h5>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="#" target="_self" class="btn btn-light btn-44">
                        <i class="fa fa-bell"></i>
                        <span class="count-indicator"></span>
                    </a>
                </div>
            </div>
        </header>
        <!-- Header ends -->
        <div class="row mb-4">
                <div class="col-12 text-center">
                    <img src="../../assets/img/infographic-scan.png" alt="">
                </div>
            </div>
  <!-- wallet balance -->
  <div class="card shadow-sm mb-4">
                <div class="card-body">
                    <div class="row">
                         
                        
                    </div>
                </div>
                <div class="card theme-bg text-white border-0 text-center">
                    <div class="card-body">
                        <h1 class="display-1 my-2"><?php printf($rotina); ?></h1>
                        <p class="text-muted mb-2">Rotina</p>
                    </div>
                </div>
            </div>

            <!-- summary -->
            <div class="row mb-3">
                <div class="col-6 col-md-4">
                    <div class="card shadow-sm mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto px-0">
                                    <div class="avatar avatar-40 bg-warning text-white shadow-sm rounded-10-end">
                                        <i class="bi bi-star"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-muted size-12 mb-0">Programada </p>
                                    <p><?php printf($programada); ?> <?php printf($id_usuario_sobrenome); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="card shadow-sm mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto px-0">
                                    <div class="avatar avatar-40 bg-success text-white shadow-sm rounded-10-end">
                                        <i class="bi bi-cash-stack"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-muted size-12 mb-0">Tempo</p>
                                    <p><?php printf($time_ms); ?> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="card shadow-sm mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto px-0">
                                    <div class="avatar avatar-40 bg-warning text-white shadow-sm rounded-10-end">
                                        <i class="bi bi-star"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-muted size-12 mb-0">Equipamento </p>
                                    <p><?php printf($codigo); ?> - <?php printf($nome); ?> - <?php printf($modelo); ?> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      <div class="col-12 col-md-8 col-lg-4 mx-auto">
        <div class="card shadow-sm mb-4">
          <div class="card-header">
            <h6 class="my-1">Anexos</h6>
          </div>
          <div class="card-body bg-light">
            <ul class="list-group list-group-flush w-100 bubble-sheet log-information">
              
              <?php
                $query="SELECT id, file, titulo, reg_date, upgrade FROM regdate_mp_dropzone WHERE id_mp like '$maintenance_preventive' ORDER BY id  DESC";
                $row=1;
                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);                          
              ?>
              <?php  while ($stmt->fetch()) { ?>
              <li class="list-group-item">
                <div class="avatar avatar-15 border-danger rounded-circle"></div>
                <p><span class="text-color-theme">Titulo: <?php printf($titulo); ?></span>
                  <br><small class="text-muted">Data: <?php printf($reg_date); ?></small><br><div class="col-auto">
                    <div class="avatar avatar-44 coverimg rounded-10">
                      <img src="http://seth.mksistemasbiomedicos.com.br/customer-ec/<?php echo $key; ?>/dropzone/mp-dropzone/<?php echo $id_anexo; ?>" />
                    </div>
                  </div> </p>
                
                <?php  $row=$row+1; }
                }   ?>
              </li>
              
            </ul>
          </div>
          <div class="card-footer">
            <div class="row">
              
              
            </div>
          </div>
        </div>
      </div>

      <div class="col-12 col-md-8 col-lg-4 mx-auto">
        <div class="card shadow-sm mb-4">
          <div class="card-header">
            <h6 class="my-1">Registro</h6>
          </div>
          <div class="card-body bg-light">
            <ul class="list-group list-group-flush w-100 bubble-sheet log-information">
              
              <?php
                $query="SELECT regdate_mp_registro.id_mp, regdate_mp_registro.id_user,regdate_mp_registro.date_start,regdate_mp_registro.date_end,regdate_mp_registro.time,regdate_mp_registro.reg_date, regdate_mp_registro.upgrade, colaborador.id, colaborador.primeironome FROM regdate_mp_registro LEFT JOIN colaborador on regdate_mp_registro.id_user = colaborador.id WHERE regdate_mp_registro.id_mp like '$maintenance_preventive'";
                $row=1;
                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id_os,$id_user,$date_start,$date_end,$time,$reg_date,$upgrade,$id,$usuario);
                  
              ?>
              <?php  while ($stmt->fetch()) { ?>
              <li class="list-group-item">
                <div class="avatar avatar-15 border-danger rounded-circle"></div>
                <p><span class="text-color-theme">Executante: <?php printf($usuario); ?></span>
                  <br><small class="text-muted">Inicio: <?php printf($date_start); ?></small>  <br><small class="text-muted">Termino: <?php printf($date_end); ?></small><br></p>
                
                <?php  $row=$row+1; }
                }   ?>
              </li>
              
            </ul>
          </div>
          <div class="card-footer">
            <div class="row">
              
              
            </div>
          </div>
        </div>
      </div>

    <!-- service provider -->
    <div class="row mb-3">
                <div class="col">
                    <h6 class="title">Ferramentas</h6>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-12 px-0">
                    <!-- swiper users connections -->
                    <div class="swiper-container connectionwiper">
                        <div class="swiper-wrapper">
                            

                            <div class="swiper-slide">
                            <a href="preventive-maintenance-open-reg?id= <?php printf($maintenance_preventive); ?>" class="card text-center">
                                    <div class="card-body">
                                        <div class="avatar avatar-50 shadow-sm mb-2 rounded-10 theme-bg text-white">
                                         
                                        </div>
                                        <p class="text-color-theme size-12 small">Registro</p>
                                    </div>
                                </a>
                            </div>
                          <div class="swiper-slide">
                            <a href="preventive-maintenance-open-upload?id= <?php printf($maintenance_preventive); ?>" class="card text-center">
                              <div class="card-body">
                                <div class="avatar avatar-50 shadow-sm mb-2 rounded-10 theme-bg text-white">
                                  
                                </div>
                                <p class="text-color-theme size-12 small">Anexo</p>
                              </div>
                            </a>
                          </div>
                          <div class="swiper-slide">
                            <a href="preventive-maintenance-open-procedure?id= <?php printf($maintenance_preventive); ?>" class="card text-center">
                              <div class="card-body">
                                <div class="avatar avatar-50 shadow-sm mb-2 rounded-10 theme-bg text-white">
                                  
                                </div>
                                <p class="text-color-theme size-12 small">Procedimento</p>
                              </div>
                            </a>
                          </div>
                          
                            <div class="swiper-slide">
                            <a href="preventive-maintenance-open-close?id= <?php printf($maintenance_preventive); ?>" class="card text-center">
                                    <div class="card-body">
                                        <div class="avatar avatar-50 shadow-sm mb-2 rounded-10 theme-bg text-white">
                                       
                                        </div>
                                        <p class="text-color-theme size-12 small">Fechar</p>
                                    </div>
                                </a>
                            </div>

                           

        <!-- main page content -->

<!-- <iframe src="https://drive.google.com/embeddedfolderview?id=16rAbXMay8M-iXygeGnpOFSgEtzE0VPX9#list" style="width:100%; height:500px; border:0;"></iframe> -->
        <!-- main page content ends -->


    </main>
    <!-- Page ends-->

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <ul class="nav nav-pills nav-justified">
                
                
              <li class="nav-item centerbutton">
                    <div class="nav-link">
                        <span class="theme-radial-gradient">
                    <i class="fa fa-times"></i> <!-- Ícone de fechar (X) do Font Awesome -->
                  </span>
                        <div class="nav-menu-popover justify-content-between">
                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('manual-technician.php');">
                                <i class="bi bi-credit-card size-32"></i><span>Tecnico</span>
                            </button>

                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('manual-user.php');">
                                <i class="bi bi-arrow-up-right-circle size-32"></i><span>Usuario</span>
                            </button>

                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('service.php');">
                                <i class="bi bi-receipt size-32"></i><span>Serviço</span>
                            </button>

                          <button type="button" class="btn btn-lg btn-icon-text"
                            onclick="window.location.replace('notification.php');">
                            <i class="bi bi-receipt size-32"></i><span>Notificações</span>
                          </button>
                          
                          
                           
                         <!--   <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('receivemoney.html');">
                                <i class="bi bi-arrow-down-left-circle size-32"></i><span>Receive</span>
                            </button> -->
                        </div>
                    </div>
                </li>
              <li class="nav-item centerbutton">
                <div class="nav-link">
                  <span class="theme-radial-gradient">
                    <i class="close bi bi-x"></i>
                    
                    <img src="../../assets/img/" class="nav-icon" alt="" />
                  </span>
                  <div class="nav-menu-popover justify-content-between">
                    
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('calendar.php');">
                      <i class="bi bi-receipt size-32"></i><span>Calendario</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('documentation.php');">
                      <i class="bi bi-receipt size-32"></i><span>Documentação</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('graduation.php');">
                      <i class="bi bi-receipt size-32"></i><span>Educação Continuada</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('dialogflow.php');">
                      <i class="bi bi-receipt size-32"></i><span>ChatBot</span>
                    </button>
                    
                    <!--   <button type="button" class="btn btn-lg btn-icon-text"
                    onclick="window.location.replace('receivemoney.html');">
                    <i class="bi bi-arrow-down-left-circle size-32"></i><span>Receive</span>
                    </button> -->
                  </div>
                </div>
              </li>

                <li class="nav-item">
                    <a class="nav-link " href="#">
                        <span>
                            <i class="nav-icon bi bi-wallet2"></i>
                            <span class="nav-text">Camêra</span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </footer>
                          <!-- Camera Modal -->
                          <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-xsm modal-dialog-centered">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h6 class="modal-title" id="cammodalLabel">Camera</h6>
                                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body text-center">
                                  <img src="../../assets/img/infographic-camera.png" alt="" class="mb-4">
                                  <script src="https://unpkg.com/html5-qrcode"></script>
                                  <div id="qr-reader"></div>
                                  <div id="qr-reader-results"></div>
                                  <script>
                                    var resultContainer = document.getElementById('qr-reader-results');
                                    var lastResult, countResults = 0;
                                    function extrairTexto(url) {
                                      var startIndex = url.indexOf("?") + 1; // Encontra o índice do caractere "?" e avança 1 para ignorá-lo
                                      var endIndex = url.indexOf("="); // Encontra o índice do caractere "="
                                      
                                      if (startIndex >= 0 && endIndex >= 0 && endIndex > startIndex) {
                                        return url.substring(startIndex, endIndex); // Extrai o texto entre "?" e "="
                                      } else {
                                        return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                                      }
                                    }
                                    function extrairTextoid(urlid) {
                                      var startIndexid = urlid.indexOf("=") + 1; // Encontra o índice do caractere "=" e avança 1 para ignorá-lo
                                      var endIndexid = urlid.length; // O índice do fim da string
                                      
                                      if (startIndexid >= 0 && endIndexid > startIndexid) {
                                        return urlid.substring(startIndexid, endIndexid); // Extrai o texto após o "=" até o final da string
                                      } else {
                                        return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                                      }
                                    }
                                    
                                    
                                    function onScanSuccess(decodedText, decodedResult) {
                                      if (decodedText !== lastResult) {
                                        ++countResults;
                                        lastResult = decodedText;
                                        // Handle on success condition with the decoded message.
                                        console.log(`Scan result ${decodedText}`, decodedResult);
                                        //  alert(`${decodedText}`, decodedResult);
                                        var textoExtraido = extrairTexto(decodedText);
                                        if (textoExtraido == "equipamento"){
                                          var textoExtraidoid = extrairTextoid(decodedText);
                                          window.location.href = `equipament-manual-user?id=`+textoExtraidoid;
                                          
                                        }
                                        if (textoExtraido == "os"){
                                          var textoExtraidoid = extrairTextoid(decodedText);
                                          window.location.href = `corrective-maintenance-open?id=`+textoExtraidoid;
                                          
                                        }
                                        if (textoExtraido == "id"){
                                          var textoExtraidoid = extrairTextoid(decodedText);
                                          window.location.href = `preventive-maintenance-open?id=`+textoExtraidoid;
                                          
                                        }
                                        
                                      }
                                    }
                                    
                                    var html5QrcodeScanner = new Html5QrcodeScanner(
                                      "qr-reader", { fps: 10, qrbox: 250 });
                                    html5QrcodeScanner.render(onScanSuccess);
                                  </script>
                                  
                                </div>
                                
                              </div>
                            </div>
                          </div>
                          
                          
                          <!-- Camera Modal ends-->
    <!-- Footer ends-->

    <!-- PWA app install toast message -->
  <!--  <div class="position-fixed bottom-0 start-50 translate-middle-x  z-index-10">
        <div class="toast mb-3" role="alert" aria-live="assertive" aria-atomic="true" id="toastinstall"
            data-bs-animation="true">
            <div class="toast-header">
                <img src="../../assets/img/favicon32.png" class="rounded me-2" alt="...">
                <strong class="me-auto">Install PWA App</strong>
                <small>now</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                <div class="row">
                    <div class="col">
                        Click "Install" to install PWA app & experience indepedent.
                    </div>
                    <div class="col-auto align-self-center ps-0">
                        <button class="btn-default btn btn-sm" id="addtohome">Install</button>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- Camera Modal -->
    <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xsm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="cammodalLabel">Autorizar</h6>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                    <img src="assets/img/infographic-camera.png" alt="" class="mb-4">
                    <h3 class="text-color-theme mb-2">Camera Acesso</h3>
                    <p class="text-muted">QRCOD.
                    </p>
                </div>
                <button type="button" class="btn btn-lg btn-primary rounded-15" data-bs-dismiss="modal">Sempre</button>
            </div>
        </div>
    </div>
    <!-- Camera Modal ends-->
    <!-- Required jquery and libraries -->
    <script src="../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/popper.min.js"></script>
           <script src="../../assets/vendor/bootstrap-5/js/bootstrap.bundle.min.js"></script> 

    <!-- cookie js -->
    <script src="../../assets/js/jquery.cookie.js"></script>

    <!-- Customized jquery file  -->
    <script src="../../assets/js/main.js"></script>
    <script src="../../assets/js/color-scheme.js"></script>

    <!-- PWA app service registration and works -->
    <script src="../../assets/js/pwa-services.js"></script>

    <!-- Chart js script -->
            <script src="../../assets/vendor/chart-js-3.3.1/chart.min.js"></script>
 
    <!-- Progress circle js script -->
    <script src="../../assets/vendor/progressbar-js/progressbar.min.js"></script>

    <!-- swiper js script -->
    <script src="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.js"></script>

   <!-- page level custom script -->
            <script src="../../assets/js/app.js"></script> 

</body>

</html>
