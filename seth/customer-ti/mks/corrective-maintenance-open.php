<?php
include("database/database.php");
//include("../../codigo.php");
//$codigoget = (int)($_GET["codigoget"]);
$os = (int)($_GET["id"]);

	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
    setcookie('file', null, -1);

	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$email=	$_SESSION['email'] ;
    $file = "preventive-maintenance";
    setcookie("file","$file");
    $query = "SELECT os_posicionamento.status,os.id_servico,os.id_category,os.restrictions,os.sla,equipamento.data_val,os.sla_term,os.standing,os.time_backlog,os.id_fornecedor,os.id_prioridade,os.id_observacao, os.id_chamado,os.id_tecnico,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo FROM os LEFT JOIN instituicao ON instituicao.id = os.id_unidade LEFT JOIN usuario on usuario.id = os.id_usuario LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor LEFT JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area LEFT JOIN os_status on os_status.id = os.id_status LEFT JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento LEFT JOIN os_carimbo ON os_carimbo.id = os.id_carimbo LEFT JOIN os_workflow on os_workflow.id = os.id_workflow LEFT JOIN equipamento on equipamento.id = os.id_equipamento LEFT JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia where os.id like '$os'";
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($posicionamento,$id_service,$id_category,$restrictions,$sla_second,$data_val,$sla_term,$standing,$time_backlog,$id_fornecedor,$id_prioridade,$id_observacao,$id_chamado,$id_tecnico,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo);
    
    
    
      while ($stmt->fetch()) {
    
      }
    }

?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
      <title>MK Sistema Biomedicos </title>
  <!-- Link para o Font Awesome (se ainda não estiver carregado no projeto) -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <!-- manifest meta -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="manifest" href="../../manifest.json" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="../../assets/img/favicon180.png" sizes="180x180">
    <link rel="icon" href="../../assets/img/favicon32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="../../assets/img/favicon16.png" sizes="16x16" type="image/png">

    <!-- Google fonts-->

    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&amp;display=swap" rel="stylesheet">

    <!-- bootstrap icons -->
    <link rel="stylesheet" href="../../../../../../cdn.jsdelivr.net/npm/bootstrap-icons%401.5.0/font/bootstrap-icons.css">

    <!-- swiper carousel css -->
    <link rel="stylesheet" href="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.css">

    <!-- style css for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet" id="style">
</head>

<body class="body-scroll" data-page="index">

    <!-- loader section -->
    <div class="container-fluid loader-wrap">
        <div class="row h-100">
            <div class="col-10 col-md-6 col-lg-5 col-xl-3 mx-auto text-center align-self-center">
                <div class="loader-cube-wrap loader-cube-animate mx-auto">
                    <img src="../../../framework/img/64x64.png" alt="Logo">
                </div>
                <p class="mt-4">Carregando<br><strong>Aguarde...</strong></p>
            </div>
        </div>
    </div>
    <!-- loader section ends -->

    <!-- Sidebar main menu -->
    <div class="sidebar-wrap  sidebar-pushcontent">
        <!-- Add overlay or fullmenu instead overlay -->
        <div class="closemenu text-muted">Fechar Menu</div>
        <div class="sidebar dark-bg">
            <!-- user information -->
            <div class="row my-3">
                <div class="col-12 ">
                    <div class="card shadow-sm bg-opac text-white border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <figure class="avatar avatar-44 rounded-15">
                                        <img src="logo/clientelogo.png" alt="">
                                    </figure>
                                </div>
                                <div class="col px-0 align-self-center">
                                    <p class="mb-1"> <?php printf($usuariologado); ?></p>
                                    <p class="text-muted size-12"><?php printf($setorlogado); ?></p>
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-44 btn-light">
                                        <i class="bi bi-box-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-opac text-white border-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <h1 class="display-4"></h1>
                                    </div>
                                    <div class="col-auto">
                                        <p class="text-muted"></p>
                                    </div>
                                    <div class="col text-end">
                                        <p class="text-muted"><a href="#" ></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- user emnu navigation -->
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="dashboard">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Dashboard</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="corrective-maintenance">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manutenção Corretiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="corrective-maintenance-new">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Nova Manutenção Corretiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="preventive-maintenance">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manutenção Preventiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-technician">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Tecnico </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-user">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Usuario </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="service">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Serviço </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="config">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Configurações </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>

                    








                        <li class="nav-item">
                            <a class="nav-link" href="../../signin.html" tabindex="-1">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-box-arrow-right"></i></div>
                                <div class="col">Sair</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Sidebar main menu ends -->

    <!-- Begin page -->
    <main class="h-100">

        <!-- Header -->
        <header class="header position-fixed">
            <div class="row">
                <div class="col-auto">
                    <a href="../../javascript:void(0)" target="_self" class="btn btn-light btn-44 menu-btn" >
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="col align-self-center text-center">
                    <div class="logo-small">
                        <img src="../../../framework/img/64x64.png" alt="Logo">
                        <h5>Sistema SETH</h5>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="#" target="_self" class="btn btn-light btn-44">
                        <i class="fa fa-bell"></i>
                        <span class="count-indicator"></span>
                    </a>
                </div>
            </div>
        </header>
        <!-- Header ends -->
        <div class="row mb-4">
                <div class="col-12 text-center">
                    <img src="../../assets/img/infographic-scan.png" alt="">
                </div>
            </div>
 
        <!-- main page content -->
        <div class="main-container container">
            <!-- wallet balance -->
            <div class="card shadow-sm mb-4">
                <div class="card-body">
                    <div class="row">
                         
                        
                    </div>
                </div>
                <div class="card theme-bg text-white border-0 text-center">
                    <div class="card-body">
                        <h1 class="display-1 my-2"><?php printf($os); ?></h1>
                        <p class="text-muted mb-2">Ordem de Serviço</p>
                    </div>
                </div>
            </div>

            <!-- summary -->
            <div class="row mb-3">
                <div class="col-6 col-md-4">
                    <div class="card shadow-sm mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto px-0">
                                    <div class="avatar avatar-40 bg-warning text-white shadow-sm rounded-10-end">
                                        <i class="bi bi-star"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-muted size-12 mb-0">Solicitante </p>
                                    <p><?php printf($id_usuario_nome); ?> <?php printf($id_usuario_sobrenome); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="card shadow-sm mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto px-0">
                                    <div class="avatar avatar-40 bg-success text-white shadow-sm rounded-10-end">
                                        <i class="bi bi-cash-stack"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-muted size-12 mb-0">Setor</p>
                                    <p><?php printf($id_area); ?> <?php printf($id_localizacao); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="card shadow-sm mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto px-0">
                                    <div class="avatar avatar-40 bg-warning text-white shadow-sm rounded-10-end">
                                        <i class="bi bi-star"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-muted size-12 mb-0">Equipamento </p>
                                    <p><?php printf($equipamento_nome); ?> - <?php printf($equipamento_modelo); ?>  - <?php printf($equipamento_codigo); ?> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col">
                    <h6 class="title">Dados da Solicitação</h6>
                </div>
                <div class="col-auto">

                </div>
            </div>
            <div class="row mb-4">
            <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                         
                            <h5 class="text-muted mb-4">Solicitante</h5>
                            <h2 class="mb-0">  <?php printf($id_usuario_nome); ?> <?php printf($id_usuario_sobrenome); ?> </h2>
                           
                        </div>
                    
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                         
                            <h5 class="text-muted mb-4">Solicitação</h5>
                            <h2 class="mb-0"> <?php printf($id_solicitacao); ?> </h2>
                           
                        </div>
                       
                    </div>
                </div>
               
                <div class="row mb-3">
                <div class="col">
                    <h6 class="title">Dados da O.S</h6>
                </div>
                <div class="col-auto">

                </div>
            </div>
            <div class="row mb-4">
            <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                          
                            <h5 class="text-muted mb-4">Tecnico</h5>
                            <h2 class="mb-0">  <?php printf($primeironome_query); ?>  </h2>
                           
                        </div>
                    
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                         
                            <h5 class="text-muted mb-4">Observação</h5>
                            <h2 class="mb-0"> <?php printf($id_observacao); ?> </h2>
                           
                        </div>
                       
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                          
                            <h5 class="text-muted mb-4">Fornecedor</h5>
                            <h2 class="mb-0"> <?php printf($empresa_query); ?> </h2>
                           
                        </div>
                       
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                         
                            <h5 class="text-muted mb-4">Chamado</h5>
                            <h2 class="mb-0"> <?php printf($id_chamado); ?> </h2>
                           
                        </div>
                       
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                          
                            <h5 class="text-muted mb-4">Prioridade</h5>
                            <h2 class="mb-0"> <?php printf($prioridade_smt_query); ?> </h2>
                           
                        </div>
                       
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                         
                            <h5 class="text-muted mb-4">SLA</h5>
                            <h2 class="mb-0"> <?php printf($sla_term); ?> </h2>
                           
                        </div>
                       
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                         
                            <h5 class="text-muted mb-4">Posicionamento</h5>
                            <h2 class="mb-0"> <?php printf($os_posicionamento); ?> </h2>
                           
                        </div>
                       
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                         
                            <h5 class="text-muted mb-4">Status</h5>
                            <h2 class="mb-0"> <?php printf($os_status); ?> </h2>
                           
                        </div>
                       
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                        
                            <h5 class="text-muted mb-4">Categoria</h5>
                            <h2 class="mb-0"> <?php printf($category); ?> </h2>
                           
                        </div>
                       
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card mb-4 text-center shadow-sm">
                        <div class="card-body">
                          
                            <h5 class="text-muted mb-4">Serviço</h5>
                            <h2 class="mb-0"> <?php printf($custom_service); ?> </h2>
                           
                        </div>
                       
                    </div>
                </div>
              <div class="col-12 col-md-4">
                <div class="card mb-4 text-center shadow-sm">
                  <div class="card-body">
                    
                    <h5 class="text-muted mb-4">Anexo</h5>
                    <div class="col-12 text-center">
                      <img src="http://seth.mksistemasbiomedicos.com.br/customer-ec/<?php echo $key; ?>/dropzone/os/<?php echo $id_anexo; ?>" />
                      
                    </div>

                  </div>
                  
                </div>
              </div>
               
              <div class="col-12 col-md-8 col-lg-4 mx-auto">
                <div class="card shadow-sm mb-4">
                  <div class="card-header">
                    <h6 class="my-1">Anexos</h6>
                  </div>
                  <div class="card-body bg-light">
                    <ul class="list-group list-group-flush w-100 bubble-sheet log-information">
                      
                      <?php
                        $query="SELECT id, file, titulo, reg_date, upgrade FROM regdate_os_dropzone WHERE id_os like '$os' ORDER BY id  DESC";
                        $row=1;
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);                          
                      ?>
                      <?php  while ($stmt->fetch()) { ?>
                      <li class="list-group-item">
                        <div class="avatar avatar-15 border-danger rounded-circle"></div>
                        <p><span class="text-color-theme">Titulo: <?php printf($titulo); ?></span>
                          <br><small class="text-muted">Data: <?php printf($reg_date); ?></small><br><div class="col-auto">
                            <div class="avatar avatar-44 coverimg rounded-10">
                              <img src="http://seth.mksistemasbiomedicos.com.br/customer-ec/<?php echo $key; ?>/dropzone/mc-dropzone/<?php echo $id_anexo; ?>" />

                            </div>
                          </div> </p>
                        
                        <?php  $row=$row+1; }
                        }   ?>
                      </li>
                      
                    </ul>
                  </div>
                  <div class="card-footer">
                    <div class="row">
                      
                      
                    </div>
                  </div>
                </div>
              </div>

                 
                <div class="col-12 col-md-8 col-lg-4 mx-auto">
                    <div class="card shadow-sm mb-4">
                        <div class="card-header">
                            <h6 class="my-1">Posicionamento</h6>
                        </div>
                        <div class="card-body bg-light">
                            <ul class="list-group list-group-flush w-100 bubble-sheet log-information">
                              
                                <?php
$query="SELECT regdate_os_posicionamento.data_end,regdate_os_posicionamento.detail,regdate_os_posicionamento.id_os, regdate_os_posicionamento.id_posicionamento, regdate_os_posicionamento.id_status, regdate_os_posicionamento.reg_date, os_posicionamento.id, os_posicionamento.status, os_status.id, os_status.status FROM  regdate_os_posicionamento LEFT JOIN os_posicionamento ON regdate_os_posicionamento.id_posicionamento = os_posicionamento.id LEFT JOIN os_status ON regdate_os_posicionamento.id_status = os_status.id where regdate_os_posicionamento.id_os = '$os '";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($data_end,$detail,$id_os,$id_posicionamento,$id_status,$regdate,$id,$status_posicionamento,$id,$status_os);

?>
          <?php  while ($stmt->fetch()) { ?>
            <li class="list-group-item">
                                    <div class="avatar avatar-15 border-danger rounded-circle"></div>
                                    <p><span class="text-color-theme">Pocicionamento: <?php printf($status_posicionamento); ?></span>
                                       <br><small class="text-muted">Detalhe: <?php printf($detail); ?></small><br><small class="text-muted">Prazo: <?php printf($data_end); ?></small></p>
                              
                                        <?php  $row=$row+1; }
}   ?>
                                    </li>
                             
                            </ul>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                
                               
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-8 col-lg-4 mx-auto">
                    <div class="card shadow-sm mb-4">
                        <div class="card-header">
                            <h6 class="my-1">Registro</h6>
                        </div>
                        <div class="card-body bg-light">
                            <ul class="list-group list-group-flush w-100 bubble-sheet log-information">
                              
                                <?php
$query="SELECT regdate_os_registro.id_os, regdate_os_registro.id_user,regdate_os_registro.reparo,regdate_os_registro.date_start,regdate_os_registro.date_end,regdate_os_registro.time,regdate_os_registro.reg_date, regdate_os_registro.upgrade, colaborador.id, colaborador.primeironome FROM regdate_os_registro LEFT JOIN colaborador on regdate_os_registro.id_user = colaborador.id WHERE regdate_os_registro.id_os like '$os'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_os,$id_user,$reparo,$date_start,$date_end,$time,$reg_date,$upgrade,$id,$usuario);

?>
          <?php  while ($stmt->fetch()) { ?>
            <li class="list-group-item">
                                    <div class="avatar avatar-15 border-danger rounded-circle"></div>
                                    <p><span class="text-color-theme">Executante: <?php printf($usuario); ?></span>
                                        <br><small class="text-muted">Inicio: <?php printf($date_start); ?></small>  <br><small class="text-muted">Termino: <?php printf($date_end); ?></small><br><small class="text-muted">Reparo: <?php printf($reparo); ?></small></p>
                              
                                        <?php  $row=$row+1; }
}   ?>
                                    </li>
                             
                            </ul>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                
                               
                            </div>
                        </div>
                    </div>
                </div>

               <!-- service provider -->
               <div class="row mb-3">
                <div class="col">
                    <h6 class="title">Ferramentas</h6>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-12 px-0">
                    <!-- swiper users connections -->
                    <div class="swiper-container connectionwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="corrective-maintenance-open-pos?id= <?php printf($os); ?>" class="card text-center">
                                    <div class="card-body">
                                        <div class="avatar avatar-50 shadow-sm mb-2 rounded-10 theme-bg text-white">
                                         </div>
                                        <p class="text-color-theme size-12 small">Posicionamento</p>
                                    </div>
                                </a>
                            </div>

                            <div class="swiper-slide">
                            <a href="corrective-maintenance-open-reg?id= <?php printf($os); ?>" class="card text-center">
                                    <div class="card-body">
                                        <div class="avatar avatar-50 shadow-sm mb-2 rounded-10 theme-bg text-white">
                                         
                                        </div>
                                        <p class="text-color-theme size-12 small">Registro</p>
                                    </div>
                                </a>
                            </div>
                          <div class="swiper-slide">
                            <a href="corrective-maintenance-open-upload?id= <?php printf($os); ?>" class="card text-center">
                              <div class="card-body">
                                <div class="avatar avatar-50 shadow-sm mb-2 rounded-10 theme-bg text-white">
                                  
                                </div>
                                <p class="text-color-theme size-12 small">Anexo</p>
                              </div>
                            </a>
                          </div>
                            <div class="swiper-slide">
                            <a href="corrective-maintenance-open-close?id= <?php printf($os); ?>" class="card text-center">
                                    <div class="card-body">
                                        <div class="avatar avatar-50 shadow-sm mb-2 rounded-10 theme-bg text-white">
                                       
                                        </div>
                                        <p class="text-color-theme size-12 small">Fechar</p>
                                    </div>
                                </a>
                            </div>

                           
                           
                           

                          
                        

                      
                           

                        </div>
                    </div>
                </div>
            </div>

            <!-- tabs structure 
            <ul class="nav nav-pills nav-justified tabs mb-3" id="assetstabs" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#cards"
                        type="button" role="tab" aria-controls="cards" aria-selected="true">Posicionamento</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="currency-tab" data-bs-toggle="tab" data-bs-target="#currency"
                        type="button" role="tab" aria-controls="currency" aria-selected="false">Fechar</button>
                </li>
            </ul>
         <!--    <div class="tab-content" id="assetstabsContent">
                <div class="tab-pane fade show active" id="cards" role="tabpanel" >
                    <!-- swiper credit cards 
                    <div class="row mb-3">
                        <div class="col-12 px-0">
                            <div class="swiper-container cardswiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="card dark-bg">
                                            <div class="card-body">
                                                <div class="row mb-3">
                                                    <div class="col-auto align-self-center">
                                                        <img src="assets/img/masterocard.png" alt="">
                                                    </div>
                                                    <div class="col align-self-center text-end">
                                                        <p class="small">
                                                            <span class="text-uppercase size-10">Validity</span><br>
                                                            <span class="text-muted">06/25</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h4 class="fw-normal mb-2">
                                                            56040.00
                                                            <span class="small text-muted">USD</span>
                                                        </h4>
                                                        <p class="mb-0 text-muted size-12">10141 0021 0001 0154</p>
                                                        <p class="text-muted size-12">Debit Card</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="card theme-radial-gradient border-0">
                                            <div class="card-body">
                                                <div class="row mb-3">
                                                    <div class="col-auto align-self-center">
                                                        <i class="bi bi-wallet2"></i> Wallet
                                                    </div>
                                                    <div class="col align-self-center text-end">
                                                        <p class="small">
                                                            <span class="text-uppercase size-10">Validity</span><br>
                                                            <span class="text-muted">Unlimited</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h4 class="fw-normal mb-2">
                                                            100.00
                                                            <span class="small text-muted">USD</span>
                                                        </h4>
                                                        <p class="mb-0 text-muted size-12">10141 0021 0001 0154</p>
                                                        <p class="text-muted size-12">Debit Card</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row mb-3">
                                                    <div class="col-auto align-self-center">
                                                        <img src="assets/img/masterocard.png" alt="">
                                                    </div>
                                                    <div class="col align-self-center text-end">
                                                        <p class="small">
                                                            <span class="text-uppercase size-10">Validity</span><br>
                                                            <span class="text-muted">09/24</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h4 class="fw-normal mb-2">
                                                            150540.00
                                                            <span class="small text-muted">USD</span>
                                                        </h4>
                                                        <p class="mb-0 text-muted size-12">10141 0021 0001 0154</p>
                                                        <p class="text-muted size-12">Debit Card</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Transactions 
                    <div class="row mb-3">
                        <div class="col">
                            <h6 class="title">Transactions</h6>
                        </div>                        
                        <div class="col-auto">
                            <a href="#" class="small">Add Card</a>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-12 px-0">
                            <ul class="list-group list-group-flush bg-none">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10 ">
                                                <img src="assets/img/company4.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Zomato</p>
                                            <p class="text-muted size-12">Food</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0">-25.00</p>
                                            <p class="text-muted size-12">Debit Card 4545</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10">
                                                <img src="assets/img/company5.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Uber</p>
                                            <p class="text-muted size-12">Travel</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0">-26.00</p>
                                            <p class="text-muted size-12">Debit Card 4545</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10">
                                                <img src="assets/img/company1.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Starbucks</p>
                                            <p class="text-muted size-12">Food</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0">-18.00</p>
                                            <p class="text-muted size-12">Cash</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10">
                                                <img src="assets/img/company3.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Walmart</p>
                                            <p class="text-muted size-12">Clothing</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0">-105.00</p>
                                            <p class="text-muted size-12">Wallet</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div> -->

                </div>
                <div class="tab-pane fade" id="currency" role="tabpanel" aria-labelledby="currency-tab">
                 <!--   <div class="row">
                        <div class="col-12 col-md-6 align-self-center">
                            <div class="chartdoughnut mb-4">
                                <div class="datadisplay text-center shadow">
                                    <h1 class="fw-normal">15.56k</h1>
                                    <p class="text-muted">Expense</p>
                                </div>
                                <canvas id="doughnutchart"></canvas>
                            </div>
                        </div>
                    </div>
                        
                    <!-- Transactions 
                    <div class="row mb-3">
                        <div class="col">
                            <h6 class="title">Currencies</h6>
                        </div>                        
                        <div class="col-auto">
                            <a href="#" class="small">Add Currency</a>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-12 px-0">
                            <ul class="list-group list-group-flush bg-none">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10 bg-warning text-white">
                                                <span class="vm d-inline-block">$</span>
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">US Dollar</p>
                                            <p class="text-muted size-12">USD</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0 fw-bold">25485.00</p>
                                            <p class="text-muted size-12 text-success">2.4 EUR (0.15%)</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10 bg-success text-white">
                                                <span class="vm d-inline-block">O</span>
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Other</p>
                                            <p class="text-muted size-12">DGC, DRC, FIT</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0 fw-bold">1001.36</p>
                                            <p class="text-muted size-12 text-success">1.65 USD (0.10%)</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10">
                                                <img src="assets/img/company6.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">BitCoin</p>
                                            <p class="text-muted size-12">BTC</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0 fw-bold">150.00</p>
                                            <p class="text-muted size-12 text-danger">1.65 USD (0.08%)</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 shadow rounded-10">
                                                <img src="assets/img/company7.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="col align-self-center ps-0">
                                            <p class="text-color-theme mb-0">Etherium</p>
                                            <p class="text-muted size-12">ETH</p>
                                        </div>
                                        <div class="col align-self-center text-end">
                                            <p class="mb-0 fw-bold">146.00</p>
                                            <p class="text-muted size-12 text-success">2.65 USD (0.16%)</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div> 
                </div>
            </div>-->

        </div>
        <!-- main page content ends -->



        <!-- main page content -->

<!-- <iframe src="https://drive.google.com/embeddedfolderview?id=16rAbXMay8M-iXygeGnpOFSgEtzE0VPX9#list" style="width:100%; height:500px; border:0;"></iframe> -->
        <!-- main page content ends -->


    </main>
    <!-- Page ends-->

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <ul class="nav nav-pills nav-justified">
                
                
              <li class="nav-item centerbutton">
                    <div class="nav-link">
                        <span class="theme-radial-gradient">
                    <i class="fa fa-times"></i> <!-- Ícone de fechar (X) do Font Awesome -->
                  </span>
                        <div class="nav-menu-popover justify-content-between">
                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('manual-technician.php');">
                                <i class="bi bi-credit-card size-32"></i><span>Tecnico</span>
                            </button>

                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('manual-user.php');">
                                <i class="bi bi-arrow-up-right-circle size-32"></i><span>Usuario</span>
                            </button>

                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('service.php');">
                                <i class="bi bi-receipt size-32"></i><span>Serviço</span>
                            </button>

                          <button type="button" class="btn btn-lg btn-icon-text"
                            onclick="window.location.replace('notification.php');">
                            <i class="bi bi-receipt size-32"></i><span>Notificações</span>
                          </button>
                          
                          
                           
                         <!--   <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('receivemoney.html');">
                                <i class="bi bi-arrow-down-left-circle size-32"></i><span>Receive</span>
                            </button> -->
                        </div>
                    </div>
                </li>
              <li class="nav-item centerbutton">
                <div class="nav-link">
                  <span class="theme-radial-gradient">
                    <i class="close bi bi-x"></i>
                    
                    <img src="../../assets/img/" class="nav-icon" alt="" />
                  </span>
                  <div class="nav-menu-popover justify-content-between">
                    
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('calendar.php');">
                      <i class="bi bi-receipt size-32"></i><span>Calendario</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('documentation.php');">
                      <i class="bi bi-receipt size-32"></i><span>Documentação</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('graduation.php');">
                      <i class="bi bi-receipt size-32"></i><span>Educação Continuada</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('dialogflow.php');">
                      <i class="bi bi-receipt size-32"></i><span>ChatBot</span>
                    </button>
                    
                    <!--   <button type="button" class="btn btn-lg btn-icon-text"
                    onclick="window.location.replace('receivemoney.html');">
                    <i class="bi bi-arrow-down-left-circle size-32"></i><span>Receive</span>
                    </button> -->
                  </div>
                </div>
              </li>

                <li class="nav-item">
                    <a class="nav-link " href="#">
                        <span>
                            <i class="nav-icon bi bi-wallet2"></i>
                            <span class="nav-text">Camêra</span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </footer>
              <!-- Camera Modal -->
              <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xsm modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h6 class="modal-title" id="cammodalLabel">Camera</h6>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center">
                      <img src="../../assets/img/infographic-camera.png" alt="" class="mb-4">
                      <script src="https://unpkg.com/html5-qrcode"></script>
                      <div id="qr-reader"></div>
                      <div id="qr-reader-results"></div>
                      <script>
                        var resultContainer = document.getElementById('qr-reader-results');
                        var lastResult, countResults = 0;
                        function extrairTexto(url) {
                          var startIndex = url.indexOf("?") + 1; // Encontra o índice do caractere "?" e avança 1 para ignorá-lo
                          var endIndex = url.indexOf("="); // Encontra o índice do caractere "="
                          
                          if (startIndex >= 0 && endIndex >= 0 && endIndex > startIndex) {
                            return url.substring(startIndex, endIndex); // Extrai o texto entre "?" e "="
                          } else {
                            return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                          }
                        }
                        function extrairTextoid(urlid) {
                          var startIndexid = urlid.indexOf("=") + 1; // Encontra o índice do caractere "=" e avança 1 para ignorá-lo
                          var endIndexid = urlid.length; // O índice do fim da string
                          
                          if (startIndexid >= 0 && endIndexid > startIndexid) {
                            return urlid.substring(startIndexid, endIndexid); // Extrai o texto após o "=" até o final da string
                          } else {
                            return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                          }
                        }
                        
                        
                        function onScanSuccess(decodedText, decodedResult) {
                          if (decodedText !== lastResult) {
                            ++countResults;
                            lastResult = decodedText;
                            // Handle on success condition with the decoded message.
                            console.log(`Scan result ${decodedText}`, decodedResult);
                            //  alert(`${decodedText}`, decodedResult);
                            var textoExtraido = extrairTexto(decodedText);
                            if (textoExtraido == "equipamento"){
                              var textoExtraidoid = extrairTextoid(decodedText);
                              window.location.href = `equipament-manual-user?id=`+textoExtraidoid;
                              
                            }
                            if (textoExtraido == "os"){
                              var textoExtraidoid = extrairTextoid(decodedText);
                              window.location.href = `corrective-maintenance-open?id=`+textoExtraidoid;
                              
                            }
                            if (textoExtraido == "id"){
                              var textoExtraidoid = extrairTextoid(decodedText);
                              window.location.href = `preventive-maintenance-open?id=`+textoExtraidoid;
                              
                            }
                            
                          }
                        }
                        
                        var html5QrcodeScanner = new Html5QrcodeScanner(
                          "qr-reader", { fps: 10, qrbox: 250 });
                        html5QrcodeScanner.render(onScanSuccess);
                      </script>
                      
                    </div>
                    
                  </div>
                </div>
              </div>
              
              
              <!-- Camera Modal ends-->
    <!-- Footer ends-->

    <!-- PWA app install toast message -->
  <!--  <div class="position-fixed bottom-0 start-50 translate-middle-x  z-index-10">
        <div class="toast mb-3" role="alert" aria-live="assertive" aria-atomic="true" id="toastinstall"
            data-bs-animation="true">
            <div class="toast-header">
                <img src="../../assets/img/favicon32.png" class="rounded me-2" alt="...">
                <strong class="me-auto">Install PWA App</strong>
                <small>now</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                <div class="row">
                    <div class="col">
                        Click "Install" to install PWA app & experience indepedent.
                    </div>
                    <div class="col-auto align-self-center ps-0">
                        <button class="btn-default btn btn-sm" id="addtohome">Install</button>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- Camera Modal -->
    <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xsm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="cammodalLabel">Autorizar</h6>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                    <img src="assets/img/infographic-camera.png" alt="" class="mb-4">
                    <h3 class="text-color-theme mb-2">Camera Acesso</h3>
                    <p class="text-muted">QRCOD.
                    </p>
                </div>
                <button type="button" class="btn btn-lg btn-primary rounded-15" data-bs-dismiss="modal">Sempre</button>
            </div>
        </div>
    </div>
    <!-- Camera Modal ends-->
    <!-- Required jquery and libraries -->
    <script src="../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/popper.min.js"></script>
           <script src="../../assets/vendor/bootstrap-5/js/bootstrap.bundle.min.js"></script> 

    <!-- cookie js -->
    <script src="../../assets/js/jquery.cookie.js"></script>

    <!-- Customized jquery file  -->
    <script src="../../assets/js/main.js"></script>
    <script src="../../assets/js/color-scheme.js"></script>

    <!-- PWA app service registration and works -->
    <script src="../../assets/js/pwa-services.js"></script>

    <!-- Chart js script -->
            <script src="../../assets/vendor/chart-js-3.3.1/chart.min.js"></script>
 
    <!-- Progress circle js script -->
    <script src="../../assets/vendor/progressbar-js/progressbar.min.js"></script>

    <!-- swiper js script -->
    <script src="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.js"></script>

   <!-- page level custom script -->
            <script src="../../assets/js/app.js"></script> 

</body>

</html>
              