<?php
include("database/database.php");
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
    setcookie('file', null, -1);

	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$email=	$_SESSION['email'] ;
    $file = "dashboard";
    setcookie("file","$file")


?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
      <title>MK Sistema Biomedicos </title>
  <!-- Link para o Font Awesome (se ainda não estiver carregado no projeto) -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <!-- manifest meta -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="manifest" href="../../manifest.json" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="../../assets/img/favicon180.png" sizes="180x180">
    <link rel="icon" href="../../assets/img/favicon32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="../../assets/img/favicon16.png" sizes="16x16" type="image/png">

    <!-- Google fonts-->

    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&amp;display=swap" rel="stylesheet">

    <!-- bootstrap icons -->
    <link rel="stylesheet" href="../../../../cdn.jsdelivr.net/npm/bootstrap-icons%401.5.0/font/bootstrap-icons.css">

    <!-- swiper carousel css -->
    <link rel="stylesheet" href="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.css">

    <!-- style css for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet" id="style">
</head>

<body class="body-scroll" data-page="index">

    <!-- loader section -->
    <div class="container-fluid loader-wrap">
        <div class="row h-100">
            <div class="col-10 col-md-6 col-lg-5 col-xl-3 mx-auto text-center align-self-center">
                <div class="loader-cube-wrap loader-cube-animate mx-auto">
                    <img src="../../../framework/img/64x64.png" alt="Logo">
                </div>
                <p class="mt-4">Carregando<br><strong>Aguarde...</strong></p>
            </div>
        </div>
    </div>
    <!-- loader section ends -->

    <!-- Sidebar main menu -->
    <div class="sidebar-wrap  sidebar-pushcontent">
        <!-- Add overlay or fullmenu instead overlay -->
        <div class="closemenu text-muted">Fechar Menu</div>
        <div class="sidebar dark-bg">
            <!-- user information -->
            <div class="row my-3">
                <div class="col-12 ">
                    <div class="card shadow-sm bg-opac text-white border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <figure class="avatar avatar-44 rounded-15">
                                        <img src="logo/clientelogo.png" alt="">
                                    </figure>
                                </div>
                                <div class="col px-0 align-self-center">
                                    <p class="mb-1"> <?php printf($usuariologado); ?></p>
                                    <p class="text-muted size-12"><?php printf($setorlogado); ?></p>
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-44 btn-light">
                                        <i class="bi bi-box-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-opac text-white border-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <h1 class="display-4"></h1>
                                    </div>
                                    <div class="col-auto">
                                        <p class="text-muted"></p>
                                    </div>
                                    <div class="col text-end">
                                        <p class="text-muted"><a href="#" ></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- user emnu navigation -->
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="dashboard">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Dashboard</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="corrective-maintenance">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manutenção Corretiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="corrective-maintenance-new">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Nova Manutenção Corretiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="preventive-maintenance">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manutenção Preventiva</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-technician">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Tecnico </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="manual-user">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Manual Usuario </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="service">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Serviço </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="config">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-house-door"></i></div>
                                <div class="col">Configurações </div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>









                        <li class="nav-item">
                            <a class="nav-link" href="../../signin.html" tabindex="-1">
                                <div class="avatar avatar-40 rounded icon"><i class="bi bi-box-arrow-right"></i></div>
                                <div class="col">Sair</div>
                                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Sidebar main menu ends -->

    <!-- Begin page -->
    <main class="h-100">

        <!-- Header -->
        <header class="header position-fixed">
            <div class="row">
                <div class="col-auto">
                    <a href="../../javascript:void(0)" target="_self" class="btn btn-light btn-44 menu-btn" >
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="col align-self-center text-center">
                    <div class="logo-small">
                        <img src="../../../framework/img/64x64.png" alt="Logo">
                        <h5>Sistema SETH-c</h5>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="#" target="_self" class="btn btn-light btn-44">
                        <i class="fa fa-bell"></i>
                        <span class="count-indicator"></span>
                    </a>
                </div>
            </div>
        </header>
        <!-- Header ends -->

        <!-- main page content -->
        <div class="main-container container">
            <!-- welcome user -->
            <div class="row mb-4">
                <div class="col-auto">
                    <div class="avatar avatar-50 shadow rounded-10">
                           <img src="logo/clientelogo.png" alt="">
                    </div>
                </div>
                <div class="col align-self-center ps-0">
                    <h4 class="text-color-theme"><span class="fw-normal">Olá!</span>, <?php printf($usuariologado); ?></h4>
                    <p class="text-muted">Bem Vindo!</p>
                </div>
            </div>
             <!-- offers banner -->
            <div class="row mb-4">
                <div class="col-12">
                    <div class="card theme-bg text-center">
                        <div class="card-body">
                            <div class="row">
                                <div class="col align-self-center">
                                    <h1>Engenharia Clínica</h1>
                                    <p class="size-12 text-muted">
                                       Gerenciamento
                                    </p>
                                    <div class="tag border-dashed border-opac">
                                   
                                    </div>
                                </div>
                                <div class="col-6 align-self-center ps-0">
                                    <!--  <img src="../../../framework/img/logo.png" alt="" class="mw-100"> -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Dark mode switch -->
            <div class="row mb-4">
                <div class="col-12">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" id="darkmodeswitch">
                                <label class="form-check-label text-muted px-2 " for="darkmodeswitch">Modo Escuro</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Saving targets -->
            <div class="row mb-3">
                <div class="col">
                    <h6 class="title">Painel</h6>
                </div>
                <div class="col-auto">

                </div>
            </div>
            <div class="row mb-4">
                <div class="col-6 col-md-4 col-lg-3">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <div class="circle-small">
                                        <div id="circleprogressone"></div>
                                        <div class="avatar avatar-30 alert-primary text-primary rounded-circle">
                                            <i class="bi bi-globe"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto align-self-center ps-0">
                                    <p class="small mb-1 text-muted">Total O.S</p>
                                    <p><?php
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM os ");
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) {
                          printf($result);
                          }
                          ?><span class="small"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-6 col-md-4 col-lg-3">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <div class="circle-small">
                                        <div id="circleprogressone"></div>
                                        <div class="avatar avatar-30 alert-primary text-primary rounded-circle">
                                            <i class="bi bi-globe"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto align-self-center ps-0">
                                    <p class="small mb-1 text-muted">Solicitado O.S</p>
                                    <p><?php
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM os where id_status = 1 ");
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) {
                          printf($result);
                          }
                          ?><span class="small"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-6 col-md-4 col-lg-3">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <div class="circle-small">
                                        <div id="circleprogressone"></div>
                                        <div class="avatar avatar-30 alert-primary text-primary rounded-circle">
                                            <i class="bi bi-globe"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto align-self-center ps-0">
                                    <p class="small mb-1 text-muted">Aberto O.S</p>
                                    <p><?php
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM os where id_status = 2 ");
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) {
                          printf($result);
                          }
                          ?><span class="small"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <div class="circle-small">
                                        <div id="circleprogresstwo"></div>
                                        <div class="avatar avatar-30 alert-success text-success rounded-circle">
                                            <i class="bi bi-cash-stack"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto align-self-center ps-0">
                                    <p class="small mb-1 text-muted">Total MP </p>
                                    <p><?php
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive ");
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) {
                          printf($result);
                          }
                          ?><span class="small"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-6 col-md-4 col-lg-3">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <div class="circle-small">
                                        <div id="circleprogresstwo"></div>
                                        <div class="avatar avatar-30 alert-success text-success rounded-circle">
                                            <i class="bi bi-cash-stack"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto align-self-center ps-0">
                                    <p class="small mb-1 text-muted">Aberto MP </p>
                                    <p><?php
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive where id_status = 1");
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) {
                          printf($result);
                          }
                          ?><span class="small"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="col-6 col-md-4 col-lg-3">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <div class="circle-small">
                                        <div id="circleprogresstwo"></div>
                                        <div class="avatar avatar-30 alert-success text-success rounded-circle">
                                            <i class="bi bi-cash-stack"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto align-self-center ps-0">
                                    <p class="small mb-1 text-muted">Atrasada MP </p>
                                    <p><?php
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive where id_status = 2");
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) {
                          printf($result);
                          }
                          ?><span class="small"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>

               <!-- main page content ends -->


          </main>
    <!-- Page ends-->

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <ul class="nav nav-pills nav-justified">
                
                
                <li class="nav-item centerbutton">
                    <div class="nav-link">
                        <span class="theme-radial-gradient">
                    <i class="fa fa-times"></i> <!-- Ícone de fechar (X) do Font Awesome -->
                  </span>
                        <div class="nav-menu-popover justify-content-between">
                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('manual-technician.php');">
                                <i class="bi bi-credit-card size-32"></i><span>Tecnico</span>
                            </button>

                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('manual-user.php');">
                                <i class="bi bi-arrow-up-right-circle size-32"></i><span>Usuario</span>
                            </button>

                            <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('service.php');">
                                <i class="bi bi-receipt size-32"></i><span>Serviço</span>
                            </button>

                          <button type="button" class="btn btn-lg btn-icon-text"
                            onclick="window.location.replace('notification.php');">
                            <i class="bi bi-receipt size-32"></i><span>Notificações</span>
                          </button>
           
                        </div>
                    </div>
                </li>
              <li class="nav-item centerbutton">
                <div class="nav-link">
                  <span class="theme-radial-gradient">
                    <i class="close bi bi-x"></i>
                    
                    <img src="../../assets/img/" class="nav-icon" alt="" />
                  </span>
                  <div class="nav-menu-popover justify-content-between">
                    
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('calendar.php');">
                      <i class="bi bi-receipt size-32"></i><span>Calendario</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('documentation.php');">
                      <i class="bi bi-receipt size-32"></i><span>Documentação</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('graduation.php');">
                      <i class="bi bi-receipt size-32"></i><span>Educação Continuada</span>
                    </button>
                    <button type="button" class="btn btn-lg btn-icon-text"
                      onclick="window.location.replace('dialogflow.php');">
                      <i class="bi bi-receipt size-32"></i><span>ChatBot</span>
                    </button>
           
                  </div>
                </div>
              </li>
              <li class="nav-item">
                <a class="btn btn-default btn-lg mb-3 px-4 shadow-sm" data-bs-target="#cammodal"
                  data-bs-toggle="modal">
                  <span>
                    <i class="nav-icon bi bi-house"></i>
                    <span class="nav-text">Câmera</span>
                  </span>
                </a>
              </li>
            
            </ul>
        </div>
    </footer>
    <!-- Footer ends-->
          <!-- Camera Modal -->
          <div class="modal fade" id="cammodal" tabindex="-1" aria-labelledby="cammodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xsm modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h6 class="modal-title" id="cammodalLabel">Camera</h6>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                  <img src="../../assets/img/infographic-camera.png" alt="" class="mb-4">
                  <script src="https://unpkg.com/html5-qrcode"></script>
                  <div id="qr-reader"></div>
                  <div id="qr-reader-results"></div>
                  <script>
                    var resultContainer = document.getElementById('qr-reader-results');
                    var lastResult, countResults = 0;
                    function extrairTexto(url) {
                      var startIndex = url.indexOf("?") + 1; // Encontra o índice do caractere "?" e avança 1 para ignorá-lo
                      var endIndex = url.indexOf("="); // Encontra o índice do caractere "="
                      
                      if (startIndex >= 0 && endIndex >= 0 && endIndex > startIndex) {
                        return url.substring(startIndex, endIndex); // Extrai o texto entre "?" e "="
                      } else {
                        return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                      }
                    }
                    function extrairTextoid(urlid) {
                      var startIndexid = urlid.indexOf("=") + 1; // Encontra o índice do caractere "=" e avança 1 para ignorá-lo
                      var endIndexid = urlid.length; // O índice do fim da string
                      
                      if (startIndexid >= 0 && endIndexid > startIndexid) {
                        return urlid.substring(startIndexid, endIndexid); // Extrai o texto após o "=" até o final da string
                      } else {
                        return null; // Retorna null se os índices não forem encontrados ou estiverem em uma ordem inválida
                      }
                    }


                    function onScanSuccess(decodedText, decodedResult) {
                      if (decodedText !== lastResult) {
                        ++countResults;
                        lastResult = decodedText;
                        // Handle on success condition with the decoded message.
                        console.log(`Scan result ${decodedText}`, decodedResult);
                      //  alert(`${decodedText}`, decodedResult);
                        var textoExtraido = extrairTexto(decodedText);
                        if (textoExtraido == "equipamento"){
                       var textoExtraidoid = extrairTextoid(decodedText);
                          window.location.href = `equipament-manual-user?id=`+textoExtraidoid;

                        }
                        if (textoExtraido == "os"){
                          var textoExtraidoid = extrairTextoid(decodedText);
                          window.location.href = `corrective-maintenance-open?id=`+textoExtraidoid;
                          
                        }
                        if (textoExtraido == "id"){
                          var textoExtraidoid = extrairTextoid(decodedText);
                          window.location.href = `preventive-maintenance-open?id=`+textoExtraidoid;
                          
                        }
                        
                      }
                    }
                    
                    var html5QrcodeScanner = new Html5QrcodeScanner(
                      "qr-reader", { fps: 10, qrbox: 250 });
                    html5QrcodeScanner.render(onScanSuccess);
                  </script>

                </div>
                
              </div>
            </div>
          </div>
          
          
          <!-- Camera Modal ends-->
          <?php
            date_default_timezone_set('America/Sao_Paulo');
            $today = date("Y-m-d");
            $read_open = 1;
            
            $query = "SELECT id, origem, ref, data, read_open FROM notif WHERE read_open = 1";
            
            $resultados = $conn->query($query);
            $notifications = array();
            
            while ($row = mysqli_fetch_assoc($resultados)) {
              $notifications[] = $row;
            }
            
            $conn->close();
          ?>
          
          <script>
            if ('serviceWorker' in navigator) {
              window.addEventListener('load', function() {
                navigator.serviceWorker.register('service-worker.js').then(function(registration) {
                  console.log('ServiceWorker registration successful with scope: ', registration.scope);
                }, function(err) {
                  console.log('ServiceWorker registration failed: ', err);
                });
              });
            }
            
            function showNotification(title, options) {
              if (Notification.permission === "granted") {
                var notification = new Notification(title, options);
              } else if (Notification.permission !== 'denied') {
                Notification.requestPermission().then(function(permission) {
                  if (permission === "granted") {
                    var notification = new Notification(title, options);
                  }
                });
              }
            }
            
            window.onload = function() {
              <?php foreach ($notifications as $notification): ?>
              var title = "<?php echo $notification['origem']; ?>";
              var options = {
                body: "<?php echo $notification['ref']; ?>",
                // Adicione mais opções de notificação conforme necessário
              };
              showNotification(title, options);
              <?php endforeach; ?>
            };
          </script>
    <!-- Required jquery and libraries -->
    <script src="../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/popper.min.js"></script>
    <script src="../../assets/vendor/bootstrap-5/js/bootstrap.bundle.min.js"></script>

    <!-- cookie js -->
    <script src="../../assets/js/jquery.cookie.js"></script>

    <!-- Customized jquery file  -->
    <script src="../../assets/js/main.js"></script>
    <script src="../../assets/js/color-scheme.js"></script>

    <!-- PWA app service registration and works -->
    <script src="../../assets/js/pwa-services.js"></script>

    <!-- Chart js script -->
    <script src="../../assets/vendor/chart-js-3.3.1/chart.min.js"></script>
 
    <!-- Progress circle js script -->
    <script src="../../assets/vendor/progressbar-js/progressbar.min.js"></script>

    <!-- swiper js script -->
    <script src="../../assets/vendor/swiperjs-6.6.2/swiper-bundle.min.js"></script>

    <!-- page level custom script -->
     <script src="../../assets/js/app.js"></script> 

</body>

</html>
          