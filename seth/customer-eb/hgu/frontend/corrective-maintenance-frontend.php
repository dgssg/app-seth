<?php
include("database/database.php");
$query = "SELECT custom_service.nome,category.nome,equipamento_familia.fabricante,os_sla.sla_verde,os_sla.sla_amarelo,os.sla_term,os_grupo_sla.grupo,os_posicionamento.cor,os_prioridade.prioridade,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo FROM os LEFT JOIN instituicao ON instituicao.id = os.id_unidade LEFT JOIN usuario on usuario.id = os.id_usuario LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor LEFT JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area LEFT JOIN os_status on os_status.id = os.id_status LEFT JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento LEFT JOIN os_carimbo ON os_carimbo.id = os.id_carimbo LEFT JOIN os_workflow on os_workflow.id = os.id_workflow LEFT JOIN equipamento on equipamento.id = os.id_equipamento LEFT JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT JOIN os_prioridade on os_prioridade.id = os.id_prioridade LEFT JOIN os_sla on os_sla.id = os.id_sla LEFT JOIN os_grupo_sla on os_grupo_sla.id = os_sla.os_grupo_sla left join custom_service on custom_service.id = os.id_servico left join category on category.id = os.id_category  where os.id_status like '2' order by os.id DESC";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($custom_service,$category,$equipamento_fabricante,$sla_verde,$sla_amarelo,$sla_term,$grupo,$color,$prioridade,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo);
 

  ?>
  <style>
  * {
    box-sizing: border-box;
  }

  #myInput {

    background-position: 10px 10px;
    background-repeat: no-repeat;
    width: 100%;
    font-size: 16px;
    padding: 12px 20px 12px 40px;
    border: 1px solid #ddd;
    margin-bottom: 12px;
  }

  #myTable {
    border-collapse: collapse;
    width: 100%;
    border: 1px solid #ddd;
    font-size: 18px;
  }

  #myTable th, #myTable td {
    text-align: left;
    padding: 12px;
  }

  #myTable tr {
    border-bottom: 1px solid #ddd;
  }

  #myTable tr.header, #myTable tr:hover {
    background-color: #f1f1f1;
  }
    .custom-black-button {
      background-color: black;
      color: white; /* Altere a cor do texto conforme necessário */
    }

</style>



<h2>Serviço</h2>



<div class="row">
  <div class="col-12">
    <input type="text" id="filtro" placeholder="Digite">
    <div class="card shadow-sm mb-4">
      <ul id="lista-usuarios" class="list-group list-group-flush bg-none">
        <?php   while ($stmt->fetch()) {   ?>
        
        <style>
          .custom-bg-color {
            background-color: <?php echo $color; ?>; /* Use a variável PHP $color aqui */
          }
        </style>
        <div id="box_<?php echo $id_os; ?>">
          <li class="list-group-item">
            <div class="row">
              <div class="col-auto">
                
                <figure class="avatar avatar-50 rounded-10 shadow-sm">
                  <img src="" alt="<?printf($id_os); ?>">
                </figure>
              </div>
              <div class="col px-0">
                <p><?php printf($equipamento_codigo); ?> - <?php printf($equipamento_nome); ?> - <?php printf($equipamento_modelo); ?> - <?php printf($equipamento_fabricante); ?> <br><small class="text-muted"><?php printf($id_usuario_nome); ?> <?php printf($id_usuario_sobrenome); ?></small></p>
              </div>
              <div class="col-auto text-end">
                <p><small class="text-muted size-12"><?php printf($id_posicionamento); ?> <span class="avatar avatar-6 rounded-circle bg-success d-inline-block"></span></small></p>
              </div>
            </div>
          </li>
          <script>
            // Selecionando o elemento para aplicar o swipe
            var box = document.getElementById('box_<?php echo $id_os; ?>');
            
            // Criando um objeto Hammer para o elemento
            var hammer = new Hammer(box);
            // Adicionando um ouvinte de evento para detectar o gesto de tap (toque)
            hammer.on('tap', function(event) {
            //  alert('Tap (Toque) detectado!');
              window.location.href = 'corrective-maintenance-open-pos?id=<?php echo $id_os; ?>';
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de double tap (toque duplo)
            hammer.on('doubletap', function(event) {
              alert('Double Tap (Toque Duplo) detectado!');
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de pinch (beliscar)
            hammer.on('pinch', function(event) {
              alert('Pinch (Beliscar) detectado!');
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de rotate (rotacionar)
            hammer.on('rotate', function(event) {
              alert('Rotate (Rotacionar) detectado!');
            });
            
            // Adicionando um ouvinte de evento para detectar o gesto de long press (pressão longa)
            hammer.on('press', function(event) {
              alert('O.S: <?php echo $id_os; ?>\nUnidade: <?php printf($id_instituicao); ?>\nSetor: <?php printf($id_area); ?>\nArea: <?php printf($id_localizacao); ?>\nSolicitação: <?php escapeshellarg($id_solicitacao); ?>\nPosicionamento: <?php printf($id_posicionamento); ?>\nPrioridade: <?php printf($prioridade); ?>');

                           
            });
            // Adicionando um ouvinte de evento para detectar o gesto swipe
            hammer.on('swipe', function(event) {
              // Verificando a direção do swipe e exibindo uma mensagem correspondente
              switch(event.direction) {
                case Hammer.DIRECTION_UP:
                  alert('Swipe para cima detectado!');
                  break;
                case Hammer.DIRECTION_DOWN:
                  alert('Swipe para baixo detectado!');
                  break;
                case Hammer.DIRECTION_LEFT:
                  //alert('Swipe para esquerda detectado!');
                  window.location.href = 'corrective-maintenance-open-close?id=<?php echo $id_os; ?>';
                  break;
                case Hammer.DIRECTION_RIGHT:
                  //alert('Swipe para direita detectado!');
                  // Redirecionando a página
                  window.location.href = 'corrective-maintenance-open?id=<?php echo $id_os; ?>';


                  break;
              }
            });
          </script> 
        </div>
        <?php   } }  ?>

        <!-- Adicione mais itens de lista conforme necessário -->
      </ul>
    </div>
  </div>
</div>

<script>
  document.getElementById("filtro").addEventListener("input", function () {
    var filtro = this.value.toLowerCase();
    var listaUsuarios = document.getElementById("lista-usuarios").getElementsByTagName("li");
    
    for (var i = 0; i < listaUsuarios.length; i++) {
      var nome = listaUsuarios[i].getElementsByTagName("p")[0].innerText.toLowerCase();
      var cargo = listaUsuarios[i].getElementsByTagName("small")[0].innerText.toLowerCase();
      
      if (nome.includes(filtro) || cargo.includes(filtro)) {
        listaUsuarios[i].style.display = "";
      } else {
        listaUsuarios[i].style.display = "none";
      }
    }
  });
</script>
