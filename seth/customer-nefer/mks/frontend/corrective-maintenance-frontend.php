<?php
include("database/database.php");
$query = "SELECT custom_service.nome,category.nome,equipamento_familia.fabricante,os_sla.sla_verde,os_sla.sla_amarelo,os.sla_term,os_grupo_sla.grupo,os_posicionamento.cor,os_prioridade.prioridade,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo FROM os INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN usuario on usuario.id = os.id_usuario INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor INNER JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area INNER JOIN os_status on os_status.id = os.id_status INNER JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento INNER JOIN os_carimbo ON os_carimbo.id = os.id_carimbo INNER JOIN os_workflow on os_workflow.id = os.id_workflow INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN os_prioridade on os_prioridade.id = os.id_prioridade INNER JOIN os_sla on os_sla.id = os.id_sla INNER JOIN os_grupo_sla on os_grupo_sla.id = os_sla.os_grupo_sla left join custom_service on custom_service.id = os.id_servico left join category on category.id = os.id_category  where os.id_status like '2' order by os.id DESC";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($custom_service,$category,$equipamento_fabricante,$sla_verde,$sla_amarelo,$sla_term,$grupo,$color,$prioridade,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo);
 

  ?>
  <style>
  * {
    box-sizing: border-box;
  }

  #myInput {

    background-position: 10px 10px;
    background-repeat: no-repeat;
    width: 100%;
    font-size: 16px;
    padding: 12px 20px 12px 40px;
    border: 1px solid #ddd;
    margin-bottom: 12px;
  }

  #myTable {
    border-collapse: collapse;
    width: 100%;
    border: 1px solid #ddd;
    font-size: 18px;
  }

  #myTable th, #myTable td {
    text-align: left;
    padding: 12px;
  }

  #myTable tr {
    border-bottom: 1px solid #ddd;
  }

  #myTable tr.header, #myTable tr:hover {
    background-color: #f1f1f1;
  }
</style>



<h2>Serviço</h2>

<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Busca por OS.." title="Type in a name">

<table id="myTable">
  <tr class="header">
    <th style="width:20%;">OS</th>
    <th style="width:20%;">Solicitante</th>
    
    <th style="width:30%;">Equipamento</th>
    <th style="width:30%;">Ação</th>
  </tr>
  <?php   while ($stmt->fetch()) {   ?>
    <tr>
      <td><?printf($id_os); ?></td>
      <td><?php printf($id_usuario_nome); ?> <?php printf($id_usuario_sobrenome); ?></td>
     
      <td><?php printf($equipamento_codigo); ?> - <?php printf($equipamento_nome); ?> - <?php printf($equipamento_modelo); ?> - <?php printf($equipamento_fabricante); ?> </td>
   <td> <button type="button" class="btn btn-lg btn-icon-text"
                                onclick="window.location.replace('corrective-maintenance-open.php?id=<?printf($id_os); ?>');">
                                <i class="bi bi-credit-card size-32"></i><span>Abrir</span>
                            </button></td>
    </tr>

  </tr>
        <?php   } }  ?>
</table>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
